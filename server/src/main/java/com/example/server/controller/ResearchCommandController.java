package com.example.server.controller;

import com.example.server.dto.inbound.AbortResearchDTO;
import com.example.server.dto.inbound.StartResearchDTO;
import com.example.server.dto.outbound.ResearchResultDTO;
import com.example.server.logic.PlayerIdentityMapper;
import com.example.server.logic.ResearchManager;
import com.example.server.model.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("/game/research")
public class ResearchCommandController {

    @PostMapping("/startResearch")
    public ResponseEntity<ResearchResultDTO> startResearch(@Valid @RequestBody final StartResearchDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        if (!g.hasStarted()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        g.getUpdateLock().lock();
        Unit u = g.getGameField().getCell(dto.getX(), dto.getY()).getUnit();
        if (u == null || u.getPlayer() != p || !(u instanceof ResearchBuilding)) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        ResearchBuilding unit = (ResearchBuilding) u;
        Research toBeStartedResearch = ResearchManager.getResearchByName(dto.getResearchName());
        if (toBeStartedResearch == null) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        if (unit.getCurrentResearch() != null) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
        if (!unit.mayStartResearch(toBeStartedResearch)) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        unit.startNewResearch(toBeStartedResearch);
        ResearchResultDTO res = new ResearchResultDTO();
        res.setNewResources(p.getResources());
        res.setResearchX(unit.getCell().x);
        res.setResearchY(unit.getCell().y);
        res.setResearchBuildingState(unit);
        g.getUpdateLock().unlock();
        return new ResponseEntity(res, HttpStatus.OK);
    }

    @PostMapping("/abortResearch")
    public ResponseEntity<ResearchResultDTO> abortResearch(@Valid @RequestBody final AbortResearchDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        if (!g.hasStarted()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        g.getUpdateLock().lock();
        Unit u = g.getGameField().getCell(dto.getX(), dto.getY()).getUnit();
        if (u == null || u.getPlayer() != p || !(u instanceof ResearchBuilding)) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        ResearchBuilding unit = (ResearchBuilding) u;
        if (!unit.abortCurrentResearch()) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        ResearchResultDTO res = new ResearchResultDTO();
        res.setNewResources(p.getResources());
        res.setResearchX(unit.getCell().x);
        res.setResearchY(unit.getCell().y);
        res.setResearchBuildingState(unit);
        g.getUpdateLock().unlock();
        return new ResponseEntity(res, HttpStatus.OK);
    }
}
