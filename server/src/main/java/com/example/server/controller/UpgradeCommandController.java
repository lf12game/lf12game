package com.example.server.controller;

import com.example.server.dto.inbound.AbortUpgradeDTO;
import com.example.server.dto.inbound.BuildRobotDTO;
import com.example.server.dto.inbound.StartUpgradeDTO;
import com.example.server.dto.outbound.RobotProductionResultDTO;
import com.example.server.dto.outbound.UpgradeResultDTO;
import com.example.server.logic.PlayerIdentityMapper;
import com.example.server.logic.UpgradeManager;
import com.example.server.model.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("/game/upgrade")
public class UpgradeCommandController {

    @PostMapping("/startUpgrade")
    public ResponseEntity<UpgradeResultDTO> startUpgrade(@Valid @RequestBody final StartUpgradeDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        if (!g.hasStarted()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        g.getUpdateLock().lock();
        Unit u = g.getGameField().getCell(dto.getX(), dto.getY()).getUnit();
        if (u == null || u.getPlayer() != p || !(u instanceof StatUnit)) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        StatUnit unit = (StatUnit) u;
        Upgrade toBeStartedUpgrade = UpgradeManager.getUpgradeByName(unit.getUnitType(), dto.getUpgradeName());
        if (toBeStartedUpgrade == null) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        if (unit.getCurrentUpgrade() != null) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
        if (!unit.mayStartUpgrade(toBeStartedUpgrade)) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        unit.startNewUpgrade(toBeStartedUpgrade);
        UpgradeResultDTO res = new UpgradeResultDTO();
        res.setNewResources(p.getResources());
        res.setUpgradeX(unit.getCell().x);
        res.setUpgradeY(unit.getCell().y);
        res.setUnitUpgradeState(unit);
        g.getUpdateLock().unlock();
        return new ResponseEntity(res, HttpStatus.OK);
    }

    @PostMapping("/abortUpgrade")
    public ResponseEntity<UpgradeResultDTO> abortUpgrade(@Valid @RequestBody final AbortUpgradeDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        if (!g.hasStarted()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        g.getUpdateLock().lock();
        Unit u = g.getGameField().getCell(dto.getX(), dto.getY()).getUnit();
        if (u == null || u.getPlayer() != p || !(u instanceof StatUnit)) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        StatUnit unit = (StatUnit) u;
        if (!unit.abortCurrentUpgrade()) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        UpgradeResultDTO res = new UpgradeResultDTO();
        res.setNewResources(p.getResources());
        res.setUpgradeX(unit.getCell().x);
        res.setUpgradeY(unit.getCell().y);
        res.setUnitUpgradeState(unit);
        g.getUpdateLock().unlock();
        return new ResponseEntity(res, HttpStatus.OK);
    }
}
