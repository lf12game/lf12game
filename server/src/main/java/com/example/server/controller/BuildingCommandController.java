package com.example.server.controller;

import com.example.server.dto.inbound.*;
import com.example.server.dto.outbound.AutomatedCommandsResultDTO;
import com.example.server.dto.outbound.BuildingResultDTO;
import com.example.server.dto.outbound.GetMainRobotMoveDTO;
import com.example.server.dto.outbound.RobotProductionResultDTO;
import com.example.server.logic.PlayerIdentityMapper;
import com.example.server.model.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.awt.*;

@RestController
@CrossOrigin
@RequestMapping("/game/building")
public class BuildingCommandController {

    @PostMapping("/build")
    public ResponseEntity<BuildingResultDTO> buildBuilding(@Valid @RequestBody final BuildBuildingDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        if (!g.hasStarted()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        if (!Building.buildingExists(dto.getBuildingType())) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        g.getUpdateLock().lock();
        MainRobot mR = p.getMainRobot();
        if (mR.getCurrentConstruction() != null) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        Cell target = g.getGameField().getCell(dto.getX(), dto.getY());
        Building toBuild = Building.createBuilding(p, target, dto.getBuildingType());
        if (toBuild == null) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
        mR.setCurrentConstruction(toBuild);
        mR.setCurrentMoveOrders(new Point(dto.getX(), dto.getY()));
        BuildingResultDTO res = new BuildingResultDTO();
        res.setMainRobotX(dto.getX());
        res.setMainRobotY(dto.getY());
        res.setNewResources(p.getResources());
        g.getUpdateLock().unlock();
        return new ResponseEntity(res, HttpStatus.OK);
    }

    @PostMapping("/abort")
    public ResponseEntity<BuildingResultDTO> abortBuilding(@Valid @RequestBody final AbortBuildingDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        if (!g.hasStarted()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        g.getUpdateLock().lock();
        MainRobot mR = p.getMainRobot();
        if (mR.getCurrentConstruction() == null) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        BuildingResultDTO res = new BuildingResultDTO();
        res.setBuildingX(mR.getCurrentConstruction().getCell().x);
        res.setBuildingY(mR.getCurrentConstruction().getCell().y);
        mR.getCurrentConstruction().abort();
        mR.setCurrentConstruction(null);
        mR.setCurrentMoveOrders(new Point(mR.getCell().x, mR.getCell().y));

        res.setMainRobotX(mR.getCurrentMoveOrders().x);
        res.setMainRobotY(mR.getCurrentMoveOrders().y);
        res.setNewResources(p.getResources());

        g.getUpdateLock().unlock();
        return new ResponseEntity(res, HttpStatus.OK);
    }

    @PostMapping("/buildRobot")
    public ResponseEntity<RobotProductionResultDTO> buildRobot(@Valid @RequestBody final BuildRobotDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        if (!g.hasStarted()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        g.getUpdateLock().lock();
        Unit u = g.getGameField().getCell(dto.getX(), dto.getY()).getUnit();
        if (u == null || u.getPlayer() != p || !(u instanceof RobotProductionBuilding)) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        if (!AutomatedRobot.robotExists(dto.getRobotType())) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        if (!AutomatedRobot.robotUnlocked(p, dto.getRobotType())) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        RobotProductionBuilding prod = (RobotProductionBuilding) u;
        if (!prod.canStartRobotProduction()) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        AutomatedRobot toBuild = AutomatedRobot.createRobot(p, g.getGameField().getCell(dto.getX(), dto.getY()), dto.getRobotType());
        if (toBuild == null) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
        prod.startRobotProduction(toBuild);
        RobotProductionResultDTO res = new RobotProductionResultDTO();
        res.setNewResources(p.getResources());
        res.setBuildingX(prod.getCell().x);
        res.setBuildingY(prod.getCell().y);
        res.setProductionBuildingState(prod);
        g.getUpdateLock().unlock();
        return new ResponseEntity(res, HttpStatus.OK);
    }

    @PostMapping("/abortRobot")
    public ResponseEntity<RobotProductionResultDTO> abortRobot(@Valid @RequestBody final AbortRobotDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        if (!g.hasStarted()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        g.getUpdateLock().lock();
        Unit u = g.getGameField().getCell(dto.getX(), dto.getY()).getUnit();
        if (u == null || u.getPlayer() != p || !(u instanceof RobotProductionBuilding)) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        RobotProductionBuilding prod = (RobotProductionBuilding) u;
        if (!prod.abortProduction()) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        RobotProductionResultDTO res = new RobotProductionResultDTO();
        res.setNewResources(p.getResources());
        res.setBuildingX(prod.getCell().x);
        res.setBuildingY(prod.getCell().y);
        res.setProductionBuildingState(prod);
        g.getUpdateLock().unlock();
        return new ResponseEntity(res, HttpStatus.OK);
    }

    @PostMapping("/changeRobotCode")
    public ResponseEntity<AutomatedCommandsResultDTO> changeRobotCode(@Valid @RequestBody final ChangeAutomatedCommandsDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        if (!g.hasStarted()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        g.getUpdateLock().lock();
        Unit u = g.getGameField().getCell(dto.getX(), dto.getY()).getUnit();
        if (u == null || u.getPlayer() != p || !(u instanceof RobotProductionBuilding)) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        RobotProductionBuilding prod = (RobotProductionBuilding) u;
        if (dto.getRootCommand() == null || !dto.getRootCommand().getName().equals("While true")) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        prod.setRootCommand(dto.getRootCommand());
        AutomatedCommandsResultDTO res = new AutomatedCommandsResultDTO();
        res.setBuildingX(dto.getX());
        res.setBuildingY(dto.getY());
        res.setRootCommand(prod.getRootCommand());
        g.getUpdateLock().unlock();
        return new ResponseEntity(res, HttpStatus.OK);
    }

    @PostMapping("/changeRepeatProduction")
    public ResponseEntity changeRepeatProduction(@Valid @RequestBody final ChangeRepeatProductionDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        if (!g.hasStarted()) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        g.getUpdateLock().lock();
        Unit u = g.getGameField().getCell(dto.getX(), dto.getY()).getUnit();
        if (u == null || u.getPlayer() != p || !(u instanceof RobotProductionBuilding)) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        RobotProductionBuilding prod = (RobotProductionBuilding) u;
        prod.setRepeatProduction(dto.isRepeatProduction());
        g.getUpdateLock().unlock();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
