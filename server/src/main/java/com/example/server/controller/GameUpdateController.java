package com.example.server.controller;

import com.example.server.Utility.Pair;
import com.example.server.dto.inbound.PollGameUpdateDTO;
import com.example.server.dto.inbound.PollLobbiesDTO;
import com.example.server.dto.outbound.GetGameUpdateDTO;
import com.example.server.dto.outbound.GetLobbiesDTO;
import com.example.server.logic.FogOfWarGenerator;
import com.example.server.logic.GameManager;
import com.example.server.logic.PlayerIdentityMapper;
import com.example.server.model.Cell;
import com.example.server.model.Game;
import com.example.server.model.Player;
import com.example.server.model.enums.GameResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/game")
public class GameUpdateController {
    @PutMapping
    public ResponseEntity<GetGameUpdateDTO> getGameUpdate(@Valid @RequestBody final PollGameUpdateDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (g == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        if (!g.hasStarted()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        GetGameUpdateDTO res = new GetGameUpdateDTO();
        Pair<Pair<Cell[][], boolean[][]>, Long> data = FogOfWarGenerator.getGameFieldOfPlayer(p, dto.getPrevVersion());
        res.setVersion(data.getSecond());
        res.setGameField(data.getFirst().getFirst());
        res.setVisible(data.getFirst().getSecond());
        res.setResources(p.getResources());
        res.setPlayerNames(g.getPlayerNames());
        res.setGameResult(GameResult.NONE);
        if (g.hasStarted()) {
            if (p.getMainRobot() == null) {
                res.setGameResult(GameResult.LOSE);
            } else {
                boolean anyAlive = g.isSinglePlayer();
                for (Player player : g.getPlayers()) {
                    if (player != p) {
                        if (player.getMainRobot() != null) {
                            anyAlive = true;
                            break;
                        }
                    }
                }
                if (!anyAlive) {
                    res.setGameResult(GameResult.WIN);
                }
            }
        }
        res.setFinishedResearch(p.getFinishedResearch());
        return new ResponseEntity(res, HttpStatus.OK);
    }
}
