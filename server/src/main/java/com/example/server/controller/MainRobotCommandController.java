package com.example.server.controller;

import com.example.server.dto.inbound.MoveMainRobotDTO;
import com.example.server.dto.outbound.GetMainRobotMoveDTO;
import com.example.server.logic.PlayerIdentityMapper;
import com.example.server.model.Game;
import com.example.server.model.MainRobot;
import com.example.server.model.Player;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.awt.*;

@RestController
@CrossOrigin
@RequestMapping("/game")
public class MainRobotCommandController {

    @PostMapping("/moveMainRobot")
    public ResponseEntity<GetMainRobotMoveDTO> moveMainRobot(@Valid @RequestBody final MoveMainRobotDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        if (!g.hasStarted()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        g.getUpdateLock().lock();
        MainRobot mR = p.getMainRobot();
        if (mR.getCurrentConstruction() != null) {
            g.getUpdateLock().unlock();
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        mR.setCurrentMoveOrders(new Point(dto.getX(), dto.getY()));
        g.getUpdateLock().unlock();
        GetMainRobotMoveDTO res = new GetMainRobotMoveDTO();
        res.setX(dto.getX());
        res.setY(dto.getY());
        return new ResponseEntity(res, HttpStatus.OK);
    }
}
