package com.example.server.controller;

import com.example.server.dto.inbound.ChangeWaypointDTO;
import com.example.server.dto.inbound.MoveMainRobotDTO;
import com.example.server.dto.outbound.GetMainRobotMoveDTO;
import com.example.server.logic.PlayerIdentityMapper;
import com.example.server.model.Game;
import com.example.server.model.MainRobot;
import com.example.server.model.Player;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.awt.*;

@RestController
@CrossOrigin
@RequestMapping("/game")
public class WaypointCommandController {

    @PostMapping("/changeWaypoint")
    public ResponseEntity changeWaypoint(@Valid @RequestBody final ChangeWaypointDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        if (!g.hasStarted()) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        if (dto.getIndex() >= Game.WAYPOINTSPERPLAYER || ((dto.getX() >= g.getGameField().x || dto.getY() >= g.getGameField().y) && !(dto.getX() == -1 && dto.getY() == -1))) {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
        g.getUpdateLock().lock();
        p.changeWaypoint(dto.getX(), dto.getY(), dto.getIndex());
        g.getUpdateLock().unlock();
        return new ResponseEntity(HttpStatus.OK);
    }
}
