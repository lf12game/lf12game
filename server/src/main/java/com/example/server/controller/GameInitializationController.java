package com.example.server.controller;

import com.example.server.Utility.Pair;
import com.example.server.dto.inbound.*;
import com.example.server.dto.outbound.GetGameUpdateDTO;
import com.example.server.dto.outbound.GetLobbiesDTO;
import com.example.server.dto.outbound.GetLobbyUpdateDTO;
import com.example.server.dto.outbound.RejoinGameResultDTO;
import com.example.server.logic.*;
import com.example.server.model.Cell;
import com.example.server.model.Game;
import com.example.server.model.GameSettings;
import com.example.server.model.Player;
import com.example.server.model.enums.GameResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/lobbies")
public class GameInitializationController {

    @PostMapping
    public ResponseEntity<GetLobbyUpdateDTO> createGame(@Valid @RequestBody final CreateGameDTO dto) {
        if (!GameManager.getInstance().gameNameUnused(dto.getGameName())) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        Player host = new Player(dto.getPlayerName());
        Game game = new Game(dto.getGameName(), host, dto.getPassword());
        GameManager.getInstance().addGame(game);
        PlayerIdentityMapper.getInstance().addPlayerIdentity(host);
        GetLobbyUpdateDTO res = new GetLobbyUpdateDTO();
        res.setGameName(game.getName());
        res.setPlayerNames(game.getPlayerNames());
        res.setPlayerIdentification(host.getIdentification());
        res.setStarted(game.hasStarted());
        return new ResponseEntity(res, HttpStatus.OK);
    }

    @PostMapping("/start")
    public ResponseEntity startGame(@Valid @RequestBody final StartGameDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        Game g = GameManager.getInstance().getGameByName(dto.getGameName());
        if (g == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        if (!g.isHost(p)) {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        if (g.hasStarted()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        g.startGame();
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/join")
    public ResponseEntity<GetLobbyUpdateDTO> joinGame(@Valid @RequestBody final JoinGameDTO dto) {
        Game g = GameManager.getInstance().getGameByName(dto.getGameName());
        if (g == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        if (g.hasStarted()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        if (g.hasPassword() && !g.passwordMatch(dto.getPassword())) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        if (!g.playerNameUnused(dto.getPlayerName())) {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        Player p = new Player(dto.getPlayerName());
        g.addPlayer(p);
        PlayerIdentityMapper.getInstance().addPlayerIdentity(p);
        GetLobbyUpdateDTO res = new GetLobbyUpdateDTO();
        res.setGameName(g.getName());
        res.setPlayerNames(g.getPlayerNames());
        res.setPlayerIdentification(p.getIdentification());
        res.setStarted(g.hasStarted());
        return new ResponseEntity(res, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<GetLobbiesDTO> getLobbies(@Valid @RequestBody final PollLobbiesDTO dto) {
        GetLobbiesDTO res = new GetLobbiesDTO();
        Pair<List<String>, Long> data = GameManager.getInstance().getLobbyNamesSynchronizer().acquireData(dto.getPrevVersion());
        res.setVersion(data.getSecond());
        res.setLobbyNames(data.getFirst());
        return new ResponseEntity(res, HttpStatus.OK);
    }

    @PutMapping("/lobby")
    public ResponseEntity<GetLobbyUpdateDTO> getLobbyUpdates(@Valid @RequestBody final LobbyUpdateDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        Game g = GameManager.getInstance().getGameByName(dto.getGameName());
        if (g == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        if (g.hasStarted()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        GetLobbyUpdateDTO res = new GetLobbyUpdateDTO();
        Pair<List<String>, Long> data = g.getPlayerNamesSynchronizer().acquireData(dto.getPrevVersion());
        res.setPlayerNames(data.getFirst());
        res.setVersion(data.getSecond());
        res.setGameName(dto.getGameName());
        res.setPlayerIdentification(dto.getIdentification());
        res.setStarted(g.hasStarted());
        res.setBlockerFrequency(g.getGameSettings().getBlockerFrequencyPerHundred());
        res.setResourceFrequency(g.getGameSettings().getResourceFrequencyPerThousand());
        res.setStartResources(g.getGameSettings().getStartResources());
        res.setUpdateFrequency(g.getGameSettings().getUpdateFrequency());
        res.setX(g.getGameSettings().getX());
        res.setY(g.getGameSettings().getY());
        if (g.hasStarted()) {
            res.setAllUpgrades(UpgradeManager.getAllUpgrades());
            res.setAllResearch(ResearchManager.getAllResearch());
        }
        return new ResponseEntity(res, HttpStatus.OK);
    }

    @PostMapping("/updateSettings")
    public ResponseEntity updateLobbySettings(@Valid @RequestBody final ChangeLobbySettingsDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (g == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (!g.isHost(p)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        if (g.hasStarted()) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        GameSettings gameSettings = new GameSettings(dto.getX(), dto.getY(), dto.getStartResources(), dto.getUpdateFrequency(),
                dto.getBlockerFrequency(), dto.getResourceFrequency());

        g.setGameSettings(gameSettings);

        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/rejoin")
    public ResponseEntity<RejoinGameResultDTO> rejoinGame(@Valid @RequestBody final RejoinGameDTO dto) {
        Player p = PlayerIdentityMapper.getInstance().getPlayer(dto.getIdentification());
        if (p == null) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        Game g = p.getGame();
        if (g == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        if (!g.getPlayers().contains(p)) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        GetLobbyUpdateDTO res = new GetLobbyUpdateDTO();
        Pair<List<String>, Long> data = g.getPlayerNamesSynchronizer().acquireData(-1);
        res.setPlayerNames(data.getFirst());
        res.setVersion(data.getSecond());
        res.setGameName(g.getName());
        res.setPlayerIdentification(dto.getIdentification());
        res.setStarted(g.hasStarted());
        res.setBlockerFrequency(g.getGameSettings().getBlockerFrequencyPerHundred());
        res.setResourceFrequency(g.getGameSettings().getResourceFrequencyPerThousand());
        res.setStartResources(g.getGameSettings().getStartResources());
        res.setUpdateFrequency(g.getGameSettings().getUpdateFrequency());
        res.setX(g.getGameSettings().getX());
        res.setY(g.getGameSettings().getY());
        if (g.hasStarted()) {
            res.setAllUpgrades(UpgradeManager.getAllUpgrades());
            res.setAllResearch(ResearchManager.getAllResearch());
        }
        RejoinGameResultDTO result = new RejoinGameResultDTO();
        result.setLobbyInfo(res);
        result.setPlayerName(p.getName());
        result.setWaypoints(p.getWaypoints());
        if (g.hasStarted()) {
            GetGameUpdateDTO upd = new GetGameUpdateDTO();
            Pair<Pair<Cell[][], boolean[][]>, Long> updateData = FogOfWarGenerator.getGameFieldOfPlayer(p, -1);
            upd.setVersion(updateData.getSecond());
            upd.setGameField(updateData.getFirst().getFirst());
            upd.setVisible(updateData.getFirst().getSecond());
            upd.setResources(p.getResources());
            upd.setPlayerNames(g.getPlayerNames());
            upd.setGameResult(GameResult.NONE);
            upd.setFinishedResearch(p.getFinishedResearch());
            result.setGameUpdate(upd);
        }
        return new ResponseEntity(result, HttpStatus.OK);
    }
}
