package com.example.server.logic;

import com.example.server.model.StatUnit;
import com.example.server.model.Upgrade;

import java.util.*;

public class UpgradeManager {

    private static Map<String, Map<String, Upgrade>> availableUpgrades;
    private static List<Upgrade> allUpgrades;

    public static boolean mayStartUpgrade(final StatUnit unit, final Upgrade upgrade) {
        if (unit.getPlayer().getResources() < upgrade.cost) {
            return false;
        }
        for (String s : upgrade.prerequisites) {
            if (!unit.getUpgrades().contains(getUpgradeByName(unit.getUnitType(), s))) {
                return false;
            }
        }
        return true;
    }

    public static Upgrade getUpgradeByName(final String unitType, final String upgradeName) {
        Map<String, Upgrade> upgradeMap = availableUpgrades.get(unitType);
        if (upgradeMap == null) {
            return null;
        }
        return upgradeMap.get(upgradeName);
    }

    public static List<Upgrade> getAllUpgrades() {
        return new ArrayList<>(allUpgrades);
    }

    private static void addUpgradeToListAndMap(final Upgrade u) {
        if (!allUpgrades.contains(u)) {
            allUpgrades.add(u);
        }
        if (availableUpgrades.get(u.upgradeType) == null) {
            availableUpgrades.put(u.upgradeType, new HashMap<>());
        }
        availableUpgrades.get(u.upgradeType).put(u.upgradeName, u);
    }

    private static void defineAndAddUpgrade(final String upgradeName, final int maxHp, final int speed, final int damage, final int range, final int vision,
                                            final int armor, final int armorPiercing, final int resourcesPerTick, final double productionSpeedMult,
                                            final int duration, final int cost, final List<String> prerequisites, final String upgradeType, final String description) {
        addUpgradeToListAndMap(new Upgrade(upgradeName, maxHp, speed, damage, range, vision, armor, armorPiercing, resourcesPerTick, productionSpeedMult,
                duration, cost, prerequisites, upgradeType, description));
    }

    static {
        availableUpgrades = new HashMap<>();
        allUpgrades = new ArrayList<>();
        defineAndAddUpgrade("Titan Armor", 2000, 0, 0, 0, 0, 0, 0, 0, 1,
                30, 400, new ArrayList<String>(), "MainRobot", "Improves HP by 2000");
        defineAndAddUpgrade("Uranium Rounds", 0, 0, 20, 0, 0, 0, 0, 0, 1,
                20, 300, new ArrayList<String>(), "MainRobot", "Improves damage by 20");
        defineAndAddUpgrade("Construction Nanobots", 0, 0, 0, 0, 0, 0, 0, 0, 2,
                15, 100, new ArrayList<String>(), "MainRobot", "2x building speed");

        defineAndAddUpgrade("Diamond Drills", 0, 0, 0, 0, 0, 0, 0, 1, 1,
                10, 100, new ArrayList<String>(), "Resource", "+1 resources per tick");
        defineAndAddUpgrade("Unobtainium Drills", 0, 0, 0, 0, 0, 0, 0, 2, 1,
                10, 200, new ArrayList<String>(Arrays.asList("Diamond Drills")), "Resource", "+2 resources per tick");

        defineAndAddUpgrade("Assembly Nanobots", 0, 0, 0, 0, 0, 0, 0, 0, 2,
                10, 50, new ArrayList<String>(), "Production", "2x robot building speed");
        defineAndAddUpgrade("Armor Hardening", 250, 0, 0, 0, 0, 5, 0, 0, 1,
                10, 50, new ArrayList<String>(), "Production", "+250 HP, +5 armor");

        defineAndAddUpgrade("Penetrating Rounds", 0, 0, 25, 0, 0, 0, 0, 0, 1,
                10, 100, new ArrayList<String>(), "Defence", "Improves damage by 25");
        defineAndAddUpgrade("Concrete Walls", 400, 0, 0, 0, 0, 0, 0, 0, 1,
                15, 150, new ArrayList<String>(), "Defence", "Improves HP by 400");

        defineAndAddUpgrade("Radar Focusing", 0, 0, 0, 0, 10, 0, 0, 0, 1,
                10, 100, new ArrayList<String>(), "Radar", "Improves Vision by 10");
        defineAndAddUpgrade("AI Analysis", 0, 0, 0, 0, 20, 0, 0, 0, 1,
                20, 200, new ArrayList<String>(Arrays.asList("Radar Focusing")), "Radar", "Improves Vision by 20");
    }
}
