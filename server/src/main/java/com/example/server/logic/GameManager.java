package com.example.server.logic;

import com.example.server.model.Game;

import java.util.ArrayList;
import java.util.List;

public class GameManager {
    private List<Game> games;
    private static GameManager instance = new GameManager();
    private LongPollingSynchronizer<List<String>> lobbyNamesSynchronizer;

    private GameManager() {
        games = new ArrayList<>();
        lobbyNamesSynchronizer = new LongPollingSynchronizer<>();
        lobbyNamesSynchronizer.giveData(new ArrayList<>());
    }

    public static GameManager getInstance() {
        return instance;
    }

    public LongPollingSynchronizer<List<String>> getLobbyNamesSynchronizer() {
        return lobbyNamesSynchronizer;
    }

    public synchronized boolean addGame(final Game game) {
        if (gameNameUnused(game.getName())) {
            games.add(game);
            lobbyNamesSynchronizer.giveData(getLobbyNames());
            return true;
        }
        return false;
    }

    public synchronized void removeGame(final Game game) {
        games.remove(game);
        lobbyNamesSynchronizer.giveData(getLobbyNames());
    }

    public synchronized boolean gameNameUnused(final String gameName) {
        for (Game g : games) {
            if (g.getName().equals(gameName)) {
                return false;
            }
        }
        return true;
    }

    public synchronized List<String> getLobbyNames() {
        List<String> res = new ArrayList<>();
        for (Game g : games) {
            if (!g.hasStarted()) {
                res.add(g.getName());
            }
        }
        return res;
    }

    public synchronized Game getGameByName(final String gameName) {
        for (Game g : games) {
            if (g.getName().equals(gameName)) {
                return g;
            }
        }
        return null;
    }

    public synchronized List<Game> getAllGames() {
        return new ArrayList<>(games);
    }


}
