package com.example.server.logic;

import com.example.server.model.Cell;
import com.example.server.model.GameField;
import com.example.server.model.StatUnit;
import com.example.server.model.UnitEvent;
import com.example.server.model.enums.UnitEventType;

import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Pathfinding {

    public static void pathFind(final StatUnit u, final Point to, final int maxConsideredCells) {
        if (u.getSpeed() <= 0 || (u.getCell().x == to.x && u.getCell().y == to.y)) {
            return;
        }
        GameField gameField = u.getCell().getGameField();
        Point from = new Point(u.getCell().x, u.getCell().y);
        Point[] path = getPath(gameField, from, to, u.getSpeed(), maxConsideredCells);
        if (path.length > 0) {
            Cell origin = u.getCell();
            Cell destination = gameField.getCell(path[path.length - 1].x, path[path.length - 1].y);
            UnitEvent e = new UnitEvent(UnitEventType.MOVE, new Point(origin.x, origin.y), new Point(destination.x, destination.y));
            origin.addTurnEvent(e);
            destination.addTurnEvent(e);
        }
        for (Point p : path) {
            Cell target = gameField.getCell(p.x, p.y);
            if (!u.getCell().moveUnit(target)) {
                break;
            }
        }
    }

    private static Point[] getPath(final GameField gameField, final Point from, final Point to, final int stepsNeeded, final int maxConsideredCells) {
        Cell[][] field = gameField.getField();
        Node[][] nodes = new Node[field.length][field[0].length];
        Comparator<Node> comparator = new NodeComparator();
        PriorityQueue<Node> queue = new PriorityQueue<Node>(maxConsideredCells * 6, comparator);
        Node start = new Node(from.x, from.y);
        start.currentDistance = 0;
        start.heuristicDistance = heuristicDistance(start, to.x, to.y);
        nodes[from.y][from.x] = start;
        queue.add(start);
        int steps = 0;
        Node best = null;
        boolean targetBlocked = field[to.y][to.x].hasUnit() || !field[to.y][to.x].getTerrain().passable;
        done:
        while (!queue.isEmpty()) {
            steps++;
            Node current = queue.poll();
            if (best == null || current.heuristicDistance < best.heuristicDistance) {
                best = current;
            }
            if (steps > maxConsideredCells) {
                break done;
            }
            for (Node neighbour : generateNeighbours(current, nodes, field, to.x, to.y)) {
                if (neighbour.x == to.x && neighbour.y == to.y) {
                    nodes[to.y][to.x] = neighbour;
                    best = neighbour;
                    break done;
                }
                if (targetBlocked && neighbour.heuristicDistance <= 1.1) {
                    nodes[neighbour.y][neighbour.x] = neighbour;
                    best = neighbour;
                    break done;
                }
                if (nodes[neighbour.y][neighbour.x] != null) {
                    Node prev = nodes[neighbour.y][neighbour.x];
                    if (neighbour.totalEstimatedDistance() < prev.totalEstimatedDistance()) {
                        queue.remove(prev);
                        nodes[neighbour.y][neighbour.x] = neighbour;
                        queue.add(neighbour);
                    }
                    ;
                } else {
                    nodes[neighbour.y][neighbour.x] = neighbour;
                    queue.add(neighbour);
                }
            }
        }
        if (best == null) {
            return new Point[1];
        }
        Point[] res = new Point[Math.min(stepsNeeded, (int) best.currentDistance)];
        int currentIndex = (int) best.currentDistance;
        while (currentIndex > stepsNeeded) {
            best = best.parent;
            currentIndex--;
        }
        while (currentIndex > 0) {
            res[currentIndex - 1] = new Point(best.x, best.y);
            best = best.parent;
            currentIndex--;
        }
        return res;
    }

    private static ArrayList<Node> generateNeighbours(final Node node, final Node[][] nodes, final Cell[][] field, int goalx, int goaly) {
        ArrayList<Node> res = new ArrayList<>();
        for (int xDif = -1; xDif <= 1; xDif++) {
            for (int yDif = -1; yDif <= 1; yDif++) {
                int x = node.x + xDif;
                int y = node.y + yDif;
                if (x < 0 || y < 0 || x >= nodes[0].length || y >= nodes.length || nodes[y][x] != null || field[y][x].hasUnit() || !field[y][x].getTerrain().passable) {
                    continue;
                }
                Node newNode = new Node(x, y);
                newNode.currentDistance = node.currentDistance + 1;
                newNode.heuristicDistance = heuristicDistance(newNode, goalx, goaly);
                newNode.parent = node;
                res.add(newNode);
            }
        }
        return res;
    }

    private static double heuristicDistance(Node node, int x, int y) {
        return Math.max(Math.abs(node.x - x), Math.abs(node.y - y)) + ((double) (Math.abs(node.x - x) + Math.abs(node.y - y))) / 1000;
    }

    private static class Node {

        public int x;
        public int y;
        public double currentDistance;
        public double heuristicDistance;
        public Node parent;


        public Node(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public double totalEstimatedDistance() {
            return currentDistance + heuristicDistance;
        }

    }

    private static class NodeComparator implements Comparator<Node> {
        @Override
        public int compare(Node x, Node y) {
            double xDis = x.totalEstimatedDistance();
            double yDis = y.totalEstimatedDistance();
            if (xDis == yDis) {
                return 0;
            }
            return xDis < yDis ? -1 : 1;
        }
    }
}
