package com.example.server.logic;

import com.example.server.Utility.Pair;
import com.example.server.model.Cell;
import com.example.server.model.Player;
import com.example.server.model.StatUnit;
import com.example.server.model.Unit;
import com.example.server.model.enums.Terrain;

public class FogOfWarGenerator {

    public static Pair<Pair<Cell[][], boolean[][]>, Long> getGameFieldOfPlayer(final Player player, final long prevVersion) {
        Pair<Cell[][], Long> map = player.getGame().getGameUpdateSynchronizer().acquireData(prevVersion);
        player.getGame().getUpdateLock().lock();
        boolean[][] visible = player.getCachedCurrentlySeenCells();
        player.getGame().getUpdateLock().unlock();
        return new Pair<>(new Pair<>(applyFogOfWar(player, map.getFirst(), visible), visible), map.getSecond());
    }

    private static Cell[][] applyFogOfWar(final Player player, final Cell[][] map, final boolean[][] visible) {
        boolean[][] onceSeenCells = player.getSeenCells();
        Cell[][] res = new Cell[map.length][map[0].length];
        for (int x = 0; x < map[0].length; x++) {
            for (int y = 0; y < map.length; y++) {
                Cell curr = map[y][x];
                Cell playerCell = new Cell(null, x, y);
                if (visible[y][x]) {
                    playerCell.setUnit(curr.getUnit(), true, false);
                    playerCell.setTerrain(curr.getTerrain());
                    playerCell.setTurnEvents(curr.getTurnEvents());
                } else if (onceSeenCells[y][x]) {
                    playerCell.setUnit(null, true, false);
                    playerCell.setTerrain(curr.getTerrain());
                } else {
                    playerCell.setUnit(null, true, false);
                    playerCell.setTerrain(Terrain.UNDISCOVERED);
                }
                res[y][x] = playerCell;
            }
        }
        return res;
    }

    public static boolean[][] getVisibleCells(final Player player) {
        int lengthY = player.getSeenCells().length;
        int lengthX = player.getSeenCells()[0].length;
        boolean[][] res = new boolean[lengthY][lengthX];
        for (Unit u : player.getUnits()) {
            if (!(u instanceof StatUnit)) {
                continue;
            }

            StatUnit s = (StatUnit) u;
            int cellX = s.getCell().x;
            int cellY = s.getCell().y;
            int vision = s.getVision();
            for (int x = Math.max(0, cellX - vision); x <= cellX + vision && x < lengthX; x++) {
                for (int y = Math.max(0, cellY - vision); y <= cellY + vision && y < lengthY; y++) {
                    res[y][x] = true;
                }
            }
        }
        return res;
    }
}
