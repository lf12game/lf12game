package com.example.server.logic;

import com.example.server.model.*;

import java.util.*;

public class ResearchManager {
    private static Map<String, Research> availableResearch;
    private static List<Research> allResearch;

    public static boolean mayStartResearch(final ResearchBuilding building, final Research research) {
        if (building.getPlayer().getResources() < research.cost) {
            return false;
        }
        for (Unit u : building.getPlayer().getUnits()) {
            if (u instanceof ResearchBuilding && u.id != building.id) {
                if (((ResearchBuilding) u).getCurrentResearch().researchName == research.researchName) {
                    return false;
                }
            }
        }
        return !building.getPlayer().getFinishedResearch().contains(research.researchName);
    }

    public static Research getResearchByName(final String researchName) {
        return availableResearch.get(researchName);
    }

    public static List<Research> getAllResearch() {
        return new ArrayList<>(allResearch);
    }

    private static void addResearchToListAndMap(final Research u) {
        if (!allResearch.contains(u)) {
            allResearch.add(u);
        }
        availableResearch.put(u.researchName, u);
    }

    private static void defineAndAddResearch(final String researchName, final int duration, final int cost, final String description) {
        addResearchToListAndMap(new Research(researchName, duration, cost, description));
    }

    static {
        availableResearch = new HashMap<>();
        allResearch = new ArrayList<>();
        defineAndAddResearch("Reconnaissance", 10, 50, "Unlocks Reconnaissance Robots");
        defineAndAddResearch("Tank", 60, 600, "Unlocks Tanks");
        defineAndAddResearch("Sniper", 40, 400, "Unlocks Sniper Robots");
    }
}
