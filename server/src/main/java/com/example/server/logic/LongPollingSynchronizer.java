package com.example.server.logic;

import com.example.server.Utility.Pair;

import java.util.List;

public class LongPollingSynchronizer<T> {

    private boolean waiting;
    private int waitingThreads;
    private T data;
    private long dataVersion;

    public LongPollingSynchronizer() {
        waiting = true;
        waitingThreads = 0;
        data = null;
        dataVersion = 0;
    }

    public Pair<T, Long> acquireData(final long prevVersion) {
        synchronized (this) {
            if (prevVersion < dataVersion) {
                return new Pair<T, Long>(data, dataVersion);
            }
            waitingThreads++;
            while (waiting) {
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    waitingThreads--;
                    return new Pair<T, Long>(data, dataVersion);
                }
            }
            waitingThreads--;
            return new Pair<T, Long>(data, dataVersion);
        }
    }

    public void giveData(final T data) {
        synchronized (this) {
            waiting = false;
            this.data = data;
            dataVersion++;
            this.notifyAll();
        }
        while (waitingThreads > 0) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException ignored) {

            }
        }
        synchronized (this) {
            waiting = true;
        }
    }
}
