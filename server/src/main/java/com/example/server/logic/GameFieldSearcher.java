package com.example.server.logic;

import com.example.server.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class GameFieldSearcher {
    public static List<Unit> findAllTargetsInRange(final StatUnit unit) {
        List<Unit> potentialTargets = getAllEnemyUnits(unit);
        int range = unit.getRange();
        Cell pos = unit.getCell();
        boolean[][] currentlyVisible = unit.getPlayer().getCachedCurrentlySeenCells();
        Predicate<Unit> pred = (s) -> s.getCell().getDistance(pos) <= range && currentlyVisible[s.getCell().y][s.getCell().x];
        return potentialTargets.stream().filter(pred).collect(Collectors.toList());
    }

    public static Unit getClosestValidTarget(final StatUnit unit, final boolean mainRobotsOnly) {
        List<Unit> potentialTargets = getAllEnemyUnits(unit);
        int lowestDistance = unit.getRange() + 1;
        Unit closest = null;
        boolean[][] currentlyVisible = unit.getPlayer().getCachedCurrentlySeenCells();
        for (Unit u : potentialTargets) {
            Cell cell = u.getCell();
            int distance = cell.getDistance(unit.getCell());
            if (currentlyVisible[cell.y][cell.x] && distance < lowestDistance && (!mainRobotsOnly || u instanceof MainRobot)) {
                closest = u;
                lowestDistance = distance;
            }
        }
        return closest;
    }

    private static List<Unit> getAllEnemyUnits(final Unit unit) {
        List<Unit> potentialTargets = new ArrayList<>();
        for (Player p : unit.getCell().getGameField().getGame().getPlayers()) {
            if (p != unit.getPlayer()) {
                potentialTargets.addAll(p.getUnits());
            }
        }
        Predicate<Unit> pred = (s) -> s.isAlive();
        return potentialTargets.stream().filter(pred).collect(Collectors.toList());
    }

    public static Cell getFreeNeighbour(final Cell cell) {
        Cell[][] field = cell.getGameField().getField();
        int cX = cell.x;
        int cY = cell.y;
        for (int x = cX - 1; x <= cX + 1; x++) {
            for (int y = cY - 1; y <= cY + 1; y++) {
                if (x < 0 || y < 0 || x >= field[0].length || y >= field.length || (x == cX && y == cY)) {
                    continue;
                }
                Cell neigh = field[y][x];
                if (neigh.isFree()) {
                    return neigh;
                }
            }
        }
        return null;
    }

    public static Unit getClosestVisibleEnemy(final Unit unit, final boolean mainRobotsOnly) {
        List<Unit> potentialTargets = getAllEnemyUnits(unit);
        int lowestDistance = Integer.MAX_VALUE;
        Unit closest = null;
        boolean[][] currentlyVisible = unit.getPlayer().getCachedCurrentlySeenCells();
        for (Unit u : potentialTargets) {
            Cell cell = u.getCell();
            int distance = cell.getDistance(unit.getCell());
            if (currentlyVisible[cell.y][cell.x] && distance < lowestDistance && (!mainRobotsOnly || u instanceof MainRobot)) {
                closest = u;
                lowestDistance = distance;
            }
        }
        return closest;
    }

    public static Cell getClosestUndiscoveredCell(final StatUnit unit) {
        //TODO probably too slow for heavy load scenarios
        boolean[][] onceSeen = unit.getPlayer().getSeenCells();
        int sizeX = onceSeen[0].length;
        int sizeY = onceSeen.length;
        //TODO mathcheck
        int maxXNeeded = (int) Math.ceil(sizeX + (Math.abs(unit.getCell().x - sizeX / 2) / (sizeX / 2)) * sizeX);
        int maxYNeeded = (int) Math.ceil(sizeY + (Math.abs(unit.getCell().y - sizeY / 2) / (sizeY / 2)) * sizeY);
        int dx = 1;
        int dy = 0;
        int segmentLength = 1;
        int x = unit.getCell().x;
        int y = unit.getCell().y;
        int segmentPassed = 0;
        while (true) {
            x += dx;
            y += dy;
            if (x >= 0 && y >= 0 && x < sizeX && y < sizeY && !onceSeen[y][x]) {
                return unit.getCell().getGameField().getCell(x, y);
            }
            ++segmentPassed;
            if (segmentPassed == segmentLength) {
                segmentPassed = 0;
                int buffer = dx;
                dx = -dy;
                dy = buffer;
                //TODO actually do proper math and stop sucking
                if (segmentLength > maxXNeeded * 1.2 && segmentLength > maxYNeeded * 1.2) {
                    break;
                }
                if (dy == 0) {
                    ++segmentLength;
                }
            }
        }
        return null;
    }
}
