package com.example.server.logic;

import com.example.server.model.Player;

import java.util.HashMap;
import java.util.Map;

public class PlayerIdentityMapper {

    private static PlayerIdentityMapper instance = new PlayerIdentityMapper();
    private Map<String, Player> identityMap;

    private PlayerIdentityMapper() {
        identityMap = new HashMap<>();
    }

    ;

    public static PlayerIdentityMapper getInstance() {
        return instance;
    }

    public synchronized boolean addPlayerIdentity(final Player player) {
        if (!identityMap.containsValue(player)) {
            identityMap.put(player.getIdentification(), player);
            return true;
        }
        return false;
    }

    //if this needs to be called for any reason that does not involve direct
    //player action (involving a http request), this needs to be refactored.
    public synchronized Player getPlayer(final String identity) {
        Player p = identityMap.get(identity);
        if (p == null) {
            return null;
        }
        p.actionTaken();
        return p;
    }

    public synchronized void removePlayer(final Player player) {
        identityMap.remove(player.getIdentification());
    }
}
