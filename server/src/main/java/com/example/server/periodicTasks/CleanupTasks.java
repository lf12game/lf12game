package com.example.server.periodicTasks;

import com.example.server.logic.GameManager;
import com.example.server.logic.PlayerIdentityMapper;
import com.example.server.model.Game;
import com.example.server.model.Player;

public class CleanupTasks {

    public static void cleanOldConnections() {
        for (Game g : GameManager.getInstance().getAllGames()) {
            for (Player p : g.getPlayers()) {
                //TODO constant
                if (p.getSecondsSinceLastAction() >= 120) {
                    if (!g.hasStarted()) {
                        PlayerIdentityMapper.getInstance().removePlayer(p);
                        g.removePlayer(p);
                    } else {
                        p.killAll(true);
                        PlayerIdentityMapper.getInstance().removePlayer(p);
                        g.removePlayer(p);
                    }
                }
            }
        }
    }
}
