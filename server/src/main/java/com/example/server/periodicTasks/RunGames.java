package com.example.server.periodicTasks;

import com.example.server.logic.GameManager;
import com.example.server.logic.PlayerIdentityMapper;
import com.example.server.model.Game;
import com.example.server.model.Player;
import org.springframework.scheduling.annotation.Scheduled;

public class RunGames {

    private static long lastUpdate = System.nanoTime();

    public static void runGames() {
        long timePassed = System.nanoTime() - lastUpdate;
        lastUpdate = System.nanoTime();
        for (Game g : GameManager.getInstance().getAllGames()) {
            g.timePassed(timePassed / 1000000);
        }
    }
}
