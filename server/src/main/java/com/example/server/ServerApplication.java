package com.example.server;

import com.example.server.periodicTasks.CleanupTasks;
import com.example.server.periodicTasks.RunGames;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@EnableScheduling
@SpringBootApplication
public class ServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication.class, args);
    }

    //TODO move into configuration?
    @Scheduled(fixedRate = 5000L)
    public void cleanOldConnections() {
        CleanupTasks.cleanOldConnections();
    }

    @Scheduled(fixedRate = 100L)
    public void runGames() {
        RunGames.runGames();
    }


}
