package com.example.server.dto.outbound;

import lombok.Data;

@Data
public class BuildingResultDTO {
    private int mainRobotX;
    private int mainRobotY;
    private int newResources;
    private int buildingX;
    private int buildingY;
}
