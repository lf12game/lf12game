package com.example.server.dto.outbound;

import com.example.server.model.RobotProductionBuilding;
import lombok.Data;

@Data
public class RobotProductionResultDTO {
    private int newResources;
    private RobotProductionBuilding productionBuildingState;
    private int buildingX;
    private int buildingY;
}
