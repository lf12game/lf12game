package com.example.server.dto.inbound;

import com.example.server.model.AutomatedCommand;
import lombok.Data;

@Data
public class ChangeAutomatedCommandsDTO {
    private String identification;
    private int x;
    private int y;
    private AutomatedCommand rootCommand;
}
