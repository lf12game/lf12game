package com.example.server.dto.inbound;

import lombok.Data;

@Data
public class MoveMainRobotDTO {
    private String identification;
    private int x;
    private int y;
}
