package com.example.server.dto.inbound;

import lombok.Data;

@Data
public class AbortUpgradeDTO {
    private String identification;
    private int x;
    private int y;
}
