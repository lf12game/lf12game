package com.example.server.dto.inbound;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class StartGameDTO {
    @NotBlank(message = "Game name is mandatory.")
    private String gameName;
    @NotBlank(message = "Identification is needed.")
    private String identification;
}
