package com.example.server.dto.outbound;

import com.example.server.model.AutomatedCommand;
import lombok.Data;

@Data
public class AutomatedCommandsResultDTO {
    private AutomatedCommand rootCommand;
    private int buildingX;
    private int buildingY;
}
