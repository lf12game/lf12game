package com.example.server.dto.outbound;

import com.example.server.model.Cell;
import com.example.server.model.enums.GameResult;
import lombok.Data;

import java.util.List;

@Data
public class GetGameUpdateDTO {
    private Cell[][] gameField;
    private boolean[][] visible;
    private List<String> playerNames;
    private int resources;
    private long version;
    private GameResult gameResult;
    private List<String> finishedResearch;
}
