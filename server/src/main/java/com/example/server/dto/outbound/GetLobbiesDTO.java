package com.example.server.dto.outbound;

import lombok.Data;

import java.util.List;

@Data
public class GetLobbiesDTO {
    private List<String> lobbyNames;
    private long version;
}
