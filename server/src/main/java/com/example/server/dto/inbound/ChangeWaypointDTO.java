package com.example.server.dto.inbound;

import lombok.Data;

@Data
public class ChangeWaypointDTO {
    private String identification;
    private int x;
    private int y;
    private int index;
}
