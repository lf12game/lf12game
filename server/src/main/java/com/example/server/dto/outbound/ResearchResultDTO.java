package com.example.server.dto.outbound;

import com.example.server.model.ResearchBuilding;
import com.example.server.model.StatUnit;
import lombok.Data;

@Data
public class ResearchResultDTO {
    private int newResources;
    private ResearchBuilding researchBuildingState;
    private int researchX;
    private int researchY;
}
