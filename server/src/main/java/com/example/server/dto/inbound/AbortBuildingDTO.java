package com.example.server.dto.inbound;

import lombok.Data;

@Data
public class AbortBuildingDTO {
    private String identification;
}
