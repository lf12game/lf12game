package com.example.server.dto.inbound;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class JoinGameDTO {
    @NotBlank(message = "Game name is mandatory.")
    private String gameName;
    @NotBlank(message = "Player name is mandatory.")
    private String playerName;
    private String password;
}
