package com.example.server.dto.inbound;

import lombok.Data;

@Data
public class ChangeRepeatProductionDTO {
    private String identification;
    private int x;
    private int y;
    private boolean repeatProduction;
}
