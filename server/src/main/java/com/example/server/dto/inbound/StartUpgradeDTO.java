package com.example.server.dto.inbound;

import lombok.Data;

@Data
public class StartUpgradeDTO {
    private String identification;
    private int x;
    private int y;
    private String upgradeName;
}
