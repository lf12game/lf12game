package com.example.server.dto.outbound;

import lombok.Data;

import java.awt.*;

@Data
public class GetMainRobotMoveDTO {
    private int x;
    private int y;
}
