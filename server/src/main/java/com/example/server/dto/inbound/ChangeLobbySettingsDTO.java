package com.example.server.dto.inbound;

import lombok.Data;

@Data
public class ChangeLobbySettingsDTO {
    private String identification;
    private int x;
    private int y;
    private int startResources;
    private int updateFrequency;
    private int blockerFrequency;
    private int resourceFrequency;
}
