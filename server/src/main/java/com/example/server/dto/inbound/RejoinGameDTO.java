package com.example.server.dto.inbound;

import lombok.Data;

@Data
public class RejoinGameDTO {
    private String identification;
}
