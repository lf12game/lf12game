package com.example.server.dto.outbound;

import com.example.server.model.StatUnit;
import lombok.Data;

@Data
public class UpgradeResultDTO {
    private int newResources;
    private StatUnit unitUpgradeState;
    private int upgradeX;
    private int upgradeY;
}
