package com.example.server.dto.outbound;

import lombok.Data;

import java.awt.Point;
import java.util.List;

@Data
public class RejoinGameResultDTO {
    private GetLobbyUpdateDTO lobbyInfo;
    private List<Point> waypoints;
    private String playerName;
    private GetGameUpdateDTO gameUpdate;
}
