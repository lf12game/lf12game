package com.example.server.dto.inbound;

import lombok.Data;

@Data
public class BuildRobotDTO {
    private String identification;
    private int x;
    private int y;
    private String robotType;
}
