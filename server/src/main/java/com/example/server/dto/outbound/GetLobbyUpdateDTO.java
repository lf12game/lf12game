package com.example.server.dto.outbound;

import com.example.server.model.Research;
import com.example.server.model.Upgrade;
import lombok.Data;

import java.util.List;

@Data
public class GetLobbyUpdateDTO {
    private String playerIdentification;
    private String gameName;
    private List<String> playerNames;
    private boolean started;
    private long version;
    private int x;
    private int y;
    private int startResources;
    private int updateFrequency;
    private int blockerFrequency;
    private int resourceFrequency;
    private List<Upgrade> allUpgrades;
    private List<Research> allResearch;
}
