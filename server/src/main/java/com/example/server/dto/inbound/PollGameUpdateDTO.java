package com.example.server.dto.inbound;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class PollGameUpdateDTO {
    @NotBlank(message = "Identification is needed.")
    private String identification;
    private long prevVersion;
}
