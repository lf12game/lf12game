package com.example.server.model.enums;

public enum Terrain {
    DEFAULT(true),
    BLOCKER(false),
    UNDISCOVERED(true),
    RESOURCE(false);

    public boolean passable;

    Terrain(boolean passable) {
        this.passable = passable;
    }

}
