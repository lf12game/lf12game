package com.example.server.model;

import com.example.server.model.enums.UnitActionType;
import com.example.server.model.enums.UnitEventType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.awt.*;

public abstract class Unit {
    private Player player;
    @JsonIgnore
    private Cell cell;
    public final long id;
    @JsonIgnore
    private static long idCounter = 0;

    public Unit(final Player player, final Cell cell) {
        this.player = player;
        this.cell = cell;
        this.id = ++idCounter;
    }

    @JsonIgnore
    public abstract boolean isAlive();

    public Player getPlayer() {
        return player;
    }

    public Cell getCell() {
        return cell;
    }

    void setCell(final Cell cell) {
        this.cell = cell;
    }

    public void killUnit() {
        if (player == null && cell == null) {
            return;
        }
        getPlayer().removeUnit(this);
        Cell c = getCell();
        if (c.getUnit() == this) {
            c.setUnit(null, true, true);
        }
        c.addTurnEvent(new UnitEvent(UnitEventType.DEATH, new Point(c.x, c.y), null));
        player = null;
        cell = null;
    }

    public abstract void act(final UnitActionType type);


}
