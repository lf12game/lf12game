package com.example.server.model;

import com.example.server.logic.GameFieldSearcher;
import com.example.server.logic.Pathfinding;
import com.example.server.model.enums.UnitActionType;

import java.awt.*;

public class MainRobot extends StatUnit {

    private Building currentConstruction;
    private Point currentMoveOrder;
    private double productionSpeedMult;

    private MainRobot(final Player player, final Cell cell, final int maxHP, final int speed, final int damage,
                      final int range, final int vision, final int armor, final int armorPiercing) {
        super(player, cell, maxHP, speed, damage, range, vision, armor, armorPiercing, "MainRobot");
        this.currentConstruction = null;
        this.currentMoveOrder = null;
        this.productionSpeedMult = 1.0;
    }

    public void setCurrentConstruction(final Building construction) {
        this.currentConstruction = construction;
    }

    public void setCurrentMoveOrders(final Point moveOrder) {
        this.currentMoveOrder = moveOrder;
    }

    public Point getCurrentMoveOrders() {
        return currentMoveOrder;
    }

    public Building getCurrentConstruction() {
        return currentConstruction;
    }

    @Override
    public void killUnit() {
        getPlayer().killAll(false);
        super.killUnit();
    }

    @Override
    protected void applyUpgrade(final Upgrade finishedUpgrade) {
        super.applyUpgrade(finishedUpgrade);
        productionSpeedMult *= finishedUpgrade.productionSpeedMult;
    }

    @Override
    public void act(final UnitActionType type) {
        super.act(type);
        Cell cur = getCell();
        if (type == UnitActionType.MOVE && currentMoveOrder != null && (currentMoveOrder.x != cur.x || currentMoveOrder.y != cur.y) &&
                (currentConstruction == null || (currentConstruction.getCell() != null && getCell().getDistance(currentConstruction.getCell()) > 1))) {
            //TODO move maxConsideredCells to config?
            Pathfinding.pathFind(this, currentMoveOrder, 200);
        }
        if (type == UnitActionType.OTHER && currentConstruction != null) {
            if (!currentConstruction.isAlive()) {
                currentConstruction = null;
                currentMoveOrder = new Point(getCell().x, getCell().y);
            } else if (getCell().getDistance(currentConstruction.getCell()) <= 1) {
                if (currentConstruction.getCell().hasUnit() && currentConstruction.getCell().getUnit() != currentConstruction) {
                    currentConstruction.abort();
                    currentConstruction = null;
                    currentMoveOrder = new Point(getCell().x, getCell().y);
                } else {
                    if (currentConstruction.getPassedDuration() == 0) {
                        getPlayer().addUnit(currentConstruction);
                        currentConstruction.getCell().setUnit(currentConstruction, false, true);
                    }
                    currentConstruction.buildTick(productionSpeedMult);
                    if (currentConstruction.isFinished()) {
                        currentConstruction = null;
                        currentMoveOrder = new Point(getCell().x, getCell().y);
                    }
                }
            }
        }
        if (type == UnitActionType.ATTACK) {
            Unit target = GameFieldSearcher.getClosestValidTarget(this, false);
            if (target != null) {
                dealDamage((StatUnit) target);
            }
        }
    }

    public static MainRobot generateMainRobot(final Player player, final Cell cell) {
        //TODO move into constants?
        return new MainRobot(player, cell, 1000, 1, 20, 5, 10, 10, 1000);
    }
}
