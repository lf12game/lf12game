package com.example.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class AutomatedCommand {
    private String name;
    private String uid;
    private List<List<AutomatedCommand>> children;
    private List<String> options;
    private String selectedOption;
    @JsonIgnore
    private AutomatedCommand parent;
    @JsonIgnore
    private List<AutomatedCommand> parentCommandList;
    @JsonIgnore
    private int currentCommandIndex;

    public AutomatedCommand(final String name, final String uid, final List<List<AutomatedCommand>> children, final List<String> options, final String selectedOption) {
        this.name = name;
        this.uid = uid;
        this.children = children;
        this.options = options;
        this.selectedOption = selectedOption;
        this.parent = null;
        this.parentCommandList = null;
        this.currentCommandIndex = -1;
    }

    public List<String> getOptions() {
        return options;
    }

    public AutomatedCommand getParent() {
        return parent;
    }

    public String getName() {
        return name;
    }

    public String getUid() {
        return uid;
    }

    public String getSelectedOption() {
        return selectedOption;
    }

    public List<List<AutomatedCommand>> getChildren() {
        return children;
    }

    @JsonIgnore
    public List<AutomatedCommand> getParentCommandList() {
        return parentCommandList;
    }

    @JsonIgnore
    public int getCurrentCommandIndex() {
        return currentCommandIndex;
    }

    public void initializeSupportingDataStructures() {
        for (List<AutomatedCommand> l : children) {
            for (int x = 0; x < l.size(); x++) {
                AutomatedCommand c = l.get(x);
                c.currentCommandIndex = x;
                c.parentCommandList = l;
                c.parent = this;
                c.initializeSupportingDataStructures();
            }
        }
    }

    public AutomatedCommand clone() {
        AutomatedCommand newC = new AutomatedCommand("", "", null, null, "");
        newC.name = new String(this.name);
        newC.uid = new String(this.uid);
        newC.selectedOption = new String(this.selectedOption);
        newC.options = new ArrayList<>(this.options);
        newC.parent = null;
        newC.children = new ArrayList<>();
        for (List<AutomatedCommand> old : this.children) {
            ArrayList<AutomatedCommand> l = new ArrayList<>();
            for (AutomatedCommand c : old) {
                l.add(c.clone());
            }
            newC.children.add(l);
        }
        return newC;
    }
}
