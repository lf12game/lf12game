package com.example.server.model;

public class GameSettings {

    private int x;
    private int y;
    private int startResources;
    private int updateFrequency;
    private int blockerFrequencyPerHundred;
    private int resourceFrequencyPerThousand;

    public GameSettings(int x, int y, int startResources, int updateFrequency, int blockerFrequencyPerHundred,
                        int ressourceFrequencyPerThousand) {
        this.x = x;
        this.y = y;
        this.startResources = startResources;
        this.updateFrequency = updateFrequency;
        this.blockerFrequencyPerHundred = blockerFrequencyPerHundred;
        this.resourceFrequencyPerThousand = ressourceFrequencyPerThousand;
    }

    public GameSettings() {
        this.x = 100;
        this.y = 47;
        this.startResources = 500;
        this.updateFrequency = 5000;
        this.blockerFrequencyPerHundred = 20;
        this.resourceFrequencyPerThousand = 3;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getStartResources() {
        return startResources;
    }

    public int getUpdateFrequency() {
        return updateFrequency;
    }

    public int getBlockerFrequencyPerHundred() {
        return blockerFrequencyPerHundred;
    }

    public int getResourceFrequencyPerThousand() {
        return resourceFrequencyPerThousand;
    }
}
