package com.example.server.model;

import com.example.server.model.enums.UnitEventType;

import java.awt.*;

public class UnitEvent {
    public final UnitEventType type;
    public final Point source;
    public final Point destination;

    public UnitEvent(final UnitEventType type, final Point source, final Point destination) {
        this.type = type;
        this.source = source;
        this.destination = destination;
    }
}
