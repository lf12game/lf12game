package com.example.server.model;

import java.util.ArrayList;
import java.util.List;

public class Upgrade {

    public final String upgradeName;
    public final int maxHP;
    public final int speed;
    public final int damage;
    public final int range;
    public final int vision;
    public final int armor;
    public final int armorPiercing;
    public final int resourcesPerTick;
    public final double productionSpeedMult;

    public final int duration;
    public final int cost;

    public final List<String> prerequisites;
    public final String upgradeType;
    public final String description;

    public Upgrade(final String upgradeName, final int maxHp, final int speed, final int damage, final int range, final int vision, final int armor, final int armorPiercing,
                   final int resourcesPerTick, final double productionSpeedMult, final int duration, final int cost, final List<String> prerequisites, final String upgradeType,
                   final String description) {
        this.upgradeName = upgradeName;
        this.maxHP = maxHp;
        this.speed = speed;
        this.damage = damage;
        this.range = range;
        this.vision = vision;
        this.armor = armor;
        this.armorPiercing = armorPiercing;
        this.resourcesPerTick = resourcesPerTick;
        this.productionSpeedMult = productionSpeedMult;
        this.duration = duration;
        this.cost = cost;
        this.prerequisites = new ArrayList<>(prerequisites);
        this.upgradeType = upgradeType;
        this.description = description;
    }
}
