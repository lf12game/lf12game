package com.example.server.model;

import com.example.server.model.enums.Terrain;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class Cell {
    public final int x;
    public final int y;
    private Terrain terrain;
    private Unit unit;
    @JsonIgnore
    private GameField gameField;
    private List<UnitEvent> turnEvents;

    public Cell(final GameField gameField, final int x, final int y) {
        this(gameField, x, y, Terrain.DEFAULT);
    }

    public Cell(final GameField gameField, final int x, final int y, final Terrain terrain) {
        this(gameField, x, y, terrain, null);
    }

    public Cell(final GameField gameField, final int x, final int y, final Terrain terrain, final Unit unit) {
        this.x = x;
        this.y = y;
        this.terrain = terrain;
        this.unit = unit;
        this.gameField = gameField;
        this.turnEvents = new ArrayList<>();
    }

    public void setTurnEvents(final List<UnitEvent> events) {
        turnEvents = events;
    }

    public List<UnitEvent> getTurnEvents() {
        return new ArrayList<>(turnEvents);
    }

    public void resetTurnEvents() {
        turnEvents = new ArrayList<>();
    }

    public void addTurnEvent(final UnitEvent e) {
        turnEvents.add(e);
    }

    public int getDistance(final Cell other) {
        return Math.max(Math.abs(other.x - x), Math.abs(other.y - y));
    }

    public GameField getGameField() {
        return gameField;
    }

    public Unit getUnit() {
        return unit;
    }

    public boolean setUnit(final Unit unit, final boolean dontSetUnit, final boolean terrainOveride) {
        if (!terrain.passable && !terrainOveride) {
            return false;
        }
        this.unit = unit;
        if (!dontSetUnit) {
            unit.setCell(this);
        }
        return true;
    }

    public boolean moveUnit(final Cell targetCell) {
        if (!targetCell.isFree()) {
            return false;
        }
        if (targetCell.setUnit(unit, false, false)) {
            unit.setCell(targetCell);
            unit = null;
            return true;
        }
        return false;
    }

    @JsonIgnore
    public boolean isFree() {
        return unit == null && terrain.passable;
    }

    public Terrain getTerrain() {
        return terrain;
    }

    public void setTerrain(final Terrain terrain) {
        this.terrain = terrain;
    }

    public boolean hasUnit() {
        return unit != null;
    }

}
