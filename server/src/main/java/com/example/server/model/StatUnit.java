package com.example.server.model;

import com.example.server.logic.UpgradeManager;
import com.example.server.model.enums.UnitActionType;
import com.example.server.model.enums.UnitEventType;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public abstract class StatUnit extends Unit {

    private int maxHP;
    private int currentHP;
    private int speed;
    private int damage;
    private int range;
    private int vision;
    private int armor;
    private int armorPiercing;
    private String unitType;
    private List<Upgrade> upgrades;
    private Upgrade currentUpgrade;
    private int passedUpgradeDuration;

    public StatUnit(final Player player, final Cell cell, final int maxHP, final int speed, final int damage,
                    final int range, final int vision, final int armor, final int armorPiercing, final String unitType) {
        super(player, cell);
        this.maxHP = maxHP;
        this.currentHP = maxHP;
        this.speed = speed;
        this.damage = damage;
        this.range = range;
        this.vision = vision;
        this.armor = armor;
        this.armorPiercing = armorPiercing;
        this.unitType = unitType;
        this.upgrades = new ArrayList<>();
        this.currentUpgrade = null;
        this.passedUpgradeDuration = 0;
    }

    protected void applyUpgrade(final Upgrade finishedUpgrade) {
        maxHP += finishedUpgrade.maxHP;
        currentHP += finishedUpgrade.maxHP;
        speed += finishedUpgrade.speed;
        damage += finishedUpgrade.damage;
        range += finishedUpgrade.range;
        vision += finishedUpgrade.vision;
        armor += finishedUpgrade.armor;
        armorPiercing += finishedUpgrade.armorPiercing;
    }

    public boolean mayStartUpgrade(final Upgrade newUpgrade) {
        if (currentUpgrade != null || upgrades.contains(newUpgrade)) {
            return false;
        }
        return UpgradeManager.mayStartUpgrade(this, newUpgrade);
    }

    public void startNewUpgrade(final Upgrade newUpgrade) {
        if (!mayStartUpgrade(newUpgrade)) {
            return;
        }
        getPlayer().changeResources(-newUpgrade.cost);
        passedUpgradeDuration = 0;
        currentUpgrade = newUpgrade;
    }

    public boolean abortCurrentUpgrade() {
        if (currentUpgrade == null) {
            return false;
        }
        getPlayer().changeResources(currentUpgrade.cost);
        currentUpgrade = null;
        return true;
    }

    @Override
    public void act(final UnitActionType type) {
        if (type == UnitActionType.OTHER && currentUpgrade != null) {
            passedUpgradeDuration++;
            if (currentUpgrade.duration <= passedUpgradeDuration) {
                applyUpgrade(currentUpgrade);
                upgrades.add(currentUpgrade);
                currentUpgrade = null;
                passedUpgradeDuration = 0;
            }
        }
    }

    public List<Upgrade> getUpgrades() {
        return new ArrayList<>(upgrades);
    }

    public Upgrade getCurrentUpgrade() {
        return currentUpgrade;
    }

    public int getPassedUpgradeDuration() {
        return passedUpgradeDuration;
    }

    public String getUnitType() {
        return unitType;
    }

    private boolean damage(final int damage) {
        currentHP -= damage;
        if (currentHP <= 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isAlive() {
        return currentHP > 0;
    }

    public boolean dealDamage(final StatUnit target) {
        int dmg = Math.max(1, damage - (Math.max(0, target.armor - armorPiercing)));
        Cell origin = getCell();
        Cell destination = target.getCell();
        UnitEvent e = new UnitEvent(UnitEventType.ATTACK, new Point(origin.x, origin.y), new Point(destination.x, destination.y));
        origin.addTurnEvent(e);
        destination.addTurnEvent(e);
        return target.damage(dmg);
    }

    public int getMaxHP() {
        return maxHP;
    }

    public int getCurrentHP() {
        return currentHP;
    }

    public int getSpeed() {
        return speed;
    }

    public int getDamage() {
        return damage;
    }

    public int getRange() {
        return range;
    }

    public int getVision() {
        return vision;
    }

    public int getArmor() {
        return armor;
    }

    public int getArmorPiercing() {
        return armorPiercing;
    }
}
