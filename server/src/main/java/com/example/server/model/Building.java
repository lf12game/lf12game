package com.example.server.model;

import com.example.server.logic.GameFieldSearcher;
import com.example.server.model.enums.Terrain;
import com.example.server.model.enums.UnitActionType;

public class Building extends StatUnit {

    private int buildDuration;
    private int passedDuration;
    private int resourcesPerTick;
    private int buildCost;

    public Building(final Player player, final Cell cell, final int maxHP, final int speed, final int damage,
                    final int range, final int vision, final int armor, final int armorPiercing, final int buildDuration,
                    final int resourcesPerTick, final int buildCost, final String unitType) {
        super(player, cell, maxHP, speed, damage, range, vision, armor, armorPiercing, unitType);
        this.buildDuration = buildDuration;
        this.passedDuration = 0;
        this.resourcesPerTick = resourcesPerTick;
        this.buildCost = buildCost;
    }

    //TODO more of these for turrets etc
    @Override
    public int getVision() {
        if (!isFinished()) {
            return 0;
        }
        return super.getVision();
    }

    @Override
    public int getRange() {
        if (!isFinished()) {
            return 0;
        }
        return super.getRange();
    }

    @Override
    public int getDamage() {
        if (!isFinished()) {
            return 0;
        }
        return super.getDamage();
    }

    @Override
    public int getArmor() {
        if (!isFinished()) {
            return 0;
        }
        return super.getArmor();
    }

    public int getBuildCost() {
        return buildCost;
    }

    public int getBuildDuration() {
        return buildDuration;
    }

    public int getPassedDuration() {
        return passedDuration;
    }

    public int getResourcesPerTick() {
        if (!isFinished()) {
            return 0;
        }
        return resourcesPerTick;
    }

    public boolean isFinished() {
        return buildDuration <= passedDuration;
    }

    public boolean abort() {
        if (isFinished()) {
            return false;
        }
        getPlayer().changeResources((int) Math.floor(buildCost * (((double) getCurrentHP()) / getMaxHP())));
        getPlayer().removeUnit(this);
        if (getCell().getUnit() == this) {
            getCell().setUnit(null, true, true);
        }
        return true;
    }

    @Override
    protected void applyUpgrade(final Upgrade finishedUpgrade) {
        super.applyUpgrade(finishedUpgrade);
        resourcesPerTick += finishedUpgrade.resourcesPerTick;
    }

    public static boolean buildingExists(final String buildingType) {
        if ("Resource".equals(buildingType)) {
            return true;
        }
        if ("Production".equals(buildingType)) {
            return true;
        }
        if ("Defence".equals(buildingType)) {
            return true;
        }
        if ("Radar".equals(buildingType)) {
            return true;
        }
        if ("Research".equals(buildingType)) {
            return true;
        }
        return false;
    }

    public void buildTick(final double productionSpeedMult) {
        passedDuration += productionSpeedMult;
        passedDuration = Math.min(passedDuration, buildDuration);
    }

    public static Building createBuilding(final Player player, final Cell cell, final String buildingType) {
        //TODO move into some kind of config

        if ("Resource".equals(buildingType)) {
            if (cell.getTerrain() != Terrain.RESOURCE || cell.getUnit() != null) {
                return null;
            }
            if (!player.hasResources(100)) {
                return null;
            }
            player.changeResources(-100);
            return new Building(player, cell, 100, 0, 0, 0, 5, 0, 0, 10, 1, 100, "Resource");
        }
        if ("Production".equals(buildingType)) {
            if (!cell.isFree()) {
                return null;
            }
            if (!player.hasResources(50)) {
                return null;
            }
            player.changeResources(-50);
            return new RobotProductionBuilding(player, cell, 250, 0, 0, 0, 5, 0, 0, 10, 0, 50, RobotProductionBuilding.getDefaultCommandSet(), "Production");
        }
        if ("Defence".equals(buildingType)) {
            if (!cell.isFree()) {
                return null;
            }
            if (!player.hasResources(100)) {
                return null;
            }
            player.changeResources(-100);
            return new Building(player, cell, 200, 0, 25, 5, 7, 10, 0, 10, 0, 100, "Defence");
        }
        if ("Radar".equals(buildingType)) {
            if (!cell.isFree()) {
                return null;
            }
            if (!player.hasResources(50)) {
                return null;
            }
            player.changeResources(-50);
            return new Building(player, cell, 100, 0, 0, 0, 20, 0, 0, 10, 0, 50, "Radar");
        }
        if ("Research".equals(buildingType)) {
            if (!cell.isFree()) {
                return null;
            }
            if (!player.hasResources(100)) {
                return null;
            }
            player.changeResources(-100);
            return new ResearchBuilding(player, cell, 200, 0, 0, 0, 5, 0, 0, 10, 0, 100, "Research");
        }
        return null;
    }

    @Override
    public boolean mayStartUpgrade(final Upgrade newUpgrade) {
        if (!isFinished()) {
            return false;
        }
        return super.mayStartUpgrade(newUpgrade);
    }

    @Override
    public void act(final UnitActionType type) {
        if (isFinished()) {
            super.act(type);
            if (type == UnitActionType.OTHER && resourcesPerTick != 0) {
                getPlayer().changeResources(resourcesPerTick);
            }
            if (type == UnitActionType.ATTACK && this.getDamage() > 0) {
                Unit target = GameFieldSearcher.getClosestValidTarget(this, false);
                if (target != null) {
                    dealDamage((StatUnit) target);
                }
            }
        }
    }
}
