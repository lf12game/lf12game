package com.example.server.model;

public class Research {
    public final String researchName;
    public final int duration;
    public final int cost;
    public final String description;

    public Research(final String researchName, final int duration, final int cost,
                    final String description) {
        this.researchName = researchName;
        this.duration = duration;
        this.cost = cost;
        this.description = description;
    }
}
