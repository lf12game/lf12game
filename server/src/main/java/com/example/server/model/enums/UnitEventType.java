package com.example.server.model.enums;

public enum UnitEventType {
    MOVE,
    ATTACK,
    DEATH;
}
