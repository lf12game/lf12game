package com.example.server.model.enums;

public enum GameResult {
    NONE,
    WIN,
    LOSE;
}
