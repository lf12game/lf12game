package com.example.server.model.enums;

public enum UnitActionType {
    MOVE,
    ATTACK,
    OTHER;
}
