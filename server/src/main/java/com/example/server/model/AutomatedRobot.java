package com.example.server.model;

import com.example.server.logic.GameFieldSearcher;
import com.example.server.logic.Pathfinding;
import com.example.server.model.enums.UnitActionType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

public class AutomatedRobot extends StatUnit {

    @JsonIgnore
    private AutomatedRobotCommands commands;
    private int buildDuration;
    private int passedDuration;
    private int buildCost;
    private boolean evaluatedCommandsThisturn;
    private AutomatedCommand evaluatedCommand;

    public AutomatedRobot(final Player player, final Cell cell, final int maxHP, final int speed, final int damage,
                          final int range, final int vision, final int armor, final int armorPiercing,
                          final int buildDuration, final int buildCost, final String unitType) {
        super(player, cell, maxHP, speed, damage, range, vision, armor, armorPiercing, unitType);
        this.commands = null;
        this.buildDuration = buildDuration;
        this.passedDuration = 0;
        this.buildCost = buildCost;
        this.evaluatedCommandsThisturn = false;
        this.evaluatedCommand = null;
    }

    public void resetTurnState() {
        evaluatedCommandsThisturn = false;
        evaluatedCommand = null;
    }

    public int getBuildDuration() {
        return buildDuration;
    }

    public int getPassedDuration() {
        return passedDuration;
    }

    public int getBuildCost() {
        return buildCost;
    }

    public void setCommands(final AutomatedCommand newRoot) {
        this.commands = new AutomatedRobotCommands(this, newRoot);
    }

    public boolean isFinished() {
        return buildDuration <= passedDuration;
    }

    public void buildTick(final double productionSpeedMult) {
        passedDuration += productionSpeedMult;
    }

    public boolean abort() {
        if (isFinished()) {
            return false;
        }
        getPlayer().changeResources(buildCost);
        return true;
    }

    @Override
    public void act(final UnitActionType type) {
        super.act(type);
        if (evaluatedCommandsThisturn) {
            if (evaluatedCommand != null) {
                executeActionCommand(evaluatedCommand, type);
            }
        } else {
            evaluatedCommandsThisturn = true;
            if (isFinished()) {
                AutomatedCommand c = commands.getNextAction();
                if (c != null) {
                    evaluatedCommand = c;
                    executeActionCommand(c, type);
                }
            }
        }
    }

    private void executeActionCommand(final AutomatedCommand c, final UnitActionType type) {
        switch (c.getName()) {
            case "Move":
                if (type != UnitActionType.MOVE) {
                    return;
                }
                break;
            case "Attack":
                if (type != UnitActionType.ATTACK) {
                    return;
                }
                break;
        }
        executeActionCommand(c);
    }

    private void executeActionCommand(final AutomatedCommand c) {
        Unit target = null;
        switch (c.getName()) {
            case "Move":
                Cell to = null;
                switch (c.getSelectedOption()) {
                    case "To nearest Enemy":
                        target = GameFieldSearcher.getClosestVisibleEnemy(this, false);
                        to = target != null ? target.getCell() : null;
                        break;
                    case "To Enemy Main Robot":
                        target = GameFieldSearcher.getClosestVisibleEnemy(this, true);
                        to = target != null ? target.getCell() : null;
                        break;
                    case "To Unexplored Terrain":
                        to = GameFieldSearcher.getClosestUndiscoveredCell(this);
                        break;
                    default:
                        String waypointStart = "To Waypoint ";
                        if (c.getSelectedOption().startsWith(waypointStart)) {
                            int index = Integer.parseInt(c.getSelectedOption().substring(waypointStart.length())) - 1;
                            Point waypoint = getPlayer().getWaypoint(index);
                            if (waypoint.x == -1 || waypoint.y == -1) {
                                to = null;
                            } else {
                                to = getCell().getGameField().getCell(waypoint.x, waypoint.y);
                            }
                        }
                        break;
                }
                //TODO move maxConsideredCells to config?
                if (to != null) {
                    Pathfinding.pathFind(this, new Point(to.x, to.y), 20);
                }
                break;
            case "Attack":
                switch (c.getSelectedOption()) {
                    case "Nearest Enemy":
                        target = GameFieldSearcher.getClosestValidTarget(this, false);
                        break;
                    case "Enemy Main Robot":
                        target = GameFieldSearcher.getClosestValidTarget(this, true);
                        break;
                }
                if (target != null) {
                    dealDamage((StatUnit) target);
                }
                break;
            case "NoOp":
                break;
            default:
        }
    }

    public static boolean robotExists(final String robotType) {
        if ("Basic".equals(robotType) || "Reconnaissance".equals(robotType) || "Tank".equals(robotType) || "Sniper".equals(robotType)) {
            return true;
        }
        return false;
    }

    public static boolean robotUnlocked(final Player player, final String robotType) {
        if ("Basic".equals(robotType)) {
            return true;
        }
        return player.hasFinishedResearch(robotType);
    }

    public static AutomatedRobot createRobot(final Player player, final Cell productionCell, final String robotType) {
        //TODO move into some kind of config
        if (!(productionCell.hasUnit() && productionCell.getUnit().getPlayer() == player && productionCell.getUnit() instanceof RobotProductionBuilding &&
                ((RobotProductionBuilding) productionCell.getUnit()).canStartRobotProduction())) {
            return null;
        }
        if ("Basic".equals(robotType)) {
            if (!player.hasResources(20)) {
                return null;
            }
            player.changeResources(-20);
            return new AutomatedRobot(player, null, 100, 2, 15, 2, 6, 0, 0, 10, 20, "Basic");
        }
        if ("Reconnaissance".equals(robotType)) {
            if (!player.hasResources(10)) {
                return null;
            }
            player.changeResources(-10);
            return new AutomatedRobot(player, null, 10, 4, 1, 1, 12, 0, 0, 5, 10, "Reconnaissance");
        }
        if ("Tank".equals(robotType)) {
            if (!player.hasResources(200)) {
                return null;
            }
            player.changeResources(-200);
            return new AutomatedRobot(player, null, 600, 2, 25, 3, 6, 10, 10, 60, 200, "Tank");
        }
        if ("Sniper".equals(robotType)) {
            if (!player.hasResources(100)) {
                return null;
            }
            player.changeResources(-100);
            return new AutomatedRobot(player, null, 100, 1, 25, 10, 10, 0, 0, 30, 100, "Sniper");
        }
        return null;
    }

    private class AutomatedRobotCommands {
        private AutomatedCommand rootCommand;
        private AutomatedCommand currentCommand;
        private Set<String> currentIterationCommands;
        private AutomatedRobot robot;

        public AutomatedRobotCommands(final AutomatedRobot robot, final AutomatedCommand rootCommand) {
            this.rootCommand = rootCommand.clone();
            this.rootCommand.initializeSupportingDataStructures();
            this.currentCommand = this.rootCommand;
            this.currentIterationCommands = null;
            this.robot = robot;
        }

        private AutomatedCommand getNextAction() {
            currentIterationCommands = new HashSet<>();
            while (true) {
                gotoNextCommand(false);
                if (currentIterationCommands.contains(currentCommand.getUid())) {
                    break;
                }
                currentIterationCommands.add(currentCommand.getUid());
                switch (currentCommand.getName()) {
                    case "NoOp":
                        break;
                    case "Move":
                    case "Attack":
                        return currentCommand;
                    default:
                }
            }
            return null;
        }

        private void gotoNextCommand(boolean ignoreIf) {
            //TODO expand for new commands/move into enum? + cleanup
            switch (currentCommand.getName()) {
                case "While true":
                    if (currentCommand.getChildren().get(0).size() >= 1) {
                        currentCommand = currentCommand.getChildren().get(0).get(0);
                    }
                    break;
                case "If":
                    if (ignoreIf) {
                        if (currentCommand.getParentCommandList().size() > currentCommand.getCurrentCommandIndex() + 1) {
                            currentCommand = currentCommand.getParentCommandList().get(currentCommand.getCurrentCommandIndex() + 1);
                        } else {
                            currentCommand = currentCommand.getParent();
                            gotoNextCommand(true);
                        }
                    } else if (evaluateConditional(currentCommand.getSelectedOption())) {
                        if (currentCommand.getChildren().get(0).size() >= 1) {
                            currentCommand = currentCommand.getChildren().get(0).get(0);
                        } else {
                            if (currentCommand.getParentCommandList().size() > currentCommand.getCurrentCommandIndex() + 1) {
                                currentCommand = currentCommand.getParentCommandList().get(currentCommand.getCurrentCommandIndex() + 1);
                            } else {
                                currentCommand = currentCommand.getParent();
                                gotoNextCommand(true);
                            }
                        }
                    } else {
                        if (currentCommand.getChildren().get(1).size() >= 1) {
                            currentCommand = currentCommand.getChildren().get(1).get(0);
                        } else {
                            if (currentCommand.getParentCommandList().size() > currentCommand.getCurrentCommandIndex() + 1) {
                                currentCommand = currentCommand.getParentCommandList().get(currentCommand.getCurrentCommandIndex() + 1);
                            } else {
                                currentCommand = currentCommand.getParent();
                                gotoNextCommand(true);
                            }
                        }
                    }
                    break;
                case "While":
                    if (evaluateConditional(currentCommand.getSelectedOption())) {
                        if (currentCommand.getChildren().get(0).size() >= 1) {
                            currentCommand = currentCommand.getChildren().get(0).get(0);
                        }
                    } else {
                        if (currentCommand.getParentCommandList().size() > currentCommand.getCurrentCommandIndex() + 1) {
                            currentCommand = currentCommand.getParentCommandList().get(currentCommand.getCurrentCommandIndex() + 1);
                        } else {
                            currentCommand = currentCommand.getParent();
                            gotoNextCommand(true);
                        }
                    }
                    break;
                case "Move":
                case "NoOp":
                case "Attack":
                    if (currentCommand.getParentCommandList().size() > currentCommand.getCurrentCommandIndex() + 1) {
                        currentCommand = currentCommand.getParentCommandList().get(currentCommand.getCurrentCommandIndex() + 1);
                    } else {
                        currentCommand = currentCommand.getParent();
                        gotoNextCommand(true);
                    }
                    break;
                case "Return":
                    currentCommand = rootCommand;
                    break;

            }
        }

        private boolean evaluateConditional(final String conditional) {
            //TODO expand/enum
            switch (conditional) {
                case "Enemy in Range":
                    return GameFieldSearcher.getClosestValidTarget(robot, false) != null;
                case "Enemy in Sight":
                    return GameFieldSearcher.getClosestVisibleEnemy(robot, false) != null;
                case "Enemy Main Robot in Sight":
                    return GameFieldSearcher.getClosestVisibleEnemy(robot, true) != null;
                case "Enemy Main Robot in Range":
                    return GameFieldSearcher.getClosestValidTarget(robot, true) != null;
                case "True":
                    return true;
                case "False":
                    return false;
                default:
                    String waypointStart = "Waypoint ";
                    String waypointEnd = "is set";
                    if (conditional.startsWith(waypointStart) && conditional.endsWith(waypointEnd)) {
                        int index = Integer.parseInt(conditional.substring(waypointStart.length(), conditional.length() - waypointEnd.length() - 1)) - 1;
                        Point waypoint = getPlayer().getWaypoint(index);
                        if (waypoint.x == -1 || waypoint.y == -1) {
                            return false;
                        }
                        return true;
                    }
                    break;
            }
            return false;
        }
    }
}
