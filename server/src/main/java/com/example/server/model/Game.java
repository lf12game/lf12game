package com.example.server.model;

import com.example.server.logic.FogOfWarGenerator;
import com.example.server.logic.GameManager;
import com.example.server.logic.LongPollingSynchronizer;
import com.example.server.logic.PlayerIdentityMapper;
import com.example.server.model.enums.UnitActionType;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Game {
    private String name;
    private List<Player> players;
    private boolean hasStarted;
    private boolean hasFinished;
    private String password;
    private LongPollingSynchronizer<List<String>> playerNamesSynchronizer;
    private LongPollingSynchronizer<Cell[][]> gameUpdateSynchronizer;
    private GameField gameField;
    private ReentrantReadWriteLock updateLock;
    private GameSettings gameSettings;
    private long accumulatedTime;
    private boolean singlePlayer;

    public final static int WAYPOINTSPERPLAYER = 3;

    public Game(final String name, final Player host, final String password) {
        this.name = name;
        hasStarted = false;
        hasFinished = false;
        playerNamesSynchronizer = new LongPollingSynchronizer<>();
        players = new ArrayList<>();
        addPlayer(host);
        this.password = password != null ? password.stripLeading() : "";
        this.gameUpdateSynchronizer = new LongPollingSynchronizer<>();
        this.gameField = null;
        this.updateLock = new ReentrantReadWriteLock(true);
        this.gameSettings = new GameSettings();
        this.accumulatedTime = 0;
        this.singlePlayer = false;
    }

    public GameSettings getGameSettings() {
        return gameSettings;
    }

    public void setGameSettings(GameSettings gameSettings) {
        this.gameSettings = gameSettings;
        playerNamesSynchronizer.giveData(getPlayerNames());
    }

    public boolean isSinglePlayer() {
        return singlePlayer;
    }

    public GameField getGameField() {
        return gameField;
    }

    public ReentrantReadWriteLock.ReadLock getUpdateLock() {
        return updateLock.readLock();
    }

    public LongPollingSynchronizer<List<String>> getPlayerNamesSynchronizer() {
        return playerNamesSynchronizer;
    }

    public LongPollingSynchronizer<Cell[][]> getGameUpdateSynchronizer() {
        return gameUpdateSynchronizer;
    }

    public boolean hasPassword() {
        return !password.isEmpty();
    }

    public boolean passwordMatch(final String password) {
        return !hasPassword() || this.password.equals(password);
    }

    public synchronized boolean playerNameUnused(final String playerName) {
        for (Player p : players) {
            if (p.getName().equals(playerName)) {
                return false;
            }
        }
        return true;
    }

    public String getName() {
        return name;
    }

    public boolean hasStarted() {
        return hasStarted;
    }

    public boolean hasFinished() {
        return hasFinished;
    }

    public synchronized boolean addPlayer(final Player player) {
        if (!hasStarted && !players.contains(player)) {
            if (players.add(player)) {
                player.setGame(this);
                playerNamesSynchronizer.giveData(getPlayerNames());
                return true;
            }
            return false;
        }
        return false;
    }

    public synchronized boolean removePlayer(final Player player) {
        if (players.remove(player)) {
            playerNamesSynchronizer.giveData(getPlayerNames());
            if (players.isEmpty()) {
                GameManager.getInstance().removeGame(this);
            }
            return true;
        }
        return false;
    }

    public synchronized List<Player> getPlayers() {
        return new ArrayList<>(players);
    }

    public synchronized List<String> getPlayerNames() {
        List<String> res = new ArrayList<>();
        for (Player p : players) {
            res.add(p.getName());
        }
        return res;
    }

    private void initializeGame() {
        //TODO move to constants
        int x = gameSettings.getX();
        int y = gameSettings.getY();
        int startResources = gameSettings.getStartResources();
        gameField = new GameField(x, y, this);
        for (Player p : players) {
            p.changeResources(startResources);
            p.initializeSeenCells(x, y);
            boolean[][] currentlySeenCells = FogOfWarGenerator.getVisibleCells(p);
            p.updateSeenCells(currentlySeenCells);
            p.setCachedCurrentlySeenCells(currentlySeenCells);
        }
    }

    public synchronized void timePassed(final long timePassed) {
        if (!hasStarted) {
            return;
        }
        accumulatedTime += timePassed;
        if (accumulatedTime >= gameSettings.getUpdateFrequency()) {
            accumulatedTime -= gameSettings.getUpdateFrequency();
            iterateGameState();
        }
    }

    public synchronized void startGame() {
        if (hasStarted) {
            return;
        }
        initializeGame();
        hasStarted = true;
        singlePlayer = players.size() == 1;
        GameManager.getInstance().getLobbyNamesSynchronizer().giveData(GameManager.getInstance().getLobbyNames());
        playerNamesSynchronizer.giveData(getPlayerNames());
        gameUpdateSynchronizer.giveData(gameField.getField());
    }

    public synchronized boolean isHost(final Player p) {
        return players.size() > 0 && players.get(0).equals(p);
    }

    public synchronized void iterateGameState() {
        if (!hasStarted) {
            return;
        }
        long startTime = System.nanoTime();
        updateLock.writeLock().lock();
        long startLockTime = System.nanoTime();
        for (Cell[] a : gameField.getField()) {
            for (Cell c : a) {
                c.resetTurnEvents();
            }
        }
        for (Player p : players) {
            for (Unit u : p.getUnits()) {
                u.act(UnitActionType.ATTACK);
            }
        }
        for (Player p : players) {
            for (Unit u : p.getUnits()) {
                if (!u.isAlive()) {
                    u.killUnit();
                }
            }
        }
        for (Player p : players) {
            for (Unit u : p.getUnits()) {
                u.act(UnitActionType.MOVE);
            }
        }
        for (Player p : players) {
            for (Unit u : p.getUnits()) {
                u.act(UnitActionType.OTHER);
            }
        }
        for (Player p : players) {
            boolean[][] currentlySeenCells = FogOfWarGenerator.getVisibleCells(p);
            p.updateSeenCells(currentlySeenCells);
            p.setCachedCurrentlySeenCells(currentlySeenCells);
        }
        for (Player p : players) {
            for (Unit u : p.getUnits()) {
                if (u instanceof AutomatedRobot) {
                    ((AutomatedRobot) u).resetTurnState();
                }
            }
        }
        gameUpdateSynchronizer.giveData(gameField.getField());
        if ((players.size() == 1 && !singlePlayer) || players.size() == 0) {
            for (Player p : getPlayers()) {
                PlayerIdentityMapper.getInstance().removePlayer(p);
                removePlayer(p);
            }
            GameManager.getInstance().removeGame(this);
        }
        updateLock.writeLock().unlock();
        long endTime = System.nanoTime();
        LoggerFactory.getLogger(Game.class).info("Iteration took:" + (endTime - startLockTime) / 1000000 + "(" + (endTime - startTime) / 1000000 + ")");
        ;
    }
}