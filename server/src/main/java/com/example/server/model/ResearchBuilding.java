package com.example.server.model;

import com.example.server.logic.ResearchManager;
import com.example.server.model.enums.UnitActionType;

public class ResearchBuilding extends Building {

    private Research currentResearch;
    private int passedResearchDuration;

    public ResearchBuilding(Player player, Cell cell, int maxHP, int speed, int damage, int range, int vision, int armor, int armorPiercing, int buildDuration, int resourcesPerTick, int buildCost, String unitType) {
        super(player, cell, maxHP, speed, damage, range, vision, armor, armorPiercing, buildDuration, resourcesPerTick, buildCost, unitType);
        currentResearch = null;
        passedResearchDuration = 0;
    }

    public boolean mayStartResearch(final Research newResearch) {
        if (currentResearch != null) {
            return false;
        }
        return ResearchManager.mayStartResearch(this, newResearch);
    }

    public void startNewResearch(final Research newResearch) {
        if (!mayStartResearch(newResearch)) {
            return;
        }
        getPlayer().changeResources(-newResearch.cost);
        passedResearchDuration = 0;
        currentResearch = newResearch;
    }

    public boolean abortCurrentResearch() {
        if (currentResearch == null) {
            return false;
        }
        getPlayer().changeResources(currentResearch.cost);
        currentResearch = null;
        return true;
    }

    @Override
    public void act(final UnitActionType type) {
        super.act(type);
        if (isFinished() && type == UnitActionType.OTHER && currentResearch != null) {
            passedResearchDuration++;
            if (currentResearch.duration <= passedResearchDuration) {
                getPlayer().addFinishedResearch(currentResearch.researchName);
                currentResearch = null;
                passedResearchDuration = 0;
            }
        }
    }

    public Research getCurrentResearch() {
        return currentResearch;
    }

    public int getPassedResearchDuration() {
        return passedResearchDuration;
    }
}
