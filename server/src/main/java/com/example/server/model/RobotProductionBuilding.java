package com.example.server.model;

import com.example.server.logic.GameFieldSearcher;
import com.example.server.model.enums.UnitActionType;

import java.util.ArrayList;
import java.util.List;

public class RobotProductionBuilding extends Building {

    private AutomatedCommand rootCommand;
    private AutomatedRobot currentProduction;
    private double productionSpeedMult;
    private boolean repeatProduction;

    public RobotProductionBuilding(final Player player, final Cell cell, final int maxHP, final int speed, final int damage,
                                   final int range, final int vision, final int armor, final int armorPiercing, final int buildDuration,
                                   final int resourcesPerTick, final int buildCost, final AutomatedCommand rootCommand, final String unitType) {
        super(player, cell, maxHP, speed, damage, range, vision, armor, armorPiercing, buildDuration, resourcesPerTick, buildCost, unitType);
        this.rootCommand = rootCommand;
        this.currentProduction = null;
        this.productionSpeedMult = 1.0;
        this.repeatProduction = false;
    }

    public AutomatedRobot getCurrentProduction() {
        return currentProduction;
    }

    public AutomatedCommand getRootCommand() {
        return rootCommand;
    }

    public boolean getRepeatProduction() {
        return repeatProduction;
    }

    public boolean canStartRobotProduction() {
        return (currentProduction == null || repeatProduction) && isFinished();
    }

    public void setRootCommand(final AutomatedCommand newRoot) {
        rootCommand = newRoot;
    }

    public void setRepeatProduction(final boolean newRepeatProduction) {
        repeatProduction = newRepeatProduction;
    }

    public boolean startRobotProduction(final AutomatedRobot robot) {
        if (!canStartRobotProduction() || robot.isFinished()) {
            return false;
        }
        currentProduction = robot;
        return true;
    }

    @Override
    protected void applyUpgrade(final Upgrade finishedUpgrade) {
        super.applyUpgrade(finishedUpgrade);
        productionSpeedMult *= finishedUpgrade.productionSpeedMult;
    }

    public boolean abortProduction() {
        if (currentProduction == null) {
            return false;
        }
        if (currentProduction.abort()) {
            currentProduction = null;
            return true;
        }
        return false;
    }

    @Override
    public void act(final UnitActionType type) {
        super.act(type);
        if (isFinished() && type == UnitActionType.OTHER) {
            if (currentProduction != null) {
                if (!currentProduction.isFinished()) {
                    currentProduction.buildTick(productionSpeedMult);
                }
                if (currentProduction.isFinished()) {
                    Cell spawnCell = GameFieldSearcher.getFreeNeighbour(getCell());
                    if (spawnCell != null) {
                        currentProduction.setCommands(rootCommand);
                        getPlayer().addUnit(currentProduction);
                        spawnCell.setUnit(currentProduction, false, false);
                        if (repeatProduction) {
                            currentProduction = AutomatedRobot.createRobot(getPlayer(), getCell(), currentProduction.getUnitType());
                        } else {
                            currentProduction = null;
                        }
                    }
                }
            }
        }
    }

    public static AutomatedCommand getDefaultCommandSet() {
        List<String> attackCommandSet = new ArrayList<>();
        attackCommandSet.add("Nearest Enemy");
        attackCommandSet.add("Enemy Main Robot");
        List<String> moveCommandSet = new ArrayList<>();
        moveCommandSet.add("To nearest Enemy");
        moveCommandSet.add("To Enemy Main Robot");
        moveCommandSet.add("To Unexplored Terrain");
        for (int x = 1; x <= Game.WAYPOINTSPERPLAYER; x++) {
            moveCommandSet.add("To Waypoint " + x);
        }
        List<String> conditionalCommandSet = new ArrayList<>();
        conditionalCommandSet.add("Enemy in Range");
        conditionalCommandSet.add("Enemy in Sight");
        conditionalCommandSet.add("Enemy Main Robot in Sight");
        conditionalCommandSet.add("Enemy Main Robot in Range");
        conditionalCommandSet.add("True");
        conditionalCommandSet.add("False");
        for (int x = 1; x <= Game.WAYPOINTSPERPLAYER; x++) {
            conditionalCommandSet.add("Waypoint " + x + " is set");
        }
        AutomatedCommand attack = new AutomatedCommand("Attack", "1", new ArrayList<>(), attackCommandSet, "Nearest Enemy");
        AutomatedCommand enemyMove = new AutomatedCommand("Move", "2", new ArrayList<>(), moveCommandSet, "To nearest Enemy");
        AutomatedCommand scoutMove = new AutomatedCommand("Move", "3", new ArrayList<>(), moveCommandSet, "To Unexplored Terrain");
        List<List<AutomatedCommand>> sightIfChildren = new ArrayList<>();
        sightIfChildren.add(new ArrayList<>());
        sightIfChildren.add(new ArrayList<>());
        sightIfChildren.get(0).add(enemyMove);
        sightIfChildren.get(1).add(scoutMove);
        AutomatedCommand sightIf = new AutomatedCommand("If", "4", sightIfChildren, conditionalCommandSet, "Enemy in Sight");
        List<List<AutomatedCommand>> attackIfChildren = new ArrayList<>();
        attackIfChildren.add(new ArrayList<>());
        attackIfChildren.add(new ArrayList<>());
        attackIfChildren.get(0).add(attack);
        attackIfChildren.get(1).add(sightIf);
        AutomatedCommand attackIf = new AutomatedCommand("If", "5", attackIfChildren, conditionalCommandSet, "Enemy in Range");
        List<List<AutomatedCommand>> whileTrueChildren = new ArrayList<>();
        whileTrueChildren.add(new ArrayList<>());
        whileTrueChildren.get(0).add(attackIf);
        AutomatedCommand whileTrueIf = new AutomatedCommand("While true", "6", whileTrueChildren, new ArrayList<>(), "");
        return whileTrueIf;
    }
}
