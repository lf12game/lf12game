package com.example.server.model;

import com.example.server.model.enums.Terrain;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class GameField {
    private final Cell[][] field;
    private Game game;
    public final int x;
    public final int y;

    public GameField(final int x, final int y, final Game game) {
        this.game = game;
        //todo move to constant
        field = createGameField(x, y, game, game.getGameSettings().getBlockerFrequencyPerHundred() / 100d, game.getGameSettings().getResourceFrequencyPerThousand() / 1000d);
        this.x = x;
        this.y = y;
    }

    public Game getGame() {
        return game;
    }

    public Cell getCell(final int x, final int y) {
        return field[y][x];
    }

    public Cell[][] getField() {
        return field;
    }

    private Cell[][] createGameField(final int mapX, final int mapY, final Game game, final double blockerChance, final double resourceChance) {
        Random random = new Random();
        Cell[][] map = new Cell[mapY][mapX];
        for (int x = 0; x < mapX; x++) {
            for (int y = 0; y < mapY; y++) {
                map[y][x] = new Cell(this, x, y);
            }
        }
        int playerNumber = game.getPlayers().size();
        double playerRoot = Math.ceil(Math.sqrt(playerNumber));
        ArrayList<Point> startingPoints = new ArrayList<>();
        for (int x = 0; x < playerRoot; x++) {
            for (int y = 0; y < playerRoot; y++) {
                int posX = (int) Math.floor((mapX / playerRoot) * x + (mapX / playerRoot / 2));
                int posY = (int) Math.floor((mapY / playerRoot) * y + (mapY / playerRoot / 2));
                startingPoints.add(new Point(posX, posY));
            }
        }
        int playersPlaced = 0;
        while (playersPlaced < playerNumber) {
            int randomIndex = random.nextInt(startingPoints.size());
            Point startPoint = startingPoints.remove(randomIndex);
            Player p = game.getPlayers().get(playersPlaced);
            MainRobot main = MainRobot.generateMainRobot(p, map[startPoint.y][startPoint.x]);
            map[startPoint.y][startPoint.x].setUnit(main, false, false);
            map[startPoint.y - 1][startPoint.x].setTerrain(Terrain.RESOURCE);
            p.addUnit(main);
            playersPlaced++;
        }
        for (int x = 0; x < mapX; x++) {
            for (int y = 0; y < mapY; y++) {
                Cell c = map[y][x];
                if (c.isFree()) {
                    double rand = random.nextDouble();
                    if (rand < blockerChance) {
                        c.setTerrain(Terrain.BLOCKER);
                    } else {
                        rand = random.nextDouble();
                        if (rand < resourceChance) {
                            c.setTerrain(Terrain.RESOURCE);
                        }
                    }
                }
            }
        }
        return map;
    }
}
