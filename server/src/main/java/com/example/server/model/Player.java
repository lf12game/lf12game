package com.example.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;

import java.awt.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Player {
    private String name;
    @JsonIgnore
    private String identification;
    @JsonIgnore
    private Game game;
    @JsonIgnore
    private LocalDateTime lastAction;
    @JsonIgnore
    private List<Unit> units;
    @JsonIgnore
    private int resources;
    @JsonIgnore
    private boolean[][] seenCells;
    @JsonIgnore
    private boolean[][] currentlySeenCellsCache;
    @JsonIgnore
    private List<Point> waypoints;
    @JsonIgnore
    private List<String> finishedResearch;

    @JsonValue
    public String serialize() {
        return name;
    }

    public Player(final String name, final String identification) {
        this.name = name;
        this.identification = identification;
        this.game = null;
        this.lastAction = LocalDateTime.now();
        this.units = new ArrayList<>();
        this.resources = 0;
        this.seenCells = null;
        this.currentlySeenCellsCache = null;
        this.waypoints = new ArrayList<>();
        for (int x = 0; x < Game.WAYPOINTSPERPLAYER; x++) {
            this.waypoints.add(new Point(-1, -1));
        }
        this.finishedResearch = new ArrayList<>();
    }

    public Player(final String name) {
        this.name = name;
        this.identification = UUID.randomUUID().toString();
        this.game = null;
        this.lastAction = LocalDateTime.now();
        this.units = new ArrayList<>();
        this.resources = 0;
        this.seenCells = null;
        this.currentlySeenCellsCache = null;
        this.waypoints = new ArrayList<>();
        for (int x = 0; x < Game.WAYPOINTSPERPLAYER; x++) {
            this.waypoints.add(new Point(-1, -1));
        }
        this.finishedResearch = new ArrayList<>();
    }

    @JsonIgnore
    public List<String> getFinishedResearch() {
        return new ArrayList<>(finishedResearch);
    }

    public boolean hasFinishedResearch(final String name) {
        return finishedResearch.contains(name);
    }

    public void addFinishedResearch(final String newResearch) {
        finishedResearch.add(newResearch);
    }

    public void changeWaypoint(final int x, final int y, final int index) {
        waypoints.set(index, new Point(x, y));
    }

    @JsonIgnore
    public List<Point> getWaypoints() {
        return new ArrayList<>(waypoints);
    }

    @JsonIgnore
    public Point getWaypoint(final int index) {
        return waypoints.get(index);
    }

    @JsonIgnore
    public boolean[][] getCachedCurrentlySeenCells() {
        return currentlySeenCellsCache;
    }

    public void setCachedCurrentlySeenCells(final boolean[][] c) {
        currentlySeenCellsCache = c;
    }

    public void initializeSeenCells(final int x, final int y) {
        if (seenCells != null) {
            return;
        }
        seenCells = new boolean[y][x];
    }

    public boolean[][] getSeenCells() {
        return seenCells;
    }

    public void updateSeenCells(final boolean[][] newSeenCells) {
        for (int y = 0; y < newSeenCells.length; y++) {
            for (int x = 0; x < newSeenCells[0].length; x++) {
                if (newSeenCells[y][x]) {
                    seenCells[y][x] = true;
                }
            }
        }
    }

    public boolean hasResources(final int amount) {
        return resources >= amount;
    }

    public void changeResources(final int amount) {
        resources += amount;
    }

    public int getResources() {
        return resources;
    }

    @JsonIgnore
    public MainRobot getMainRobot() {
        for (Unit u : units) {
            if (u instanceof MainRobot) {
                return (MainRobot) u;
            }
        }
        return null;
    }

    public void killAll(final boolean killMainRobot) {
        for (Unit u : new ArrayList<>(units)) {
            if ((!(u instanceof MainRobot)) || killMainRobot) {
                u.killUnit();
            }
        }
    }

    public List<Unit> getUnits() {
        return new ArrayList<>(units);
    }

    public boolean addUnit(final Unit unit) {
        if (!units.contains(unit)) {
            units.add(unit);
            return true;
        }
        return false;
    }

    public boolean removeUnit(final Unit unit) {
        return units.remove(unit);
    }

    @JsonIgnore
    public long getSecondsSinceLastAction() {
        return Duration.between(lastAction, LocalDateTime.now()).getSeconds();
    }

    public void actionTaken() {
        lastAction = LocalDateTime.now();
    }

    public Game getGame() {
        return game;
    }

    public boolean setGame(final Game game) {
        if (this.game != null) {
            return false;
        }
        this.game = game;
        return true;
    }

    public String getIdentification() {
        return identification;
    }

    public String getName() {
        return name;
    }
}
