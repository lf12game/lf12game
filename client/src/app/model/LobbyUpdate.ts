import {Upgrade} from "./Upgrade";
import {Research} from "./Research";

export class LobbyUpdate {
  constructor(
    public playerIdentification: string,
    public gameName: string,
    public playerNames: string[],
    public started: boolean,
    public version: number,
    public x: number,
    public y: number,
    public startResources: number,
    public updateFrequency: number,
    public blockerFrequency: number,
    public resourceFrequency: number,
    public allUpgrades: Upgrade[],
    public allResearch: Research[]) {
  }
}
