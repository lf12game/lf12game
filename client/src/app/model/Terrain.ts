export enum Terrain {
  DEFAULT,
  BLOCKER,
  RESOURCE,
  UNDISCOVERED
}
