export class Upgrade {
  constructor(
    public upgradeName: string,
    public maxHP: number,
    public speed: number,
    public damage: number,
    public range: number,
    public vision: number,
    public armor: number,
    public armorPiercing: number,
    public resourcesPerTick: number,
    public productionSpeedMult: number,
    public duration: number,
    public cost: number,
    public prerequisites: string[],
    public upgradeType: string,
    public description: string) {
  }
}
