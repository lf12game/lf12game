import {StatUnit} from "./StatUnit";
import {Building} from "./Building";
import {Point} from "./Point";
import {Upgrade} from "./Upgrade";

export class MainRobot extends StatUnit {
  constructor(
    id: number,
    player: string,
    maxHP: number,
    currentHP: number,
    speed: number,
    damage: number,
    range: number,
    vision: number,
    armor: number,
    armorPiercing: number,
    public currentConstruction: Building,
    public currentMoveOrders: Point,
    unitType: string,
    upgrades: Upgrade[],
    currentUpgrade: Upgrade,
    passedUpgradeDuration: number
  ) {
    super(id, player, maxHP, currentHP, speed, damage, range, vision, armor, armorPiercing, unitType, upgrades, currentUpgrade, passedUpgradeDuration);
  }
}
