import {StatUnit} from "./StatUnit";
import {Upgrade} from "./Upgrade";

export class AutomatedRobot extends StatUnit {
  constructor(
    id: number,
    player: string,
    maxHP: number,
    currentHP: number,
    speed: number,
    damage: number,
    range: number,
    vision: number,
    armor: number,
    armorPiercing: number,
    public buildDuration: number,
    public passedDuration: number,
    public buildCost: number,
    unitType: string,
    upgrades: Upgrade[],
    currentUpgrade: Upgrade,
    passedUpgradeDuration: number
  ) {
    super(id, player, maxHP, currentHP, speed, damage, range, vision, armor, armorPiercing, unitType, upgrades, currentUpgrade, passedUpgradeDuration);
  }
}
