export class BuildingResult {
  constructor(
    public mainRobotX: number,
    public mainRobotY: number,
    public newResources: number,
    public buildingX: number,
    public buildingY: number) {
  }

}
