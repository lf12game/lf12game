export class Research {
  constructor(
    public researchName: string,
    public duration: number,
    public cost: number,
    public description: string) {
  }
}
