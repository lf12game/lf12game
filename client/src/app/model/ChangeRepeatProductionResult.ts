import {RobotProductionBuilding} from "./RobotProductionBuilding";

export class ChangeRepeatProductionResult {
  constructor(
    public buildingState: RobotProductionBuilding,
    public buildingX: number,
    public buildingY: number) {
  }

}
