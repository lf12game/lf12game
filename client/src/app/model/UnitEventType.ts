export enum UnitEventType {
  MOVE,
  ATTACK,
  DEATH
}
