import {RobotProductionBuilding} from "./RobotProductionBuilding";

export class RobotProductionResult {
  constructor(
    public productionBuildingState: RobotProductionBuilding,
    public newResources: number,
    public buildingX: number,
    public buildingY: number) {
  }

}
