import {ResearchBuilding} from "./ResearchBuilding";

export class ResearchResult {
  constructor(
    public researchBuildingState: ResearchBuilding,
    public newResources: number,
    public researchX: number,
    public researchY: number) {
  }
}
