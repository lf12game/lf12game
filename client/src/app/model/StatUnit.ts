import {GameElement} from "./GameElement";
import {Upgrade} from "./Upgrade";

export abstract class StatUnit extends GameElement {
  constructor(
    id: number,
    player: string,
    public maxHP: number,
    public currentHP: number,
    public speed: number,
    public damage: number,
    public range: number,
    public vision: number,
    public armor: number,
    public armorPiercing: number,
    public unitType: string,
    public upgrades: Upgrade[],
    public currentUpgrade: Upgrade,
    public passedUpgradeDuration: number
  ) {
    super(id, player)
  }
}
