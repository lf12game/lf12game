import {Upgrade} from "./Upgrade";
import {LobbyUpdate} from "./LobbyUpdate";
import {Point} from "./Point";
import {GameUpdate} from "./GameUpdate";

export class RejoinGameResult {
  constructor(
    public lobbyInfo: LobbyUpdate,
    public waypoints: Point[],
    public playerName: string,
    public gameUpdate: GameUpdate) {
  }
}
