import {UnitEventType} from "./UnitEventType";
import {Point} from "./Point";

export class UnitEvent {
  constructor(
    public type: UnitEventType,
    public source: Point,
    public destination: Point
  ) {
  }
}
