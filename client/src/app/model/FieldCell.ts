import {Terrain} from "./Terrain";
import {GameElement} from "./GameElement";
import {UnitEvent} from "./UnitEvent";

export class FieldCell {
  constructor(
    public x: number,
    public y: number,
    public terrain: Terrain,
    public unit: GameElement,
    public turnEvents: UnitEvent[]
  ) {
  }
}
