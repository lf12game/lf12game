import {AutomatedCommand} from "./AutomatedCommand";

export class AutomatedCommandsResult {
  constructor(
    public rootCommand: AutomatedCommand,
    public buildingX: number,
    public buildingY: number) {
  }

}
