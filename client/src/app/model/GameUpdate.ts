import {FieldCell} from "./FieldCell";
import {GameResult} from "./GameResult";

export class GameUpdate {
  constructor(
    public gameField: FieldCell[][],
    public visible: boolean[][],
    public playerNames: string[],
    public resources: number,
    public version: number,
    public gameResult: GameResult,
    public finishedResearch: string[]
  ) {
  }
}
