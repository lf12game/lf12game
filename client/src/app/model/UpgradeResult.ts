import {StatUnit} from "./StatUnit";

export class UpgradeResult {
  constructor(
    public unitUpgradeState: StatUnit,
    public newResources: number,
    public upgradeX: number,
    public upgradeY: number) {
  }
}
