export abstract class GameElement {
  constructor(
    public id: number,
    public player: string
  ) {
  }
}
