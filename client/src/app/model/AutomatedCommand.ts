import * as uuid from 'uuid';

export class AutomatedCommand {
  name: string;
  uid: string;
  children: AutomatedCommand[][];
  options: string[];
  selectedOption: String;

  constructor(name: string, children: AutomatedCommand[][], options: string[]) {
    this.name = name;
    this.children = children;
    this.uid = uuid.v4();
    this.options = options;
    this.selectedOption = this.options.length > 0 ? this.options[0] : "";
  }

  static simpleClone(item: AutomatedCommand): AutomatedCommand {
    let newArray: AutomatedCommand[][] = [];
    for (let x = 0; x < item.children.length; x++) {
      newArray.push([]);
    }
    return new AutomatedCommand(item.name, newArray, Array.from(item.options));
  }
}
