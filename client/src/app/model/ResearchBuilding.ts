import {Building} from "./Building";
import {Upgrade} from "./Upgrade";
import {Research} from "./Research";

export class ResearchBuilding extends Building {
  constructor(
    id: number,
    player: string,
    maxHP: number,
    currentHP: number,
    speed: number,
    damage: number,
    range: number,
    vision: number,
    armor: number,
    armorPiercing: number,
    buildDuration: number,
    passedDuration: number,
    resourcesPerTick: number,
    buildCost: number,
    public passedResearchDuration: number,
    public currentResearch: Research,
    unitType: string,
    upgrades: Upgrade[],
    currentUpgrade: Upgrade,
    passedUpgradeDuration: number
  ) {
    super(id, player, maxHP, currentHP, speed, damage, range, vision, armor, armorPiercing, buildDuration, passedDuration, resourcesPerTick, buildCost,
      unitType, upgrades, currentUpgrade, passedUpgradeDuration);
  }
}
