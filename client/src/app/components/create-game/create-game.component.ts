import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {GameInitializationService} from "../../services/game-initialization.service";
import {CreateGameResult} from "../../enums/CreateGameResult";

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.css']
})
export class CreateGameComponent implements OnInit {

  gameName: string;
  playerName: string;
  password: string;
  errorMessage: string;
  private currentlyCreating: boolean;


  constructor(private router: Router, private gameInitializationService: GameInitializationService) {
    this.gameName = "";
    this.playerName = "";
    this.password = "";
    this.errorMessage = "";
    this.currentlyCreating = false;
  }

  ngOnInit(): void {
  }

  createGame() {
    if (this.currentlyCreating) {
      return;
    }
    this.currentlyCreating = true;
    this.gameInitializationService.createGame(this.gameName, this.playerName, this.password).then
    ((answer) => {
      switch (answer) {
        case CreateGameResult.SUCCESS:
          this.errorMessage = "";
          this.router.navigateByUrl('/lobby', {skipLocationChange: true});
          break;
        case CreateGameResult.GAMENAMEALREADYINUSE:
          this.errorMessage = "Game name is already in use."
          break;
        case CreateGameResult.COMMUNICATIONERROR:
          this.errorMessage = "Error in communication."
          break;
      }
      this.currentlyCreating = false;
    }, (reason) => {
      this.currentlyCreating = false;
    });
  }

  backToGameLobby() {
    this.router.navigateByUrl('/main', {skipLocationChange: true});
  }
}
