import {Component, Input, OnInit} from '@angular/core';
import {FieldCell} from "../../model/FieldCell";
import {Point} from "../../model/Point";
import {Constants} from "../../model/Constants";
import {WaypointCommandsService} from "../../services/waypoint-commands.service";
import {WaypointService} from "../../services/waypoint.service";

@Component({
  selector: 'app-waypoint',
  templateUrl: './waypoint.component.html',
  styleUrls: ['./waypoint.component.css']
})
export class WaypointComponent implements OnInit {

  @Input() selectedCell?: FieldCell | null;

  waypoints: Point[];

  constructor(private waypointCommandsService: WaypointCommandsService, public waypointService: WaypointService) {
    this.waypoints = [];
    for (let x = 0; x < Constants.NUMBEROFWAYPOINTS; x++) {
      this.waypoints.push(new Point(-1, -1));
    }
  }

  ngOnInit(): void {
  }

  setWaypointsAfterRejoin(newWaypoints: Point[]) {
    this.waypoints = newWaypoints;
    for (let x = 0; x < this.waypoints.length; x++) {
      let wp: Point = this.waypoints[x];
      this.waypointService.changeWaypoint(wp.x, wp.y, x);
    }
  }

  setWaypoint(index: number) {
    if (this.selectedCell) {
      this.waypointCommandsService.changeWaypoint(this.selectedCell.x, this.selectedCell.y, index);
      this.waypointService.changeWaypoint(this.selectedCell.x, this.selectedCell.y, index);
    }
  }

  unsetWaypoint(index: number) {
    this.waypoints[index] = new Point(-1, -1);
    this.waypointCommandsService.changeWaypoint(-1, -1, index);
    this.waypointService.changeWaypoint(-1, -1, index);
  }
}
