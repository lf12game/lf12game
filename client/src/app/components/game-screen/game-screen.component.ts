import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {GameUpdateService} from "../../services/game-update.service";
import {PlayerIdentificationService} from "../../services/player-identification.service";
import {MainRobotCommandsService} from "../../services/main-robot-commands.service";
import {HttpResponse} from "@angular/common/http";
import {MainRobotMoveResult} from "../../model/MainRobotMoveResult";
import {GameVisualizationComponent} from "../game-visualization/game-visualization.component";
import {GameElement} from "../../model/GameElement";
import {FieldCell} from "../../model/FieldCell";
import {MessageService} from "../../services/message.service";
import {MainRobot} from "../../model/MainRobot";
import {Point} from "../../model/Point";
import {BuildingResult} from "../../model/BuildingResult";
import {RobotProductionResult} from "../../model/RobotProductionResult";
import {UpgradeResult} from "../../model/UpgradeResult";
import {ChangeRepeatProductionResult} from "../../model/ChangeRepeatProductionResult";
import {Router} from "@angular/router";
import {PlayerColorService} from "../../services/player-color.service";
import {GameStateResetService} from "../../services/game-state-reset.service";
import {WaypointComponent} from "../waypoint/waypoint.component";
import {StatUnit} from "../../model/StatUnit";
import {ResearchResult} from "../../model/ResearchResult";
import {AvailableOptionsService} from "../../services/available-options.service";

@Component({
  selector: 'app-game-screen',
  templateUrl: './game-screen.component.html',
  styleUrls: ['./game-screen.component.css']
})
export class GameScreenComponent implements OnInit {

  private updateGameSubscription?: Subscription;
  public resources: number;
  public gameName: string;
  public turn: number;
  public players: string[];

  private lastMap: FieldCell[][];
  public displayUnit: GameElement | null;
  public selectedCell: FieldCell | null;


  @ViewChild(GameVisualizationComponent) visualizationChild?: GameVisualizationComponent

  private mouseDown: boolean;
  private startX: number;
  private startY: number;
  private scrollLeft: number;
  private scrollTop: number;

  @ViewChild('elemt') visualizationContainer?: ElementRef;
  private initialMainRobotFocusing: boolean;

  @ViewChild(WaypointComponent) waypointChild?: WaypointComponent
  private wayPointInitData: Point[] | undefined;

  constructor(public playerColorService: PlayerColorService, public gameUpdateService: GameUpdateService, private identification: PlayerIdentificationService,
              private mainRobotCommandService: MainRobotCommandsService, public messageService: MessageService,
              private router: Router, private gameStateResetService: GameStateResetService,
              private availableOptionService: AvailableOptionsService) {
    this.resources = 0;
    this.turn = -1;
    this.gameName = "";
    this.players = [];
    this.displayUnit = null;
    this.lastMap = [];
    this.selectedCell = null;
    this.mouseDown = false;
    this.startX = 0;
    this.startY = 0;
    this.scrollLeft = 0;
    this.scrollTop = 0;
    this.initialMainRobotFocusing = false;
    this.wayPointInitData = this.router.getCurrentNavigation()?.extras.state as Point[];
  }

  ngAfterViewInit() {
    if (this.wayPointInitData) {
      this.waypointChild!.setWaypointsAfterRejoin(this.wayPointInitData);
    }
  }


  startDragging(e: MouseEvent, el: HTMLElement) {
    if (e.button === 0) {
      this.mouseDown = true;
      this.startX = e.pageX - el.offsetLeft;
      this.startY = e.pageY - el.offsetTop;
      this.scrollLeft = el.scrollLeft;
      this.scrollTop = el.scrollTop;
    } else if (e.button === 1) {
      e.preventDefault();
      let preZoom = this.visualizationChild!.getZoomFactor();
      let preScrollLevels = this.getScrollLevels();
      this.visualizationChild!.resetZoomLevel();
      let zoom = this.visualizationChild!.getZoomFactor();
      if (preZoom !== zoom) {
        this.setScrollLevels(preScrollLevels);
      }
    }
  }

  stopDragging(e: MouseEvent) {
    this.mouseDown = false;
  }

  moveEvent(e: MouseEvent, el: HTMLElement) {
    e.preventDefault();
    if (!this.mouseDown) {
      return;
    }
    let x: number = e.pageX - el.offsetLeft;
    let y: number = e.pageY - el.offsetTop;
    let scrollX: number = x - this.startX;
    let scrollY: number = y - this.startY;
    el.scrollLeft = this.scrollLeft - scrollX;
    el.scrollTop = this.scrollTop - scrollY;
  }

  zoomChange(event: WheelEvent) {
    event.preventDefault();
    let preZoom = this.visualizationChild!.getZoomFactor();
    let preScrollLevels = this.getScrollLevels();
    if (event.deltaY > 0) {
      this.visualizationChild!.changeZoomLevel(-0.05);
    }
    if (event.deltaY < 0) {
      this.visualizationChild!.changeZoomLevel(0.05);
    }
    let zoom = this.visualizationChild!.getZoomFactor();
    if (preZoom !== zoom) {
      this.setScrollLevels(preScrollLevels);
    }
  }

  getScrollLevels(): number[] {
    return [this.visualizationContainer!.nativeElement.scrollWidth - this.visualizationContainer!.nativeElement.offsetWidth,
      this.visualizationContainer!.nativeElement.scrollHeight - this.visualizationContainer!.nativeElement.offsetHeight
    ];
  }

  setScrollLevels(scaling: number[]) {
    let xDif = scaling[0] - (this.visualizationContainer!.nativeElement.scrollWidth - this.visualizationContainer!.nativeElement.offsetWidth);
    let yDif = scaling[1] - (this.visualizationContainer!.nativeElement.scrollHeight - this.visualizationContainer!.nativeElement.offsetHeight);
    this.visualizationContainer!.nativeElement.scrollLeft -= xDif / 2;
    this.visualizationContainer!.nativeElement.scrollTop -= yDif / 2;
  }

  centerVisualizationOn(x: number, y: number, zoom: number) {
    this.visualizationContainer!.nativeElement.scrollLeft =
      x * zoom - (this.visualizationContainer!.nativeElement.offsetWidth) / (2);
    this.visualizationContainer!.nativeElement.scrollTop =
      y * zoom - (this.visualizationContainer!.nativeElement.offsetHeight) / (2);
  }

  centerOnMainRobot(): boolean {
    for (let y = 0; y < this.lastMap.length; y++) {
      for (let x = 0; x < this.lastMap[0].length; x++) {
        let unit: GameElement = this.lastMap[y][x].unit;
        if (unit && unit.hasOwnProperty("currentMoveOrders") && unit.player === this.identification.getPlayerName()) {
          let zoom = this.visualizationChild!.getZoomFactor();
          this.centerVisualizationOn(x * 16, y * 16, zoom);
          return true;
        }
      }
    }
    return false;
  }

  clickEvent(event: { event: MouseEvent; x: number; y: number }) {
    if (event.event.button === 0) {
      this.displayUnit = this.lastMap[event.y][event.x].unit;
      this.selectedCell = this.lastMap[event.y][event.x];
      if (this.displayUnit) {
        let attackRangeUnit: StatUnit = this.displayUnit as StatUnit;
        this.visualizationChild!.drawAttackRangeIndicator(event.x, event.y, attackRangeUnit.range);
      } else {
        this.visualizationChild!.drawAttackRangeIndicator(0, 0, 0);
      }
    }
    if (event.event.button === 2) {
      let answer: Promise<HttpResponse<MainRobotMoveResult>> = this.mainRobotCommandService.moveMainRobot(event.x, event.y);
      answer.then(e => {
        if (e.status == 403) {
          this.messageService.addMessage("Can't change movement while building");
        }
        if (e.body) {
          let res: MainRobotMoveResult = e.body;
          this.visualizationChild!.drawMainRobotMovementGoal(res.x, res.y, this.identification.getPlayerName());
        }

      }, f => {

      })
    }
  }

  updateOnRobotProductionEvent(robotProductionEvent: RobotProductionResult): void {
    this.resources = robotProductionEvent.newResources;
    this.lastMap[robotProductionEvent.buildingY][robotProductionEvent.buildingX].unit = robotProductionEvent.productionBuildingState;
    if (this.displayUnit?.id === robotProductionEvent.productionBuildingState.id) {
      this.displayUnit = this.lastMap[robotProductionEvent.buildingY][robotProductionEvent.buildingX].unit;
      this.selectedCell = this.lastMap[robotProductionEvent.buildingY][robotProductionEvent.buildingX];
    }

    this.visualizationChild!.updateSingleCellGameElement(robotProductionEvent.buildingX, robotProductionEvent.buildingY, robotProductionEvent.productionBuildingState, this.identification.getPlayerName());

  }

  updateOnBuildingEvent(buildingEvent: BuildingResult): void {
    this.resources = buildingEvent.newResources;
    this.visualizationChild!.drawMainRobotMovementGoal(buildingEvent.mainRobotX, buildingEvent.mainRobotY, this.identification.getPlayerName());
    if (buildingEvent.buildingX && buildingEvent.buildingY) {
      // @ts-ignore
      this.visualizationChild!.updateSingleCellGameElement(buildingEvent.buildingX, buildingEvent.buildingY, null, this.identification.getPlayerName())
    }

  }

  updateOnUpgradeEvent(upgradeEvent: UpgradeResult): void {
    this.resources = upgradeEvent.newResources;
    this.lastMap[upgradeEvent.upgradeY][upgradeEvent.upgradeX].unit = upgradeEvent.unitUpgradeState;
    this.visualizationChild!.updateSingleCellGameElement(upgradeEvent.upgradeX, upgradeEvent.upgradeY, upgradeEvent.unitUpgradeState, this.identification.getPlayerName())

    if (this.displayUnit) {
      this.displayUnit = null;
      this.displayUnit = this.lastMap[upgradeEvent.upgradeY][upgradeEvent.upgradeX].unit;
    }
    if (this.selectedCell) {
      this.selectedCell = null;
      this.selectedCell = this.lastMap[upgradeEvent.upgradeY][upgradeEvent.upgradeX];
    }
  }

  updateOnResearchEvent(researchEvent: ResearchResult): void {
    this.resources = researchEvent.newResources;
    this.lastMap[researchEvent.researchY][researchEvent.researchX].unit = researchEvent.researchBuildingState;
    this.visualizationChild!.updateSingleCellGameElement(researchEvent.researchX, researchEvent.researchY, researchEvent.researchBuildingState, this.identification.getPlayerName())

    if (this.displayUnit) {
      this.displayUnit = null;
      this.displayUnit = this.lastMap[researchEvent.researchY][researchEvent.researchX].unit;
    }
    if (this.selectedCell) {
      this.selectedCell = null;
      this.selectedCell = this.lastMap[researchEvent.researchY][researchEvent.researchX];
    }
  }

  updateOnChangeRepeatEvent(repeatEvent: ChangeRepeatProductionResult): void {
    this.lastMap[repeatEvent.buildingY][repeatEvent.buildingX].unit = repeatEvent.buildingState;
    this.visualizationChild!.updateSingleCellGameElement(repeatEvent.buildingX, repeatEvent.buildingY, repeatEvent.buildingState, this.identification.getPlayerName())

    if (this.displayUnit) {
      this.displayUnit = null;
      this.displayUnit = this.lastMap[repeatEvent.buildingY][repeatEvent.buildingX].unit;
    }
    if (this.selectedCell) {
      this.selectedCell = null;
      this.selectedCell = this.lastMap[repeatEvent.buildingY][repeatEvent.buildingX];
    }

  }

  private updateDisplayUnit(): void {
    let oldID: number = this.displayUnit!.id;
    for (let y = 0; y < this.lastMap.length; y++) {
      for (let x = 0; x < this.lastMap[0].length; x++) {
        let unit: GameElement = this.lastMap[y][x].unit;
        if (unit && unit.id === oldID) {
          this.displayUnit = unit;
          this.selectedCell = this.lastMap[y][x];
          let attackRangeUnit: StatUnit = unit as StatUnit;
          this.visualizationChild!.drawAttackRangeIndicator(x, y, attackRangeUnit.range);
          return;
        }
      }
    }
    this.displayUnit = null;
  }

  ngOnInit(): void {
    this.updateGameSubscription = this.gameUpdateService.gameUpdate$.subscribe(value => {
      this.resources = value.resources;
      this.turn = value.version;
      this.players = value.playerNames;
      this.lastMap = value.gameField;
      if (this.displayUnit) {
        this.updateDisplayUnit();
      } else {
        this.visualizationChild?.drawAttackRangeIndicator(0, 0, -1);
      }
      this.availableOptionService.setFinishedResearch(value.finishedResearch);
      outer:
        for (let y = 0; y < this.lastMap.length; y++) {
          for (let x = 0; x < this.lastMap[0].length; x++) {
            let unit: GameElement = this.lastMap[y][x].unit;
            if (unit && unit.hasOwnProperty("currentMoveOrders") && unit.player === this.identification.getPlayerName()) {
              let newMove: Point = (unit as MainRobot).currentMoveOrders;
              if (newMove) {
                this.visualizationChild?.drawMainRobotMovementGoal(newMove.x, newMove.y, this.identification.getPlayerName());
              }
              break outer;
            }
          }
        }
      if (value.gameResult.toString() !== "NONE" && value.gameResult.toString().length > 2) {
        let endMessage: string = value.gameResult.toString() === "WIN" ? "Congratulations, you won." : "You lost";
        this.gameUpdateService.stopPollingGameUpdates();
        if (this.updateGameSubscription) {
          this.updateGameSubscription.unsubscribe();
        }
        alert(endMessage);
        this.gameStateResetService.resetGame();
        this.router.navigateByUrl('', {skipLocationChange: true});
      }
      if (this.turn >= 1 && !this.initialMainRobotFocusing) {
        setTimeout(() => this.initialMainRobotFocusing = this.centerOnMainRobot(), 50);
      }
    });
    this.gameName = this.identification.getPlayerGame();
    this.gameUpdateService.startPollingGameUpdates();
  }

  ngOnDestroy() {
    this.gameUpdateService.stopPollingGameUpdates();
    if (this.updateGameSubscription) {
      this.updateGameSubscription.unsubscribe();
    }
  }
}
