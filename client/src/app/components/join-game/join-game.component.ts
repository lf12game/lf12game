import {Component, OnInit} from '@angular/core';
import {GameInitializationService} from "../../services/game-initialization.service";
import {Router} from "@angular/router";
import {JoinGameResult} from "../../enums/JoinGameResult";

@Component({
  selector: 'app-join-game',
  templateUrl: './join-game.component.html',
  styleUrls: ['./join-game.component.css']
})
export class JoinGameComponent implements OnInit {

  playerName: string;
  password: string;
  private currentlyJoining: boolean;
  errorMessage: string;

  constructor(private router: Router, public gameInitializationService: GameInitializationService) {
    this.playerName = "";
    this.password = "";
    this.currentlyJoining = false;
    this.errorMessage = "";
  }

  ngOnInit(): void {
    this.gameInitializationService.startPollingLobbies();
  }

  backToLobby() {
    this.router.navigateByUrl('', {skipLocationChange: true});
  }

  async joinGame(gameName: string) {
    if (this.currentlyJoining) {
      return;
    }
    this.currentlyJoining = true;
    this.gameInitializationService.joinGame(gameName, this.playerName, this.password).then
    ((answer) => {
      switch (answer) {
        case JoinGameResult.SUCCESS:
          this.errorMessage = ""
          this.router.navigateByUrl('/lobby', {skipLocationChange: true});
          break;
        case JoinGameResult.PASSWORDMISMATCH:
          this.errorMessage = "Password does not match."
          break;
        case JoinGameResult.GAMEALREADYSTARTED:
          this.errorMessage = "Game has already been started."
          break;
        case JoinGameResult.NAMEALREADYINUSE:
          this.errorMessage = "Name is already in use."
          break;
        case JoinGameResult.GAMENOTFOUND:
          this.errorMessage = "Game could not be found."
          break;
        case JoinGameResult.COMMUNICATIONERROR:
          this.errorMessage = "Error in communication."
          break;
      }
      this.currentlyJoining = false;
    }, (reason) => {
      this.currentlyJoining = false;
    });
  }

  ngOnDestroy() {
    this.gameInitializationService.stopPollingLobbies();
  }
}
