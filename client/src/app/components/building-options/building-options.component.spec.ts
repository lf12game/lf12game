import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BuildingOptionsComponent} from './building-options.component';

describe('BuildingOptionsComponent', () => {
  let component: BuildingOptionsComponent;
  let fixture: ComponentFixture<BuildingOptionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuildingOptionsComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildingOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
