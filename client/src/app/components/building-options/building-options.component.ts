import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FieldCell} from "../../model/FieldCell";
import {BuildingCommandsService} from "../../services/building-commands.service";
import {HttpResponse} from "@angular/common/http";
import {MainRobotMoveResult} from "../../model/MainRobotMoveResult";
import {MessageService} from "../../services/message.service";
import {BuildingResult} from "../../model/BuildingResult";
import {CdkDragDrop} from "@angular/cdk/drag-drop";
import {AvailableOptionsService} from "../../services/available-options.service";
import {Upgrade} from "../../model/Upgrade";
import {StatUnit} from "../../model/StatUnit";
import {UpgradeCommandsService} from "../../services/upgrade-commands.service";
import {UpgradeResult} from "../../model/UpgradeResult";
import {Building} from "../../model/Building";
import {PlayerIdentificationService} from "../../services/player-identification.service";
import {GameElement} from "../../model/GameElement";
import {Research} from "../../model/Research";
import {ResearchResult} from "../../model/ResearchResult";
import {ResearchCommandsService} from "../../services/research-commands.service";
import {ResearchBuilding} from "../../model/ResearchBuilding";

@Component({
  selector: 'app-building-options',
  templateUrl: './building-options.component.html',
  styleUrls: ['./building-options.component.css']
})
export class BuildingOptionsComponent implements OnInit {

  @Input() selectedCell?: FieldCell | null;

  @Output() buildingNotification: EventEmitter<BuildingResult>;
  @Output() upgradeNotification: EventEmitter<UpgradeResult>;
  @Output() researchNotification: EventEmitter<ResearchResult>;

  allUpgrades: Upgrade[];
  allResearch: Research[];

  constructor(private buildingCommandsService: BuildingCommandsService, private messageService: MessageService,
              private availableOptionsService: AvailableOptionsService, private upgradeCommandsService: UpgradeCommandsService,
              private identification: PlayerIdentificationService, private researchCommandsService: ResearchCommandsService) {
    this.buildingNotification = new EventEmitter();
    this.upgradeNotification = new EventEmitter();
    this.researchNotification = new EventEmitter();
    this.allUpgrades = this.availableOptionsService.getAllUpgrades();
    this.allResearch = this.availableOptionsService.getAllResearch();
  }

  ngOnInit(): void {
  }

  mayBuildResource(): boolean {
    if (this.selectedCell) {
      return !this.selectedCell.unit && this.selectedCell.terrain.toString() === "RESOURCE";
    }
    return false;
  }

  mayBuildNormal(): boolean {
    if (this.selectedCell) {
      return !this.selectedCell.unit && this.selectedCell.terrain.toString() === "DEFAULT";
    }
    return false;
  }

  mayBuildDefence(): boolean {
    if (this.selectedCell) {
      return !this.selectedCell.unit && this.selectedCell.terrain.toString() === "DEFAULT";
    }
    return false;
  }

  mayBuildRadar(): boolean {
    if (this.selectedCell) {
      return !this.selectedCell.unit && this.selectedCell.terrain.toString() === "DEFAULT";
    }
    return false;
  }

  mayBuildResearch(): boolean {
    if (this.selectedCell) {
      return !this.selectedCell.unit && this.selectedCell.terrain.toString() === "DEFAULT";
    }
    return false;
  }

  maySelectUpgrade(upgrade: Upgrade): boolean {
    if (this.selectedCell && this.selectedCell.unit) {
      let unit: StatUnit = this.selectedCell.unit as StatUnit;
      if (unit.currentUpgrade && unit.currentUpgrade.upgradeName === upgrade.upgradeName) {
        return false;
      }
      if (unit.upgrades.some(u => u.upgradeName === upgrade.upgradeName)) {
        return false;
      }

      return upgrade.upgradeType === unit.unitType && upgrade.prerequisites.every(s => unit.upgrades.some(owned => owned.upgradeName === s));
    }
    return false;
  }

  isResearchBuilding(): boolean {
    return this.selectedCell !== null && this.selectedCell?.unit !== null && this.selectedCell!.unit.hasOwnProperty("passedResearchDuration");
  }

  maySelectResearch(research: Research): boolean {
    if (this.isResearchBuilding()) {
      let unit: ResearchBuilding = this.selectedCell!.unit as ResearchBuilding;
      if (unit.currentResearch && unit.currentResearch.researchName === research.researchName) {
        return false;
      }
      if (this.availableOptionsService.getFinishedResearch().some(u => u === research.researchName)) {
        return false;
      }
      return true;
    }
    return false;
  }

  isNotBuildingOrDone(): boolean {
    if (this.selectedCell && this.selectedCell.unit && this.selectedCell.unit.hasOwnProperty("resourcesPerTick")) {
      let b: Building = this.selectedCell.unit as Building;
      return b.passedDuration >= b.buildDuration;
    }
    return true;
  }

  ownUnit(): boolean {
    if (this.selectedCell && this.selectedCell.unit) {
      return this.identification.getPlayerName() === (this.selectedCell.unit as GameElement).player;
    }
    return false;
  }

  isResearching(): boolean {
    if (this.isResearchBuilding()) {
      let unit: ResearchBuilding = this.selectedCell!.unit as ResearchBuilding;
      return unit.currentResearch !== null;
    }
    return false;
  }

  isUpgrading(): boolean {
    if (this.selectedCell && this.selectedCell.unit) {
      let unit: StatUnit = this.selectedCell.unit as StatUnit;
      return unit.currentUpgrade !== null;
    }
    return false;
  }

  hasUnit(): boolean {
    return this.selectedCell !== null && this.selectedCell?.unit !== null;
  }

  startUpgrade(upgrade: Upgrade): void {
    let answer: Promise<HttpResponse<UpgradeResult>> = this.upgradeCommandsService.startUpgrade(this.selectedCell!.x, this.selectedCell!.y, upgrade.upgradeName);
    answer.then(e => {
      if (e.status == 403) {
        this.messageService.addMessage("Not enough resources for upgrade");
      }
      if (e.status == 417) {
        this.messageService.addMessage("Can't start new upgrades while upgrading");
      }
      if (e.body) {
        this.upgradeNotification.emit(e.body);
      }
    }, f => {

    })
  }

  abortUpgrade(): void {
    let answer: Promise<HttpResponse<UpgradeResult>> = this.upgradeCommandsService.abortUpgrade(this.selectedCell!.x, this.selectedCell!.y);
    answer.then(e => {
      if (e.status == 404) {
        this.messageService.addMessage("No upgrade to abort");
      }
      if (e.body) {
        this.upgradeNotification.emit(e.body);
      }
    }, f => {

    })
  }

  startResearch(research: Research): void {
    let answer: Promise<HttpResponse<ResearchResult>> = this.researchCommandsService.startResearch(this.selectedCell!.x, this.selectedCell!.y, research.researchName);
    answer.then(e => {
      if (e.status == 403) {
        this.messageService.addMessage("Not enough resources for research");
      }
      if (e.status == 417) {
        this.messageService.addMessage("Can't start new research while researching");
      }
      if (e.body) {
        this.researchNotification.emit(e.body);
      }
    }, f => {

    })
  }

  abortResearch(): void {
    let answer: Promise<HttpResponse<ResearchResult>> = this.researchCommandsService.abortResearch(this.selectedCell!.x, this.selectedCell!.y);
    answer.then(e => {
      if (e.status == 404) {
        this.messageService.addMessage("No research to abort");
      }
      if (e.body) {
        this.researchNotification.emit(e.body);
      }
    }, f => {

    })
  }

  abortBuilding(): void {
    let answer: Promise<HttpResponse<BuildingResult>> = this.buildingCommandsService.abortBuilding();
    answer.then(e => {
      if (e.status == 404) {
        this.messageService.addMessage("No construction to abort");
      }
      if (e.body) {
        this.buildingNotification.emit(e.body);
      }
    }, f => {

    })
  }

  buildBuilding(type: string): void {
    let answer: Promise<HttpResponse<BuildingResult>> = this.buildingCommandsService.buildBuilding(this.selectedCell!.x, this.selectedCell!.y, type);
    answer.then(e => {
      if (e.status == 403) {
        this.messageService.addMessage("Can't build new buildings while building");
      }
      if (e.status == 417) {
        this.messageService.addMessage("Not enough resources for building");
      }
      if (e.body) {
        this.buildingNotification.emit(e.body);
      }
    }, f => {

    })
  }
}
