import {ComponentFixture, TestBed} from '@angular/core/testing';

import {GameVisualizationComponent} from './game-visualization.component';

describe('GameVisualizationComponent', () => {
  let component: GameVisualizationComponent;
  let fixture: ComponentFixture<GameVisualizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GameVisualizationComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameVisualizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
