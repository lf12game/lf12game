import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {FieldCell} from "../../model/FieldCell";
import {GameElement} from "../../model/GameElement";
import {Terrain} from "../../model/Terrain";
import {GameUpdateService} from "../../services/game-update.service";
import {PlayerIdentificationService} from "../../services/player-identification.service";
import {Point} from "../../model/Point";
import {UnitEvent} from "../../model/UnitEvent";
import {PlayerColorService} from "../../services/player-color.service";
import {Building} from "../../model/Building";
import {AutomatedRobot} from "../../model/AutomatedRobot";
import {RobotProductionBuilding} from "../../model/RobotProductionBuilding";
import {Subscription} from "rxjs";
import {StatUnit} from "../../model/StatUnit";
import {WaypointService} from "../../services/waypoint.service";
import {ResearchBuilding} from "../../model/ResearchBuilding";

@Component({
  selector: 'app-game-visualization',
  templateUrl: './game-visualization.component.html',
  styleUrls: ['./game-visualization.component.css']
})
export class GameVisualizationComponent implements OnInit {

  @ViewChild('myCanvas', {static: false}) myCanvas!: ElementRef;
  @ViewChild('myCanvasFront', {static: false}) myCanvasFront!: ElementRef;
  @ViewChild('myCanvasFrontiest', {static: false}) myCanvasFrontiest!: ElementRef;

  private updateGameSubscription?: Subscription;
  private context?: CanvasRenderingContext2D;
  private contextFront?: CanvasRenderingContext2D;
  private contextFrontiest?: CanvasRenderingContext2D;
  private pixelLength: number = 16; //TODO
  private lastMap: FieldCell[][];
  private lastVisible: boolean[][];
  private mainRobotMoveGoal?: Point;
  private mapInitialized: boolean;
  private zoomLevel: number;
  private readonly maxZoomLevel: number = 1;
  private readonly minZoomLevel: number = 0.25;

  @Output() clickEvent = new EventEmitter<{ event: MouseEvent; x: number; y: number }>();

  constructor(private playerColorService: PlayerColorService, private gameUpdateService: GameUpdateService,
              private playerIdentificationService: PlayerIdentificationService, private waypointService: WaypointService) {
    this.lastMap = [];
    this.lastVisible = [];
    this.mapInitialized = false;
    this.zoomLevel = 1;
  }

  ngOnInit(): void {

  }

  drawAttackRangeIndicator(centerX: number, centerY: number, range: number) {
    let dist: number = range * this.pixelLength + this.pixelLength / 2;
    centerX = centerX * this.pixelLength + this.pixelLength / 2;
    centerY = centerY * this.pixelLength + this.pixelLength / 2;
    this.contextFrontiest!.clearRect(0, 0, (this.myCanvasFrontiest?.nativeElement as HTMLCanvasElement).width, (this.myCanvasFrontiest?.nativeElement as HTMLCanvasElement).height);
    if (range <= 0) {
      return;
    }
    this.contextFrontiest!.strokeStyle = "red";
    this.contextFrontiest!.moveTo(centerX - dist, centerY - dist);
    this.contextFrontiest!.beginPath();
    this.contextFrontiest!.rect(centerX - dist, centerY - dist, 2 * dist, 2 * dist);
    this.contextFrontiest!.stroke();
    this.contextFrontiest!.closePath();
  }

  onLeftClick(event: MouseEvent) {
    this.clickEvent.next({
      event: event,
      x: Math.floor((event.offsetX / this.zoomLevel) / this.pixelLength),
      y: Math.floor((event.offsetY / this.zoomLevel) / this.pixelLength)
    });
  }

  onRightClick(event: MouseEvent) {
    event.preventDefault();
    this.clickEvent.next({
      event: event,
      x: Math.floor((event.offsetX / this.zoomLevel) / this.pixelLength),
      y: Math.floor((event.offsetY / this.zoomLevel) / this.pixelLength)
    });
  }

  getZoomFactor(): number {
    return this.zoomLevel;
  }

  resetZoomLevel() {
    this.zoomLevel = 1;
    this.myCanvas.nativeElement!.style.zoom = this.zoomLevel * 100 + "%";
    this.myCanvasFront.nativeElement!.style.zoom = this.zoomLevel * 100 + "%";
    this.myCanvasFrontiest.nativeElement!.style.zoom = this.zoomLevel * 100 + "%";
  }

  changeZoomLevel(level: number) {
    level = Math.max(this.minZoomLevel, Math.min(this.maxZoomLevel, this.zoomLevel + level));
    this.zoomLevel = level;

    this.myCanvas.nativeElement!.style.zoom = level * 100 + "%";
    this.myCanvasFront.nativeElement!.style.zoom = level * 100 + "%";
    this.myCanvasFrontiest.nativeElement!.style.zoom = level * 100 + "%";
  }

  drawMap(field: FieldCell[][], pixelLength: number, playerName: string, currentlyVisible: boolean[][]): void {
    if (!field || field.length === 0) {
      return
    }
    if (this.context) {
      //this.context.clearRect(0, 0, (this.myCanvas?.nativeElement as HTMLCanvasElement).width, (this.myCanvas?.nativeElement as HTMLCanvasElement).height);
    }
    if (this.contextFront) {
      this.contextFront.clearRect(0, 0, (this.myCanvasFront?.nativeElement as HTMLCanvasElement).width, (this.myCanvasFront?.nativeElement as HTMLCanvasElement).height);
    }
    if (!this.mapInitialized) {
      this.mapInitialized = true;
      (this.myCanvas?.nativeElement as HTMLCanvasElement).setAttribute("width", (pixelLength * field[0].length).toString() + "px");
      (this.myCanvas?.nativeElement as HTMLCanvasElement).setAttribute("height", (pixelLength * field.length).toString() + "px");
      (this.myCanvasFront?.nativeElement as HTMLCanvasElement).setAttribute("width", (pixelLength * field[0].length).toString() + "px");
      (this.myCanvasFront?.nativeElement as HTMLCanvasElement).setAttribute("height", (pixelLength * field.length).toString() + "px");
      (this.myCanvasFrontiest?.nativeElement as HTMLCanvasElement).setAttribute("width", (pixelLength * field[0].length).toString() + "px");
      (this.myCanvasFrontiest?.nativeElement as HTMLCanvasElement).setAttribute("height", (pixelLength * field.length).toString() + "px");
    }
    for (let x = 0; x < field[0].length; x++) {
      for (let y = 0; y < field.length; y++) {
        this.drawCell(x * pixelLength, y * pixelLength, pixelLength, playerName, field[y][x], currentlyVisible[y][x], false);
      }
    }
    for (let w of this.waypointService.getWaypoints()) {
      this.contextFront?.beginPath()
      this.contextFront!.strokeStyle = 'red';
      this.contextFront!.arc(w.x * pixelLength + 8, w.y * pixelLength + 8, 8, 0, 2 * Math.PI);
      this.contextFront!.stroke()
    }
  }

  drawMainRobotMovementGoal(startX: number, startY: number, playerName: string) {
    if (this.mainRobotMoveGoal) {
      let tempX = this.mainRobotMoveGoal.x;
      let tempY = this.mainRobotMoveGoal.y;
      this.mainRobotMoveGoal = new Point(startX, startY);
      this.redrawCell(tempX * this.pixelLength, tempY * this.pixelLength, this.pixelLength, playerName, this.lastMap[tempY][tempX], this.lastVisible[tempY][tempX], false);
    } else {
      this.mainRobotMoveGoal = new Point(startX, startY);
    }
    this.redrawCell(startX * this.pixelLength, startY * this.pixelLength, this.pixelLength, playerName, this.lastMap[startY][startX], this.lastVisible[startY][startX], false);
  }

  redrawCellWithoutUnit(startX: number, startY: number, playerName: string) {
    this.redrawCell(startX * this.pixelLength, startY * this.pixelLength, this.pixelLength, playerName, this.lastMap[startY][startX], this.lastVisible[startY][startX], true);
  }

  redrawCell(startX: number, startY: number, pixelLength: number, playerName: string, cell: FieldCell, currentlyVisible: boolean, ignoreUnit: boolean) {
    this.context!.clearRect(startX, startY, pixelLength, pixelLength);
    this.drawCell(startX, startY, pixelLength, playerName, cell, currentlyVisible, ignoreUnit, true);
  }

  updateSingleCellGameElement(cellX: number, cellY: number, element: GameElement, playerName: string) {
    this.lastMap[cellY][cellX].unit = element;
    this.redrawCell(cellX * this.pixelLength, cellY * this.pixelLength, this.pixelLength, playerName, this.lastMap[cellY][cellX], this.lastVisible[cellY][cellX], false);
  }

  drawArrow(c: CanvasRenderingContext2D, fromx: number, fromy: number, tox: number, toy: number) {
    let headlen = this.pixelLength / 3 * 2;
    let dx = tox - fromx;
    let dy = toy - fromy;
    let angle = Math.atan2(dy, dx);
    c.moveTo(fromx, fromy);
    c.lineTo(tox, toy);
    c.moveTo(tox, toy);
    c.lineTo(tox - headlen * Math.cos(angle - Math.PI / 6), toy - headlen * Math.sin(angle - Math.PI / 6));
    c.moveTo(tox, toy);
    c.lineTo(tox - headlen * Math.cos(angle + Math.PI / 6), toy - headlen * Math.sin(angle + Math.PI / 6));
  }

  drawEvent(event: UnitEvent) {
    switch (event.type.toString()) {
      case "DEATH":
        let xPos = event.source.x;
        let yPos = event.source.y;
        this.contextFront!.strokeStyle = "red";
        this.contextFront!.beginPath();
        this.contextFront!.moveTo(xPos * this.pixelLength + this.pixelLength / 2 - this.pixelLength / 3, yPos * this.pixelLength + this.pixelLength / 2 - this.pixelLength / 3);
        this.contextFront!.lineTo(xPos * this.pixelLength + this.pixelLength / 2 + this.pixelLength / 3, yPos * this.pixelLength + this.pixelLength / 2 + this.pixelLength / 3);
        this.contextFront!.stroke();
        this.contextFront!.closePath();
        this.contextFront!.beginPath();
        this.contextFront!.moveTo(xPos * this.pixelLength + this.pixelLength / 2 + this.pixelLength / 3, yPos * this.pixelLength + this.pixelLength / 2 - this.pixelLength / 3);
        this.contextFront!.lineTo(xPos * this.pixelLength + this.pixelLength / 2 - this.pixelLength / 3, yPos * this.pixelLength + this.pixelLength / 2 + this.pixelLength / 3);
        this.contextFront!.stroke();
        this.contextFront!.closePath();
        break;
      case "MOVE":
        this.contextFront!.strokeStyle = "blue";
        this.contextFront!.beginPath();
        this.drawArrow(this.contextFront!, event.source.x * this.pixelLength + this.pixelLength / 2, event.source.y * this.pixelLength + this.pixelLength / 2,
          event.destination.x * this.pixelLength + this.pixelLength / 2, event.destination.y * this.pixelLength + this.pixelLength / 2);
        this.contextFront!.stroke();
        this.contextFront!.closePath();
        break;
      case "ATTACK":
        this.contextFront!.strokeStyle = "red";
        this.contextFront!.beginPath();
        this.drawArrow(this.contextFront!, event.source.x * this.pixelLength + this.pixelLength / 2, event.source.y * this.pixelLength + this.pixelLength / 2,
          event.destination.x * this.pixelLength + this.pixelLength / 2, event.destination.y * this.pixelLength + this.pixelLength / 2);
        this.contextFront!.stroke();
        this.contextFront!.closePath();
        break;
    }
  }

  drawCell(startX: number, startY: number, pixelLength: number, playerName: string, cell: FieldCell, currentlyVisible: boolean, ignoreUnit: boolean, alwaysDraw: boolean = false) {
    let unit: StatUnit = cell.unit as StatUnit;
    let terrain: string = cell.terrain.toString();
    let events: UnitEvent[] = cell.turnEvents;
    events.forEach(e => this.drawEvent(e));
    if (!alwaysDraw && this.lastMap.length > 0 && this.lastVisible.length > 0) {
      let prevCell: FieldCell = this.lastMap[cell.y][cell.x];
      let prevVisible: boolean = this.lastVisible[cell.y][cell.x];
      if (((!prevVisible && !currentlyVisible) || (terrain === "UNDISCOVERED" && prevCell.terrain.toString() === "UNDISCOVERED") || (prevVisible && currentlyVisible && !unit && !prevCell.unit)) &&
        !(this.mainRobotMoveGoal && this.mainRobotMoveGoal.x * this.pixelLength === startX && this.mainRobotMoveGoal.y * this.pixelLength === startY)) {
        return;
      }
    }
    this.context!.clearRect(startX, startY, pixelLength, pixelLength);
    if (currentlyVisible) {
      if (unit && !ignoreUnit) {
        if (unit.unitType == 'MainRobot') {
          this.context!.fillStyle = this.playerColorService.getColorOfPlayer(unit.player);
          this.context?.fillRect(startX + 2, startY + 2, pixelLength - 4, pixelLength - 4);
          let percentOfHealthBar: number = (unit.currentHP / unit.maxHP) * 100;
          this.context!.fillStyle = 'green';
          this.context?.fillRect(startX + 2, startY, (percentOfHealthBar / 100) * 12, 2);
        } else if (unit.unitType == 'Basic') {
          this.context!.fillStyle = "grey";
          this.context?.fillRect(startX + 6, startY + 3, pixelLength - 12, pixelLength - 12);
          this.context?.fillRect(startX + 5, startY + 8, pixelLength - 18, pixelLength - 14);
          this.context?.fillRect(startX + 11, startY + 8, pixelLength - 14, pixelLength - 14);
          this.context?.fillRect(startX + 5, startY + 12, pixelLength - 14, pixelLength - 13);
          this.context?.fillRect(startX + 9, startY + 12, pixelLength - 14, pixelLength - 13);
          this.context!.fillStyle = this.playerColorService.getColorOfPlayer(unit.player);
          this.context?.fillRect(startX + 5, startY + 7, pixelLength - 10, pixelLength - 11);
          let percentOfHealthBar: number = (unit.currentHP / unit.maxHP) * 100;
          this.context!.fillStyle = 'green';
          this.context?.fillRect(startX + 2, startY, (percentOfHealthBar / 100) * 12, 2);
        } else if (unit.unitType == 'Reconnaissance') {
          this.context?.beginPath();
          this.context!.strokeStyle = "black";
          this.context?.moveTo(startX + 8, startY + 6)
          this.context?.lineTo(startX + 7, startY + 2)
          this.context?.stroke()
          this.context!.fillStyle = "red"
          this.context?.fillRect(startX + 6, startY + 1, pixelLength - 14, pixelLength - 14)
          this.context!.fillStyle = "grey";
          this.context?.fillRect(startX + 6, startY + 6, pixelLength - 12, pixelLength - 13);
          this.context?.fillRect(startX + 5, startY + 13, pixelLength - 14, pixelLength - 13);
          this.context?.fillRect(startX + 9, startY + 13, pixelLength - 14, pixelLength - 13);
          this.context!.fillStyle = this.playerColorService.getColorOfPlayer(unit.player);
          this.context?.fillRect(startX + 6, startY + 9, pixelLength - 12, pixelLength - 12);
          let percentOfHealthBar: number = (unit.currentHP / unit.maxHP) * 100;
          this.context!.fillStyle = 'green';
          this.context?.fillRect(startX + 2, startY, (percentOfHealthBar / 100) * 12, 2);
        } else if (unit.unitType == 'Tank') {
          this.context!.fillStyle = this.playerColorService.getColorOfPlayer(unit.player);
          this.context?.fillRect(startX + 1, startY + 4, pixelLength - 8, pixelLength - 8);
          this.context!.fillStyle = "grey";
          this.context?.fillRect(startX + 3, startY + 1, pixelLength - 12, pixelLength - 13);
          this.context?.fillRect(startX + 2, startY + 12, pixelLength - 14, pixelLength - 13);
          this.context?.fillRect(startX + 6, startY + 12, pixelLength - 14, pixelLength - 13);
          this.context?.fillRect(startX, startY + 5, pixelLength - 13, pixelLength - 10);
          this.context?.fillRect(startX + 8, startY + 5, pixelLength - 13, pixelLength - 10);
          this.context!.fillStyle = "black"
          this.context?.fillRect(startX + 3, startY + 8, pixelLength - 5, pixelLength - 13)
          let percentOfHealthBar: number = (unit.currentHP / unit.maxHP) * 100;
          this.context!.fillStyle = 'green';
          this.context?.fillRect(startX + 2, startY, (percentOfHealthBar / 100) * 12, 2);
        } else if (unit.unitType == 'Sniper') {
          //kopf
          this.context!.fillStyle = "grey";
          this.context?.fillRect(startX + 3, startY + 3, pixelLength - 12, pixelLength - 12);

          //rechter arm
          this.context?.fillRect(startX + 7, startY + 9, pixelLength - 13, pixelLength - 14);

          //torso
          this.context!.fillStyle = 'grey';
          this.context!.fillRect(startX + 4, startY + 6, pixelLength - 14, pixelLength - 9);

          //linkes bein
          this.context!.fillRect(startX + 2, startY + 12, pixelLength - 14, pixelLength - 13);
          //rechtes bein
          this.context?.fillRect(startX + 6, startY + 12, pixelLength - 14, pixelLength - 13);

          //waffe
          this.context!.fillStyle = "black";
          this.context?.fillRect(startX + 4, startY + 7, pixelLength - 18, pixelLength - 12);

          this.context!.fillStyle = this.playerColorService.getColorOfPlayer(unit.player);
          this.context?.fillRect(startX + 4, startY + 7, pixelLength - 13, pixelLength - 12)

          //lauf
          this.context?.fillRect(startX + 5, startY + 7, pixelLength - 7, pixelLength - 14)
          this.context!.fillStyle = "black"
          this.context?.fillRect(startX + 14, startY + 7, pixelLength - 14, pixelLength - 14)
          //rohr
          this.context?.fillRect(startX + 5, startY + 5, pixelLength - 15, pixelLength - 14)
          this.context?.fillRect(startX + 9, startY + 5, pixelLength - 15, pixelLength - 14)
          this.context!.fillStyle = this.playerColorService.getColorOfPlayer(unit.player);
          this.context?.fillRect(startX + 6, startY + 5, pixelLength - 13, pixelLength - 14)

          //linker arm
          this.context!.fillStyle = 'grey';
          this.context?.fillRect(startX + 4, startY + 6, pixelLength - 18, pixelLength - 15);
          this.context?.fillRect(startX + 3, startY + 6, pixelLength - 18, pixelLength - 12);

          let percentOfHealthBar: number = (unit.currentHP / unit.maxHP) * 100;
          this.context!.fillStyle = 'green';
          this.context?.fillRect(startX + 2, startY, (percentOfHealthBar / 100) * 12, 2);
        } else if (unit.hasOwnProperty("buildCost") && unit.hasOwnProperty("resourcesPerTick")) {
          let building: Building = unit as Building;

          if (unit.unitType == 'Production') {
            let robotBuilding: RobotProductionBuilding = building as RobotProductionBuilding;
            this.context!.fillStyle = this.playerColorService.getColorOfPlayer(unit.player);
            this.context?.fillRect(startX, startY + 7, pixelLength, pixelLength - 7);
            this.context!.fillStyle = 'black';
            this.context?.fillRect(startX + 6, startY + 11, 4, 5);
            this.context!.fillStyle = 'grey';
            this.context?.fillRect(startX + 1, startY + 8, 2, -8);
            this.context?.fillRect(startX + 7, startY + 8, 2, -6);
            this.context?.fillRect(startX + 13, startY + 8, 2, -4);
            let percentOfHealthBar: number = (unit.currentHP / unit.maxHP) * 100;
            this.context!.fillStyle = 'green';
            this.context?.fillRect(startX, startY, (percentOfHealthBar / 100) * 16, 2);
            if (robotBuilding.currentProduction) {
              let percentOfRobotProduction: number = (robotBuilding.currentProduction.passedDuration / robotBuilding.currentProduction.buildDuration) * 100;
              this.context!.fillStyle = 'red';
              this.context?.fillRect(startX, startY + 16, 2, -(percentOfRobotProduction / 100) * 16);
            }
          } else if (unit.unitType == 'Resource') {
            this.context!.fillStyle = this.playerColorService.getColorOfPlayer(unit.player);
            this.context?.fillRect(startX + 3, startY + 3, pixelLength - 6, pixelLength - 6);
            this.context!.clearRect(startX + 5, startY + 5, pixelLength - 10, pixelLength - 10);
            this.context!.fillStyle = 'blue';
            this.context?.fillRect(startX + 5, startY + 5, pixelLength - 10, pixelLength - 10);
            let percentOfHealthBar: number = (unit.currentHP / unit.maxHP) * 100;
            this.context!.fillStyle = 'green';
            this.context?.fillRect(startX + 3, startY, (percentOfHealthBar / 100) * 10, 2);
          } else if (unit.unitType == 'Defence') {
            this.context!.fillStyle = 'grey';
            this.context?.fillRect(startX + 3, startY + 3, 10, 10);
            this.context!.fillStyle = this.playerColorService.getColorOfPlayer(unit.player);
            this.context?.fillRect(startX + 5, startY + 5, 6, 6);
            this.context!.fillStyle = 'black';
            this.context?.fillRect(startX + 11, startY + 6, 3, 1);
            this.context?.fillRect(startX + 11, startY + 9, 3, 1);
            this.context?.fillRect(startX + 5, startY + 6, -3, 1);
            this.context?.fillRect(startX + 5, startY + 9, -3, 1);
            this.context?.fillRect(startX + 6, startY + 11, 1, 3);
            this.context?.fillRect(startX + 9, startY + 11, 1, 3);
            this.context?.fillRect(startX + 6, startY + 5, 1, -3);
            this.context?.fillRect(startX + 9, startY + 5, 1, -3);
            let percentOfHealthBar: number = (unit.currentHP / unit.maxHP) * 100;
            this.context!.fillStyle = 'green';
            this.context?.fillRect(startX + 2, startY, (percentOfHealthBar / 100) * 12, 2);
          } else if (unit.unitType == 'Radar') {
            this.context?.beginPath()
            this.context!.fillStyle = 'black';
            this.context?.fillRect(startX + 3, startY + 5, pixelLength - 11, pixelLength - 9);
            this.context!.fillStyle = this.playerColorService.getColorOfPlayer(unit.player);
            this.context?.arc(startX + 8, startY + 3, 5, Math.PI * 0.3, Math.PI * 1.2);
            this.context?.fill()
            this.context?.closePath()
            this.context?.beginPath()
            this.context!.strokeStyle = 'black';
            this.context?.moveTo(startX + 8, startY + 3);
            this.context?.lineTo(startX + 10, startY + 1);
            this.context?.stroke()
            let percentOfHealthBar: number = (unit.currentHP / unit.maxHP) * 100;
            this.context!.fillStyle = 'green';
            this.context?.fillRect(startX + 3, startY, (percentOfHealthBar / 100) * 10, 2);
          } else if (unit.unitType == 'Research') {
            let researchBuilding: ResearchBuilding = building as ResearchBuilding;
            this.context!.beginPath();
            this.context!.fillStyle = this.playerColorService.getColorOfPlayer(unit.player);
            this.context?.fillRect(startX, startY + 6, pixelLength, pixelLength - 7);
            this.context?.moveTo(startX, startY + 6);
            this.context?.lineTo(startX + 8, startY + 2);
            this.context?.lineTo(startX + 16, startY + 6)
            this.context?.lineTo(startX, startY + 6)
            this.context?.fill();
            this.context!.strokeStyle = 'black';
            this.context?.stroke();
            this.context!.font = '7px sans-serif'
            this.context?.strokeText('LAB', startX + 1, startY + 13)
            let percentOfHealthBar: number = (unit.currentHP / unit.maxHP) * 100;
            this.context!.fillStyle = 'green';
            this.context?.fillRect(startX, startY, (percentOfHealthBar / 100) * 16, 2);
            if (researchBuilding.currentResearch) {
              let percent: number = (researchBuilding.passedResearchDuration / researchBuilding.currentResearch.duration) * 100;
              this.context!.fillStyle = '#ee400f';
              this.context?.fillRect(startX + 14, startY + 15, 2, -(percent / 100) * 15);
            }
          }
          if (building.passedDuration < building.buildDuration) {
            let percent: number = (building.passedDuration / building.buildDuration) * 100;
            this.context!.fillStyle = 'red';
            this.context?.fillRect(startX, startY + 15, 2, -(percent / 100) * 16);
          }
        }
        if (unit.currentUpgrade) {
          let percent: number = (unit.passedUpgradeDuration / unit.currentUpgrade.duration) * 100;
          this.context!.fillStyle = '#ee400f';
          this.context?.fillRect(startX + 14, startY + 15, 2, -(percent / 100) * 16);
        }
      } else {
        this.context!.fillStyle = 'white';
        this.context?.fillRect(startX, startY, pixelLength, pixelLength);
        switch (terrain) {
          case "DEFAULT":
            this.context!.fillStyle = 'white';
            break;
          case "BLOCKER":
            this.context!.fillStyle = 'black';
            break;
          case "RESOURCE":
            this.context!.fillStyle = 'blue';
            break;
          default:
        }
        this.context?.fillRect(startX + 2, startY + 2, pixelLength - 4, pixelLength - 4);
      }
    } else {
      if (terrain === "UNDISCOVERED") {
        this.context!.fillStyle = 'grey';
        this.context?.fillRect(startX, startY, pixelLength, pixelLength);
      } else {
        this.context!.fillStyle = 'lightgrey';
        this.context?.fillRect(startX, startY, pixelLength, pixelLength);
        switch (terrain) {
          case "DEFAULT":
            this.context!.fillStyle = 'grey';
            break;
          case "BLOCKER":
            this.context!.fillStyle = 'black';
            break;
          case "RESOURCE":
            this.context!.fillStyle = 'blue';
            break;
          default:
        }
        this.context?.fillRect(startX + 2, startY + 2, pixelLength - 4, pixelLength - 4);
      }
    }
    if (this.mainRobotMoveGoal && this.mainRobotMoveGoal.x * this.pixelLength === startX && this.mainRobotMoveGoal.y * this.pixelLength === startY) {
      this.context!.strokeStyle = "purple";
      this.context!.beginPath();
      this.context!.moveTo(startX + this.pixelLength / 2 - this.pixelLength / 3, startY + this.pixelLength / 2 - this.pixelLength / 3);
      this.context!.lineTo(startX + this.pixelLength / 2 + this.pixelLength / 3, startY + this.pixelLength / 2 + this.pixelLength / 3);
      this.context!.stroke();
      this.context!.closePath();

      this.context!.beginPath();
      this.context!.moveTo(startX + this.pixelLength / 2 + this.pixelLength / 3, startY + this.pixelLength / 2 - this.pixelLength / 3);
      this.context!.lineTo(startX + this.pixelLength / 2 - this.pixelLength / 3, startY + this.pixelLength / 2 + this.pixelLength / 3);
      this.context!.stroke();
      this.context!.closePath();

    }
  }

  ngAfterViewInit(): void {
    this.context = this.myCanvas!.nativeElement.getContext('2d');
    this.contextFront = this.myCanvasFront!.nativeElement.getContext('2d');
    this.contextFrontiest = this.myCanvasFrontiest!.nativeElement.getContext('2d');
    this.updateGameSubscription = this.gameUpdateService.gameUpdate$.subscribe(value => {
      this.drawMap(value.gameField, this.pixelLength, this.playerIdentificationService.getPlayerName(), value.visible);
      this.lastMap = value.gameField;
      this.lastVisible = value.visible;
    });
  }

  ngOnDestroy() {
    this.gameUpdateService.stopPollingGameUpdates();
    if (this.updateGameSubscription) {
      this.updateGameSubscription.unsubscribe();
    }
  }


}
