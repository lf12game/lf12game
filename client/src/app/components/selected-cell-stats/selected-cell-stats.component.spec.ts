import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SelectedCellStatsComponent} from './selected-cell-stats.component';

describe('SelectedCellStatsComponent', () => {
  let component: SelectedCellStatsComponent;
  let fixture: ComponentFixture<SelectedCellStatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SelectedCellStatsComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedCellStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
