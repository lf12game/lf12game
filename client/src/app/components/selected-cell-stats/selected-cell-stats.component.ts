import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {StatUnit} from "../../model/StatUnit";
import {GameElement} from "../../model/GameElement";
import {MainRobot} from "../../model/MainRobot";
import {AutomatedRobot} from "../../model/AutomatedRobot";
import {Building} from "../../model/Building";
import {RobotProductionBuilding} from "../../model/RobotProductionBuilding";
import {BuildingCommandsService} from "../../services/building-commands.service";
import {Point} from "../../model/Point";
import {FieldCell} from "../../model/FieldCell";
import {HttpResponse} from "@angular/common/http";
import {MainRobotMoveResult} from "../../model/MainRobotMoveResult";
import {MessageService} from "../../services/message.service";
import {PlayerIdentificationService} from "../../services/player-identification.service";
import {RobotProductionResult} from "../../model/RobotProductionResult";
import {BuildingResult} from "../../model/BuildingResult";
import {AutomatedCommandsResult} from "../../model/AutomatedCommandsResult";
import {GameVisualizationComponent} from "../game-visualization/game-visualization.component";
import {AutomatedCommandsEditorComponent} from "../automated-commands-editor/automated-commands-editor.component";
import {ChangeRepeatProductionResult} from "../../model/ChangeRepeatProductionResult";
import {AvailableOptionsService} from "../../services/available-options.service";
import {ResearchBuilding} from "../../model/ResearchBuilding";

@Component({
  selector: 'app-selected-cell-stats',
  templateUrl: './selected-cell-stats.component.html',
  styleUrls: ['./selected-cell-stats.component.css']
})
export class SelectedCellStatsComponent implements OnInit {

  @Input() unit?: Object | null;
  @Input() selectedCell?: FieldCell | null;

  @Output() robotNotification: EventEmitter<RobotProductionResult>;
  @Output() repeatNotification: EventEmitter<ChangeRepeatProductionResult>;
  @ViewChild(AutomatedCommandsEditorComponent) commandsEditorChild?: AutomatedCommandsEditorComponent

  constructor(private buildingCommandsService: BuildingCommandsService, private messageService: MessageService,
              private identification: PlayerIdentificationService, private availableOptionsService: AvailableOptionsService) {
    this.robotNotification = new EventEmitter();
    this.repeatNotification = new EventEmitter();
  }

  ngOnInit(): void {
  }

  isStatUnit(): boolean {
    if (this.unit) {
      return this.unit.hasOwnProperty("maxHP");
    }
    return false;
  }

  get statUnit(): StatUnit {
    return this.unit as StatUnit;
  }

  isMainRobot(): boolean {
    if (this.unit) {
      return this.unit.hasOwnProperty("currentMoveOrders");
    }
    return false;
  }

  get mainRobot(): MainRobot {
    return this.unit as MainRobot;
  }

  isAutomatedRobot(): boolean {
    if (this.unit) {
      return this.unit.hasOwnProperty("buildCost") && !this.unit.hasOwnProperty("resourcesPerTick");
    }
    return false;
  }

  get automatedRobot(): AutomatedRobot {
    return this.unit as AutomatedRobot;
  }

  get isBuilding(): boolean {
    if (this.unit) {
      return this.unit.hasOwnProperty("resourcesPerTick");
    }
    return false;
  }

  get building(): Building {
    return this.unit as Building;
  }

  isRobotProductionBuilding(): boolean {
    if (this.unit) {
      return this.unit.hasOwnProperty("rootCommand");
    }
    return false;
  }

  get robotProductionBuilding(): RobotProductionBuilding {
    return this.unit as RobotProductionBuilding;
  }

  isResearchBuilding(): boolean {
    if (this.unit) {
      return this.unit.hasOwnProperty("passedResearchDuration");
    }
    return false;
  }

  get researchBuilding(): ResearchBuilding {
    return this.unit as ResearchBuilding;
  }

  isCurrentlyBuildingUnit(): boolean {
    if (this.unit) {
      return this.isRobotProductionBuilding() && this.robotProductionBuilding.currentProduction != null;
    }
    return false;
  }

  canBuildUnit(): boolean {
    if (this.unit) {
      return !this.isCurrentlyBuildingUnit() && this.robotProductionBuilding.passedDuration >= this.robotProductionBuilding.buildDuration;
    }
    return false;
  }

  isUnitOwned(): boolean {
    if (this.unit) {
      return this.identification.getPlayerName() === (this.unit as GameElement).player;
    }
    return false;
  }

  isUpgrading(): boolean {
    if (this.unit) {
      return this.statUnit.currentUpgrade !== null;
    }
    return false;
  }

  isResearching(): boolean {
    if (this.unit) {
      return this.researchBuilding.currentResearch !== null;
    }
    return false;
  }

  abortRobot(): void {
    let answer: Promise<HttpResponse<RobotProductionResult>> = this.buildingCommandsService.abortRobot(this.selectedCell!.x, this.selectedCell!.y);
    answer.then(e => {
      if (e.body) {
        this.robotNotification.emit(e.body);
      }
    }, f => {

    })
  }

  buildRobot(robotType: string): void {

    let answer: Promise<HttpResponse<RobotProductionResult>> = this.buildingCommandsService.buildRobot(this.selectedCell!.x, this.selectedCell!.y, robotType);
    answer.then(e => {
      if (e.status == 417) {
        this.messageService.addMessage("Not enough resources for robot");
      }
      if (e.body) {
        this.robotNotification.emit(e.body);
      }
    }, f => {

    })
  }

  getPoint(): Point {
    return new Point(this.selectedCell!.x, this.selectedCell!.y);
  }

  isUnlocked(robotName: string) {
    return this.availableOptionsService.getFinishedResearch().includes(robotName);
  }

  updateOnCodeChangeEvent(automatedCommandsResult: AutomatedCommandsResult) {
    if (this.unit) {
      this.robotProductionBuilding.rootCommand = automatedCommandsResult.rootCommand;
      if (this.commandsEditorChild && this.selectedCell && automatedCommandsResult.buildingX === this.selectedCell.x && automatedCommandsResult.buildingY === this.selectedCell.y) {
        this.commandsEditorChild.updateWorkitem(automatedCommandsResult.rootCommand);
      }
    }
  }

  repeatProductionChanged() {
    this.repeatNotification.emit(new ChangeRepeatProductionResult(this.unit! as RobotProductionBuilding, this.selectedCell!.x, this.selectedCell!.y));
    this.buildingCommandsService.setRepeatProduction(this.selectedCell!.x, this.selectedCell!.y, (this.unit! as RobotProductionBuilding).repeatProduction);
  }

}
