import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {AutomatedCommand} from "../../model/AutomatedCommand";
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import {Point} from "../../model/Point";
import {BuildingCommandsService} from "../../services/building-commands.service";
import {Constants} from "../../model/Constants";
import {HttpResponse} from "@angular/common/http";
import {BuildingResult} from "../../model/BuildingResult";
import {AutomatedCommandsResult} from "../../model/AutomatedCommandsResult";

@Component({
  selector: 'app-automated-commands-editor',
  templateUrl: './automated-commands-editor.component.html',
  styleUrls: ['./automated-commands-editor.component.css']
})
export class AutomatedCommandsEditorComponent implements OnInit {
  @Input() parentItem!: AutomatedCommand;
  public workItem: AutomatedCommand;
  public newOptions: AutomatedCommand[];
  @Input() buildingLocation!: Point;

  @Output() codeChangeNotification: EventEmitter<AutomatedCommandsResult>;

  public get connectedDropListsIds(): string[] {
    return this.getIdsRecursive(this.workItem).reverse();
  }

  constructor(private buildingCommandsService: BuildingCommandsService) {
    this.newOptions = [];
    this.workItem = this.parentItem;
    this.codeChangeNotification = new EventEmitter();
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'buildingLocation': {
            let chng = changes['buildingLocation'];
            let cur: Point = chng.currentValue;
            let prev: Point = chng.previousValue;
            if (prev && (cur.y !== prev.y || cur.x !== prev.x)) {
              this.workItem = this.parentItem;
            }
          }
        }
      }
    }
  }

  updateWorkitem(newRoot: AutomatedCommand): void {
    this.workItem = newRoot;
  }

  public ngOnInit() {
    this.newOptions.push(new AutomatedCommand('If', [[], []], ["Enemy in Range", "Enemy in Sight", "Enemy Main Robot in Sight", "Enemy Main Robot in Range", "True", "False"]));
    this.newOptions.push(new AutomatedCommand('While', [[]], ["Enemy in Range", "Enemy in Sight", "Enemy Main Robot in Sight", "Enemy Main Robot in Range", "True", "False"]));
    this.newOptions.push(new AutomatedCommand('Move', [], ["To nearest Enemy", "To Enemy Main Robot", "To Unexplored Terrain"]));
    for (let x = 1; x <= Constants.NUMBEROFWAYPOINTS; x++) {
      this.newOptions[0].options.push("Waypoint " + x + " is set");
      this.newOptions[1].options.push("Waypoint " + x + " is set");
      this.newOptions[2].options.push("To Waypoint " + x);
    }
    this.newOptions.push(new AutomatedCommand('Attack', [], ["Nearest Enemy", "Enemy Main Robot"]));
    this.newOptions.push(new AutomatedCommand('NoOp', [], []));
    this.newOptions.push(new AutomatedCommand('Return', [], []));
    this.workItem = this.parentItem;
  }

  public onDragDrop(event: CdkDragDrop<any[], any[]>) {
    event.container.element.nativeElement.classList.remove('active');

    if (this.canBeDropped(event)) {
      const movingItem: AutomatedCommand = event.item.data;
      let target: AutomatedCommand[] = event.container.data[0] as AutomatedCommand[];
      if (event.previousContainer.id === "optionSource") {
        target.splice(event.currentIndex, 0, AutomatedCommand.simpleClone(movingItem));
      } else {
        let source: AutomatedCommand[] = event.previousContainer.data[0] as AutomatedCommand[];
        transferArrayItem(source, target, event.previousIndex, event.currentIndex);
      }
    } else {
      moveItemInArray(
        event.container.data[0] as AutomatedCommand[],
        event.previousIndex,
        event.currentIndex);
    }
  }

  public saveCommands(): void {
    let answer: Promise<HttpResponse<AutomatedCommandsResult>> = this.buildingCommandsService.changeRobotCode(this.workItem, this.buildingLocation);
    answer.then(e => {
      if (e.body) {
        this.codeChangeNotification.emit(e.body);
      }
    }, f => {

    })
  }

  private getIdsRecursive(item: AutomatedCommand): string[] {
    let ids: string[] = [];
    item.children.forEach((list, index) => {
      ids.push(item.uid + index);
      list.forEach((childItem) => {
        ids = childItem.children.length > 0 ? ids.concat(this.getIdsRecursive(childItem)) : ids
      })
    });
    return ids;
  }

  private canBeDropped(event: CdkDragDrop<any[], any[]>): boolean {
    const movingItem: AutomatedCommand = event.item.data;

    return event.previousContainer.id !== event.container.id
      //&& this.isNotSelfDrop(event)
      && !this.hasChild(movingItem, event.container.data[1] as AutomatedCommand);
  }

  //private isNotSelfDrop(event: CdkDragDrop<Item[], Item> | CdkDragEnter<Item[], Item> | CdkDragExit<Item[], Item>): boolean {
  //return event.container.data[event.previousIndex].uId !== event.item.data.uId;
  //}

  private hasChild(mainItem: AutomatedCommand, childItem: AutomatedCommand): boolean {
    const hasChild = mainItem.children.some(
      (list) => list.some(item => item.uid === childItem.uid)
    );
    return hasChild
      ? true
      : mainItem.children.some((list) => list.some(item => this.hasChild(item, childItem)));
  }
}
