import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AutomatedCommandsEditorComponent} from './automated-commands-editor.component';

describe('AutomatedCommandsEditorComponent', () => {
  let component: AutomatedCommandsEditorComponent;
  let fixture: ComponentFixture<AutomatedCommandsEditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AutomatedCommandsEditorComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomatedCommandsEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
