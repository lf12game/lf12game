import {Component, OnInit} from '@angular/core';
import {GameInitializationService} from "../../services/game-initialization.service";
import {HttpRequest, HttpResponse} from "@angular/common/http";
import {RejoinGameResult} from "../../model/RejoinGameResult";
import {JoinGameResult} from "../../enums/JoinGameResult";
import {PlayerIdentificationService} from "../../services/player-identification.service";
import {Router} from "@angular/router";
import {AvailableOptionsService} from "../../services/available-options.service";
import {PlayerColorService} from "../../services/player-color.service";
import {GameUpdateService} from "../../services/game-update.service";

@Component({
  selector: 'app-game-menu',
  templateUrl: './game-menu.component.html',
  styleUrls: ['./game-menu.component.css']
})
export class GameMenuComponent implements OnInit {

  constructor(private gameInitializationService: GameInitializationService, private identificationService: PlayerIdentificationService,
              private router: Router, private availableOptions: AvailableOptionsService, private playerColorService: PlayerColorService,
              private gameUpdateService: GameUpdateService) {
  }

  ngOnInit(): void {
  }

  async rejoinGame(): Promise<void> {
    let cookies: string[] = document.cookie.split(";");
    let iden = cookies.filter(c => c.split("=")[0].includes("identification"))[0].split("=")[1];
    document.cookie = "identification=";
    await this.gameInitializationService.rejoinGame(iden).then
    ((answer) => {
      if (answer.status !== 200 || !answer.body) {
        return;
      }
      let body: RejoinGameResult = answer.body;
      this.identificationService.setIdentification(body.lobbyInfo.playerIdentification);
      this.identificationService.setPlayerGame(body.lobbyInfo.gameName);
      this.identificationService.setPlayerName(body.playerName);
      if (body.lobbyInfo.started) {
        this.availableOptions.setAllUpgrades(body.lobbyInfo.allUpgrades);
        this.availableOptions.setAllResearch(body.lobbyInfo.allResearch);
        this.playerColorService.initializePlayerColors(body.lobbyInfo.playerNames);
        this.availableOptions.setFinishedResearch(body.gameUpdate.finishedResearch);
        this.gameUpdateService.setGameUpdateAfterRejoin(body.gameUpdate);
        this.router.navigateByUrl('gameView', {skipLocationChange: true, state: body.waypoints});
      } else {
        this.gameInitializationService.setLobbySettingsAfterRejoin(body.lobbyInfo);
        this.router.navigateByUrl('/lobby', {skipLocationChange: true});
      }
    });
  }

  rejoinCookieExists(): boolean {
    let cookie = document.cookie.split(";");
    return cookie.some(c => c.split("=")[0].includes("identification") && c.split("=")[1]);
  }
}
