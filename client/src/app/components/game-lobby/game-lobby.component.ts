import {Component, Input, OnInit} from '@angular/core';
import {PlayerIdentificationService} from "../../services/player-identification.service";
import {GameInitializationService} from "../../services/game-initialization.service";
import {Subscription} from "rxjs";
import {Router} from "@angular/router";
import {PlayerColorService} from "../../services/player-color.service";

@Component({
  selector: 'app-game-lobby',
  templateUrl: './game-lobby.component.html',
  styleUrls: ['./game-lobby.component.css']
})
export class GameLobbyComponent implements OnInit {

  gameName: string;
  private currentlyStarting: boolean;
  private hasStartedSubscription?: Subscription;
  private lobbyUpdateSubscription?: Subscription;
  public x: number;
  public y: number;
  public startRessources: number
  public updateFrequency: number;
  public blockerFrequency: number;
  public resourceFrequency: number;

  constructor(private router: Router, private identification: PlayerIdentificationService, public gameInitializationService: GameInitializationService, public playerColorService: PlayerColorService) {
    this.gameName = this.identification.getPlayerGame();
    this.currentlyStarting = false;
    this.x = 100;
    this.y = 47;
    this.startRessources = 500;
    this.updateFrequency = 5000;
    this.blockerFrequency = 20;
    this.resourceFrequency = 3;
  }

  isHost(): boolean {
    return this.gameInitializationService.getCurrentHost() === this.identification.getPlayerName();
  }

  ngOnInit(): void {
    this.gameInitializationService.startPollingLobbyPlayers();
    this.hasStartedSubscription = this.gameInitializationService.gameStarted$.subscribe(val => {
      if (val) {
        this.gameInitializationService.stopPollingLobbyPlayers();
        if (this.hasStartedSubscription) {
          this.hasStartedSubscription.unsubscribe();
        }
        this.playerColorService.initializePlayerColors(this.gameInitializationService.getCurrentPlayers())
        this.router.navigateByUrl('gameView', {skipLocationChange: true});
      }
    })
    this.lobbyUpdateSubscription = this.gameInitializationService.lobbyUpdate$.subscribe(val => {
      this.x = val.x;
      this.y = val.y;
      this.resourceFrequency = val.resourceFrequency;
      this.blockerFrequency = val.blockerFrequency;
      this.updateFrequency = val.updateFrequency;
      this.startRessources = val.startResources;
    })
  }

  async startGame() {
    if (this.currentlyStarting) {
      return;
    }
    this.currentlyStarting = true;
    await this.gameInitializationService.startGame(this.identification.getPlayerGame());
    this.currentlyStarting = false;
  }

  ngOnDestroy() {
    this.gameInitializationService.stopPollingLobbyPlayers();
    if (this.hasStartedSubscription) {
      this.hasStartedSubscription.unsubscribe();
    }
  }

  updateSettings(): void {
    if (this.isHost()) {
      this.gameInitializationService.changeSettings(this.x, this.y, this.startRessources, this.updateFrequency,
        this.blockerFrequency, this.resourceFrequency);
    }
  }
}
