import {Component, EventEmitter, Input, Output} from '@angular/core';
import {CdkDragDrop} from "@angular/cdk/drag-drop";
import {AutomatedCommand} from "../../model/AutomatedCommand";

@Component({
  selector: 'app-command-display',
  templateUrl: './command-display.component.html',
  styleUrls: ['./command-display.component.css']
})
export class CommandDisplayComponent {

  @Input() item!: AutomatedCommand;
  @Input() parentItem?: AutomatedCommand;

  @Input()
  public set connectedDropListsIds(ids: string[]) {
    this.allDropListsIds = ids;
  }

  public get connectedDropListsIds(): string[] {
    return this.allDropListsIds;
    //return this.allDropListsIds.filter((id) => id !== this.item.uId);
  }

  public allDropListsIds: string[];

  public get dragDisabled(): boolean {
    return false;
  }

  public get parentItemId(): string {
    return this.dragDisabled ? '' : this.parentItem!.uid;
  }


  @Output() itemDrop: EventEmitter<CdkDragDrop<any[], any[]>>

  constructor() {
    this.allDropListsIds = [];
    this.itemDrop = new EventEmitter();
  }

  public onDragDrop(event: CdkDragDrop<any[], any[]>): void {
    this.itemDrop.emit(event);
  }

  public delete(child: AutomatedCommand, parent: AutomatedCommand): void {
    for (let x = 0; x < parent.children.length; x++) {
      parent.children[x] = parent.children[x].filter(e => e.uid !== child.uid);
    }
  }
}
