import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subscription} from "rxjs";
import {GameUpdateService} from "./game-update.service";
import {GameUpdate} from "../model/GameUpdate";
import {GameResult} from "../model/GameResult";

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private messages: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
  public messages$: Observable<string[]> = this.messages.asObservable();
  private turn: number;
  private gameUpdateSubscription?: Subscription;

  constructor(public gameUpdateService: GameUpdateService) {
    this.turn = -1;
    this.gameUpdateSubscription = this.gameUpdateService.gameUpdate$.subscribe(value => {
      this.turn = value.version;
    });
  }

  resetService(): void {
    if (this.gameUpdateSubscription) {
      this.gameUpdateSubscription.unsubscribe();
    }
    this.messages = new BehaviorSubject<string[]>([]);
    this.messages$ = this.messages.asObservable();
    this.turn = -1;
    this.gameUpdateService.gameUpdate$.subscribe(value => {
      this.turn = value.version;
    });
  }

  addMessage(message: string): void {
    let currentMessages: string[] = this.messages.value;
    currentMessages.unshift(this.turn + ": " + message);
    currentMessages = currentMessages.slice(0, 5);
    this.messages.next(currentMessages);
  }

}
