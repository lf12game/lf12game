import {TestBed} from '@angular/core/testing';

import {MainRobotCommandsService} from './main-robot-commands.service';

describe('MainRobotCommandsService', () => {
  let service: MainRobotCommandsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MainRobotCommandsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
