import {Injectable} from '@angular/core';
import {BehaviorSubject, firstValueFrom, Observable} from "rxjs";
import {PlayerIdentificationService} from "./player-identification.service";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {LobbiesList} from "../model/LobbiesList";
import {JoinGameResult} from "../enums/JoinGameResult";
import {LobbyUpdate} from "../model/LobbyUpdate";
import {CreateGameResult} from "../enums/CreateGameResult";
import {AvailableOptionsService} from "./available-options.service";
import {RejoinGameResult} from "../model/RejoinGameResult";

@Injectable({
  providedIn: 'root'
})
export class GameInitializationService {

  private lobbies: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
  public lobbies$: Observable<string[]> = this.lobbies.asObservable();
  private lobbyPlayers: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
  public lobbyPlayers$: Observable<string[]> = this.lobbyPlayers.asObservable();
  private keepPollingLobbies: boolean;
  private lobbiesPollingActive: boolean;
  private lobbiesVersion: number;
  private keepPollingLobbyPlayers: boolean;
  private lobbyPlayersPollingActive: boolean;
  private lobbyPlayersVersion: number;
  private gameStarted: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public gameStarted$: Observable<boolean> = this.gameStarted.asObservable();
  private lobbyUpdate: BehaviorSubject<LobbyUpdate> = new BehaviorSubject<LobbyUpdate>(new LobbyUpdate("", "", [], false, 0, 100, 47, 500, 5000, 20, 3, [], []));
  public lobbyUpdate$: Observable<LobbyUpdate> = this.lobbyUpdate.asObservable();

  constructor(private identification: PlayerIdentificationService, private http: HttpClient, private availableOptions: AvailableOptionsService) {
    this.lobbiesVersion = -1;
    this.lobbyPlayersVersion = -1;
    this.keepPollingLobbies = false;
    this.keepPollingLobbyPlayers = false;
    this.lobbiesPollingActive = false;
    this.lobbyPlayersPollingActive = false;
  }

  resetService(): void {
    this.lobbiesVersion = -1;
    this.lobbyPlayersVersion = -1;
    this.keepPollingLobbies = false;
    this.keepPollingLobbyPlayers = false;
    this.lobbiesPollingActive = false;
    this.lobbyPlayersPollingActive = false;
    this.lobbies = new BehaviorSubject<string[]>([]);
    this.lobbies$ = this.lobbies.asObservable();
    this.lobbyPlayers = new BehaviorSubject<string[]>([]);
    this.lobbyPlayers$ = this.lobbyPlayers.asObservable();
    this.gameStarted = new BehaviorSubject<boolean>(false);
    this.gameStarted$ = this.gameStarted.asObservable();
    this.lobbyUpdate = new BehaviorSubject<LobbyUpdate>(new LobbyUpdate("", "", [], false, 0, 100, 47, 500, 5000, 20, 3, [], []));
    this.lobbyUpdate$ = this.lobbyUpdate.asObservable();
  }

  public getCurrentPlayers(): string[] {
    return this.lobbyPlayers.value;
  }

  public getCurrentHost(): string {
    return this.lobbyPlayers.value[0];
  }

  public startPollingLobbies(): void {
    this.keepPollingLobbies = true;
    if (!this.lobbiesPollingActive) {
      this.longpollLobbies();
    }
  }

  public stopPollingLobbies(): void {
    this.keepPollingLobbies = false;
  }

  private async longpollLobbies(): Promise<void> {
    this.lobbiesPollingActive = true;
    while (this.keepPollingLobbies) {
      let temp: LobbiesList | null = await this.getLobbies();
      if (temp && this.keepPollingLobbies) {
        this.lobbiesVersion = temp.version;
        this.lobbies.next(temp.lobbyNames);
      }
    }
    this.lobbiesPollingActive = false;
  }

  private async getLobbies(): Promise<LobbiesList | null> {
    let body: string = JSON.stringify({
      "prevVersion": this.lobbiesVersion
    });
    return await firstValueFrom(this.http.request<LobbiesList>('put', '/server/lobbies', {
      body: body,
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
    })).catch((err) => {
      return null;
    });
  }

  public startPollingLobbyPlayers(): void {
    this.keepPollingLobbyPlayers = true;
    if (!this.lobbyPlayersPollingActive) {
      this.longpollLobbyPlayers();
    }
  }

  public stopPollingLobbyPlayers(): void {
    this.keepPollingLobbyPlayers = false;
  }

  private async longpollLobbyPlayers(): Promise<void> {
    this.lobbyPlayersPollingActive = true;
    while (this.keepPollingLobbyPlayers) {
      let temp: LobbyUpdate | null = await this.getLobbyPlayers();
      if (temp && this.keepPollingLobbyPlayers) {
        this.lobbyPlayersVersion = temp.version;
        this.lobbyPlayers.next(temp.playerNames);
        this.gameStarted.next(temp.started);
        this.lobbyUpdate.next(temp);
        if (temp.allUpgrades) {
          this.availableOptions.setAllUpgrades(temp.allUpgrades);
          this.availableOptions.setAllResearch(temp.allResearch);
        }
      }
    }
    this.lobbyPlayersPollingActive = false;
  }

  public setLobbySettingsAfterRejoin(update: LobbyUpdate): void {
    this.lobbyUpdate.next(update);
  }

  private async getLobbyPlayers(): Promise<LobbyUpdate | null> {
    let body: string = JSON.stringify({
      "gameName": this.identification.getPlayerGame(),
      "identification": this.identification.getidentification(),
      "prevVersion": this.lobbyPlayersVersion
    });
    let res: HttpResponse<LobbyUpdate> = await firstValueFrom(this.http.request<LobbyUpdate>('put', '/server/lobbies/lobby', {
      body: body,
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
    switch (res.status) {
      case 401: //Unauthorized
        this.keepPollingLobbyPlayers = false;
        break;
      default:
    }
    return res.body;
  }

  public async joinGame(gameName: string, playerName: string, password: string): Promise<JoinGameResult> {
    let body: string = JSON.stringify({
      "gameName": gameName,
      "playerName": playerName,
      "password": password
    });
    let res: HttpResponse<LobbyUpdate> = await firstValueFrom(this.http.post<LobbyUpdate>('/server/lobbies/join', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
    switch (res.status) {
      case 401: //Unauthorized
        return JoinGameResult.PASSWORDMISMATCH;
      case 403: //Forbidden
        return JoinGameResult.NAMEALREADYINUSE;
      case 404: //Not Found
        return JoinGameResult.GAMENOTFOUND;
      case 409: //Conflict
        return JoinGameResult.GAMEALREADYSTARTED;
      default:
    }
    if (!res?.body) {
      return JoinGameResult.COMMUNICATIONERROR;
    }
    let payload: LobbyUpdate = res.body;
    this.identification.setIdentification(payload.playerIdentification);
    this.identification.setPlayerGame(payload.gameName);
    this.identification.setPlayerName(playerName);
    this.lobbyPlayers.next(payload.playerNames);
    this.lobbyPlayersVersion = payload.version;
    return JoinGameResult.SUCCESS;
  }

  public async rejoinGame(identification: string): Promise<HttpResponse<RejoinGameResult>> {
    let body: string = JSON.stringify({
      "identification": identification
    });
    return await firstValueFrom(this.http.put<RejoinGameResult>('/server/lobbies/rejoin', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
  }

  public async createGame(gameName: string, playerName: string, password: string): Promise<CreateGameResult> {
    let body: string = JSON.stringify({
      "gameName": gameName,
      "playerName": playerName,
      "password": password
    });
    let res: HttpResponse<LobbyUpdate> = await firstValueFrom(this.http.post<LobbyUpdate>('/server/lobbies', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
    switch (res.status) {
      case 409: //Conflict
        return CreateGameResult.GAMENAMEALREADYINUSE;
      default:
    }
    if (!res?.body) {
      return CreateGameResult.COMMUNICATIONERROR;
    }
    let payload: LobbyUpdate = res.body;
    this.identification.setIdentification(payload.playerIdentification);
    this.identification.setPlayerGame(payload.gameName);
    this.identification.setPlayerName(playerName);
    this.lobbyPlayers.next(payload.playerNames);
    this.lobbyPlayersVersion = payload.version;
    return CreateGameResult.SUCCESS;
  }

  public async startGame(gameName: string): Promise<void> {
    let body: string = JSON.stringify({
      "gameName": this.identification.getPlayerGame(),
      "identification": this.identification.getidentification()
    });
    await firstValueFrom(this.http.post('/server/lobbies/start', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
    }));
  }

  public async changeSettings(x: number, y: number, startResources: number, updateStartFrequency: number, blockerFrequency: number,
                              resourceFrequency: number): Promise<HttpResponse<void>> {
    let body: string = JSON.stringify({
      "x": x,
      "y": y,
      "startResources": startResources,
      "updateFrequency": updateStartFrequency,
      "blockerFrequency": blockerFrequency,
      "resourceFrequency": resourceFrequency,
      "identification": this.identification.getidentification()
    });
    return await firstValueFrom(this.http.post<void>('/server/lobbies/updateSettings', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    }))
      .catch((err) => {
        return err;
      });
  }

  ngOnDestroy() {
    this.keepPollingLobbies = false;
    this.keepPollingLobbyPlayers = false
  }

}
