import {TestBed} from '@angular/core/testing';

import {ResearchCommandsService} from './research-commands.service';

describe('ResearchCommandsService', () => {
  let service: ResearchCommandsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResearchCommandsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
