import {Injectable} from '@angular/core';
import {PlayerIdentificationService} from "./player-identification.service";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {MainRobotMoveResult} from "../model/MainRobotMoveResult";
import {firstValueFrom} from "rxjs";
import {AutomatedCommand} from "../model/AutomatedCommand";
import {Point} from "../model/Point";
import {BuildingResult} from "../model/BuildingResult";
import {RobotProductionResult} from "../model/RobotProductionResult";
import {AutomatedCommandsResult} from "../model/AutomatedCommandsResult";

@Injectable({
  providedIn: 'root'
})
export class BuildingCommandsService {

  constructor(private identification: PlayerIdentificationService, private http: HttpClient) {
  }

  public async buildBuilding(x: number, y: number, buildingType: string): Promise<HttpResponse<BuildingResult>> {//TODO?
    let body: string = JSON.stringify({
      "x": x,
      "y": y,
      "buildingType": buildingType,
      "identification": this.identification.getidentification()
    });
    return await firstValueFrom(this.http.post<BuildingResult>('/server/game/building/build', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
  }

  public async abortBuilding(): Promise<HttpResponse<BuildingResult>> {//TODO?
    let body: string = JSON.stringify({
      "identification": this.identification.getidentification()
    });
    return await firstValueFrom(this.http.post<BuildingResult>('/server/game/building/abort', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
  }

  public async buildRobot(x: number, y: number, robotType: string): Promise<HttpResponse<RobotProductionResult>> {
    let body: string = JSON.stringify({
      "x": x,
      "y": y,
      "robotType": robotType,
      "identification": this.identification.getidentification()
    });
    return await firstValueFrom(this.http.post<RobotProductionResult>('/server/game/building/buildRobot', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
  }

  public async abortRobot(x: number, y: number): Promise<HttpResponse<RobotProductionResult>> {
    let body: string = JSON.stringify({
      "x": x,
      "y": y,
      "identification": this.identification.getidentification()
    });
    return await firstValueFrom(this.http.post<RobotProductionResult>('/server/game/building/abortRobot', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
  }

  public async setRepeatProduction(x: number, y: number, repeatProduction: boolean): Promise<HttpResponse<void>> {
    let body: string = JSON.stringify({
      "x": x,
      "y": y,
      "repeatProduction": repeatProduction,
      "identification": this.identification.getidentification()
    });
    return await firstValueFrom(this.http.post<void>('/server/game/building/changeRepeatProduction', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
  }

  public async changeRobotCode(code: AutomatedCommand, location: Point): Promise<HttpResponse<AutomatedCommandsResult>> {
    let body: string = JSON.stringify({
      "x": location.x,
      "y": location.y,
      "rootCommand": code,
      "identification": this.identification.getidentification()
    });
    return await firstValueFrom(this.http.post<AutomatedCommandsResult>('/server/game/building/changeRobotCode', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
  }
}

