import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlayerIdentificationService {

  private identification?: string;
  private playerName: string;
  private playerGame: string;

  constructor() {
    this.playerGame = "";
    this.playerName = "";
  }

  resetService(): void {
    this.playerGame = "";
    this.playerName = "";
    this.identification = undefined;
  }

  setIdentification(identification: string) {
    this.identification = identification;
    document.cookie = "identification=" + identification;
  }

  hasIdentification(): boolean {
    return this.identification != null;
  }

  getidentification(): string {
    if (this.identification) {
      return this.identification;
    }
    return "";
  }

  getPlayerName() {
    return this.playerName;
  }

  getPlayerGame() {
    return this.playerGame;
  }

  setPlayerName(name: string) {
    this.playerName = name;
  }

  setPlayerGame(name: string) {
    this.playerGame = name;
  }


}
