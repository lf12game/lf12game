import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PlayerColorService {

  public initialized: boolean;

  public readonly colors: string[] =
    ["#FF8000",
      "#FFFF00",
      "#00FFFF",
      "#8A0808",
      "#FF00FF",
      "#b36cd2",
      "#00BFFF",
      "#8904B1",
      "#FA5858",
      "#F5A9E1",
      "#0B610B",
      "#88fcc3",
      "#B18904",
      "#61210B",]

  public colorsOfPlayers: Map<string, string>;

  constructor() {
    this.initialized = false;
    this.colorsOfPlayers = new Map<string, string>();
  }

  resetService(): void {
    this.initialized = false;
    this.colorsOfPlayers = new Map<string, string>();
  }

  initializePlayerColors(playerNames: string[]) {
    if (!this.initialized) {
      playerNames.forEach((value, index) => {
        this.colorsOfPlayers.set(value, this.colors[index])
      })
      this.initialized = true;
    }
  }

  getColorOfPlayer(playerName: string): string {
    return this.colorsOfPlayers.get(playerName)!
  }


}
