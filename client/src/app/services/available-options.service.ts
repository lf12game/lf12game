import {Injectable} from '@angular/core';
import {Upgrade} from "../model/Upgrade";
import {Research} from "../model/Research";

@Injectable({
  providedIn: 'root'
})
export class AvailableOptionsService {

  private allUpgrades: Upgrade[];
  private allResearch: Research[];
  private finishedResearch: string[];

  constructor() {
    this.allUpgrades = [];
    this.allResearch = [];
    this.finishedResearch = [];
  }

  resetService(): void {
    this.allUpgrades = [];
    this.allResearch = [];
    this.finishedResearch = [];
  }

  setFinishedResearch(finishedResearch: string[]): void {
    this.finishedResearch = finishedResearch;
  }

  getFinishedResearch(): string[] {
    return this.finishedResearch;
  }

  setAllUpgrades(allUpgrades: Upgrade[]): void {
    this.allUpgrades = allUpgrades;
  }

  getAllUpgrades(): Upgrade[] {
    return this.allUpgrades;
  }

  setAllResearch(allResearch: Research[]): void {
    this.allResearch = allResearch;
  }

  getAllResearch(): Research[] {
    return this.allResearch;
  }
}
