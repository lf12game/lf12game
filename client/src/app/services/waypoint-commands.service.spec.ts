import {TestBed} from '@angular/core/testing';

import {WaypointCommandsService} from './waypoint-commands.service';

describe('WaypointCommandsService', () => {
  let service: WaypointCommandsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WaypointCommandsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
