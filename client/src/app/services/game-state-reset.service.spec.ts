import {TestBed} from '@angular/core/testing';

import {GameStateResetService} from './game-state-reset.service';

describe('GameStateResetService', () => {
  let service: GameStateResetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GameStateResetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
