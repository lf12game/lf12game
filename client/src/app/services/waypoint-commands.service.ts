import {Injectable} from '@angular/core';
import {PlayerIdentificationService} from "./player-identification.service";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {MainRobotMoveResult} from "../model/MainRobotMoveResult";
import {firstValueFrom} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class WaypointCommandsService {

  constructor(private identification: PlayerIdentificationService, private http: HttpClient) {
  }

  public async changeWaypoint(waypointX: number, waypointY: number, waypointIndex: number): Promise<HttpResponse<void>> {//TODO?
    let body: string = JSON.stringify({
      "x": waypointX,
      "y": waypointY,
      "index": waypointIndex,
      "identification": this.identification.getidentification()
    });
    return await firstValueFrom(this.http.post<void>('/server/game/changeWaypoint', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
  }
}
