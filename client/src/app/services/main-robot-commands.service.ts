import {Injectable} from '@angular/core';
import {PlayerIdentificationService} from "./player-identification.service";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {JoinGameResult} from "../enums/JoinGameResult";
import {LobbyUpdate} from "../model/LobbyUpdate";
import {firstValueFrom, Observable} from "rxjs";
import {MainRobotMoveResult} from "../model/MainRobotMoveResult";

@Injectable({
  providedIn: 'root'
})
export class MainRobotCommandsService {

  constructor(private identification: PlayerIdentificationService, private http: HttpClient) {
  }

  public async moveMainRobot(goalX: number, goalY: number): Promise<HttpResponse<MainRobotMoveResult>> {//TODO?
    let body: string = JSON.stringify({
      "x": goalX,
      "y": goalY,
      "identification": this.identification.getidentification()
    });
    return await firstValueFrom(this.http.post<MainRobotMoveResult>('/server/game/moveMainRobot', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
  }
}
