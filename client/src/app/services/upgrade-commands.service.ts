import {Injectable} from '@angular/core';
import {PlayerIdentificationService} from "./player-identification.service";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {UpgradeResult} from "../model/UpgradeResult";
import {firstValueFrom} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UpgradeCommandsService {

  constructor(private identification: PlayerIdentificationService, private http: HttpClient) {
  }

  public async startUpgrade(x: number, y: number, upgradeName: string): Promise<HttpResponse<UpgradeResult>> {
    let body: string = JSON.stringify({
      "x": x,
      "y": y,
      "upgradeName": upgradeName,
      "identification": this.identification.getidentification()
    });
    return await firstValueFrom(this.http.post<UpgradeResult>('/server/game/upgrade/startUpgrade', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
  }

  public async abortUpgrade(x: number, y: number): Promise<HttpResponse<UpgradeResult>> {
    let body: string = JSON.stringify({
      "x": x,
      "y": y,
      "identification": this.identification.getidentification()
    });
    return await firstValueFrom(this.http.post<UpgradeResult>('/server/game/upgrade/abortUpgrade', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
  }
}
