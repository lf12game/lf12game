import {Injectable} from '@angular/core';
import {Point} from "../model/Point";
import {Constants} from "../model/Constants";
import {BehaviorSubject, Observable} from "rxjs";
import {GameUpdate} from "../model/GameUpdate";
import {GameResult} from "../model/GameResult";

@Injectable({
  providedIn: 'root'
})
export class WaypointService {

  private waypointsLocal: Point[];
  private waypoints: BehaviorSubject<Point[]> = new BehaviorSubject<Point[]>([]);
  public waypoints$: Observable<Point[]> = this.waypoints.asObservable();

  constructor() {
    this.waypointsLocal = [];
    for (let x = 0; x < Constants.NUMBEROFWAYPOINTS; x++) {
      this.waypointsLocal.push(new Point(-1, -1));
    }
    this.waypoints.next(this.waypointsLocal);
  }

  resetService(): void {
    this.waypoints = new BehaviorSubject<Point[]>([]);
    this.waypoints$ = this.waypoints.asObservable();
    this.waypointsLocal = [];
    for (let x = 0; x < Constants.NUMBEROFWAYPOINTS; x++) {
      this.waypointsLocal.push(new Point(-1, -1));
    }
    this.waypoints.next(this.waypointsLocal);
  }

  public getWaypoints(): Point[] {
    return this.waypointsLocal;
  }

  public changeWaypoint(x: number, y: number, index: number): void {
    this.waypointsLocal[index] = new Point(x, y);
    this.waypoints.next(this.waypointsLocal);
  }


}
