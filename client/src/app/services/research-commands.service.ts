import {Injectable} from '@angular/core';
import {PlayerIdentificationService} from "./player-identification.service";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {UpgradeResult} from "../model/UpgradeResult";
import {firstValueFrom} from "rxjs";
import {ResearchResult} from "../model/ResearchResult";

@Injectable({
  providedIn: 'root'
})
export class ResearchCommandsService {

  constructor(private identification: PlayerIdentificationService, private http: HttpClient) {
  }

  public async startResearch(x: number, y: number, researchName: string): Promise<HttpResponse<ResearchResult>> {
    let body: string = JSON.stringify({
      "x": x,
      "y": y,
      "researchName": researchName,
      "identification": this.identification.getidentification()
    });
    return await firstValueFrom(this.http.post<ResearchResult>('/server/game/research/startResearch', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
  }

  public async abortResearch(x: number, y: number): Promise<HttpResponse<ResearchResult>> {
    let body: string = JSON.stringify({
      "x": x,
      "y": y,
      "identification": this.identification.getidentification()
    });
    return await firstValueFrom(this.http.post<ResearchResult>('/server/game/research/abortResearch', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
  }
}
