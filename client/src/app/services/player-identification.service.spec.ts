import {TestBed} from '@angular/core/testing';

import {PlayerIdentificationService} from './player-identification.service';

describe('PlayerIdentificationService', () => {
  let service: PlayerIdentificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlayerIdentificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
