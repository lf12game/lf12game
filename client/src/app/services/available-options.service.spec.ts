import {TestBed} from '@angular/core/testing';

import {AvailableOptionsService} from './available-options.service';

describe('AvailableOptionsServiceService', () => {
  let service: AvailableOptionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AvailableOptionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
