import {Injectable} from '@angular/core';
import {BehaviorSubject, firstValueFrom, Observable} from "rxjs";
import {GameUpdate} from "../model/GameUpdate";
import {PlayerIdentificationService} from "./player-identification.service";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {GameResult} from "../model/GameResult";
import {LobbyUpdate} from "../model/LobbyUpdate";

@Injectable({
  providedIn: 'root'
})
export class GameUpdateService {

  private gameUpdate: BehaviorSubject<GameUpdate> = new BehaviorSubject<GameUpdate>(new GameUpdate([], [], [], -1, -1, GameResult.NONE, []));
  public gameUpdate$: Observable<GameUpdate> = this.gameUpdate.asObservable();
  private keepPollingGameUpdates: boolean;
  private gameUpdatePollingActive: boolean;
  private gameUpdateVersion: number;

  constructor(private identification: PlayerIdentificationService, private http: HttpClient) {
    this.keepPollingGameUpdates = false;
    this.gameUpdatePollingActive = false;
    this.gameUpdateVersion = -1;
  }

  resetService(): void {
    this.keepPollingGameUpdates = false;
    this.gameUpdatePollingActive = false;
    this.gameUpdateVersion = -1;
    this.gameUpdate = new BehaviorSubject<GameUpdate>(new GameUpdate([], [], [], -1, -1, GameResult.NONE, []));
    this.gameUpdate$ = this.gameUpdate.asObservable();
  }

  public setGameUpdateAfterRejoin(update: GameUpdate): void {
    this.gameUpdate.next(update);
  }

  public startPollingGameUpdates(): void {
    this.keepPollingGameUpdates = true;
    if (!this.gameUpdatePollingActive) {
      this.longpollGameUpdates();
    }
  }

  public stopPollingGameUpdates(): void {
    this.keepPollingGameUpdates = false;
  }

  private async longpollGameUpdates(): Promise<void> {
    this.gameUpdatePollingActive = true;
    while (this.keepPollingGameUpdates) {
      let temp: GameUpdate | null = await this.getGameUpdate();
      if (temp && this.keepPollingGameUpdates) {
        this.gameUpdateVersion = temp.version;
        this.gameUpdate.next(temp);
      }
    }
    this.gameUpdatePollingActive = false;
  }

  private async getGameUpdate(): Promise<GameUpdate | null> {
    let body: string = JSON.stringify({
      "identification": this.identification.getidentification(),
      "prevVersion": this.gameUpdateVersion
    });
    let res: HttpResponse<GameUpdate> = await firstValueFrom(this.http.request<GameUpdate>('put', '/server/game', {
      body: body,
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
      observe: 'response'
    })).catch((err) => {
      return err;
    });
    switch (res.status) {
      case 200: //success
        break;
      default:
        this.keepPollingGameUpdates = false;
        break;
    }
    return res.body;
  }
}
