import {TestBed} from '@angular/core/testing';

import {UpgradeCommandsService} from './upgrade-commands.service';

describe('UpgradeCommandsServiceService', () => {
  let service: UpgradeCommandsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UpgradeCommandsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
