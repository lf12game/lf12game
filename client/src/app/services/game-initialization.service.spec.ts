import {TestBed} from '@angular/core/testing';

import {GameInitializationService} from './game-initialization.service';

describe('GameInitializationService', () => {
  let service: GameInitializationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GameInitializationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
