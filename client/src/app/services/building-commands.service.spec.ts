import {TestBed} from '@angular/core/testing';

import {BuildingCommandsService} from './building-commands.service';

describe('BuildingCommandsService', () => {
  let service: BuildingCommandsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BuildingCommandsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
