import {Injectable} from '@angular/core';
import {PlayerIdentificationService} from "./player-identification.service";
import {AvailableOptionsService} from "./available-options.service";
import {GameInitializationService} from "./game-initialization.service";
import {GameUpdateService} from "./game-update.service";
import {MessageService} from "./message.service";
import {PlayerColorService} from "./player-color.service";
import {WaypointService} from "./waypoint.service";

@Injectable({
  providedIn: 'root'
})
export class GameStateResetService {

  constructor(private availableOptionsService: AvailableOptionsService,
              private gameInitializationService: GameInitializationService,
              private gameUpdateService: GameUpdateService,
              private messageService: MessageService,
              private playerColorService: PlayerColorService,
              private playerIdentificationService: PlayerIdentificationService,
              private waypointService: WaypointService) {
  }

  public resetGame(): void {
    this.availableOptionsService.resetService();
    this.gameInitializationService.resetService();
    this.gameUpdateService.resetService();
    this.playerColorService.resetService();
    this.playerIdentificationService.resetService();
    this.waypointService.resetService();
    this.messageService.resetService();
  }
}


