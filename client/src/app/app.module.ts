import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {CreateGameComponent} from './components/create-game/create-game.component';
import {FormsModule} from "@angular/forms";
import {GameLobbyComponent} from "./components/game-lobby/game-lobby.component";
import {JoinGameComponent} from "./components/join-game/join-game.component";
import {GameInitializationService} from "./services/game-initialization.service";
import {PlayerIdentificationService} from "./services/player-identification.service";
import {RouterModule} from "@angular/router";
import {AppRoutingModule} from "./app-routing.module";
import {GameMenuComponent} from './components/game-menu/game-menu.component';
import {GameVisualizationComponent} from './components/game-visualization/game-visualization.component';
import {GameScreenComponent} from './components/game-screen/game-screen.component';
import {
  AutomatedCommandsEditorComponent
} from './components/automated-commands-editor/automated-commands-editor.component';
import {CommandDisplayComponent} from './components/command-display/command-display.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {GameUpdateService} from "./services/game-update.service";
import {BuildingCommandsService} from "./services/building-commands.service";
import {MainRobotCommandsService} from "./services/main-robot-commands.service";
import {SelectedCellStatsComponent} from './components/selected-cell-stats/selected-cell-stats.component';
import {BuildingOptionsComponent} from './components/building-options/building-options.component';
import {MessageService} from "./services/message.service";
import {WaypointCommandsService} from "./services/waypoint-commands.service";
import {WaypointComponent} from './components/waypoint/waypoint.component';
import {UpgradeCommandsService} from "./services/upgrade-commands.service";
import {AvailableOptionsService} from "./services/available-options.service";
import {PlayerColorService} from "./services/player-color.service";
import {WaypointService} from "./services/waypoint.service";
import {GameStateResetService} from "./services/game-state-reset.service";
import {ResearchCommandsService} from "./services/research-commands.service";

@NgModule({
  declarations: [
    AppComponent,
    CreateGameComponent,
    GameLobbyComponent,
    JoinGameComponent,
    GameMenuComponent,
    GameVisualizationComponent,
    GameScreenComponent,
    AutomatedCommandsEditorComponent,
    CommandDisplayComponent,
    SelectedCellStatsComponent,
    BuildingOptionsComponent,
    WaypointComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule,
    AppRoutingModule,
    DragDropModule
  ],
  providers: [
    GameInitializationService,
    PlayerIdentificationService,
    GameUpdateService,
    BuildingCommandsService,
    MainRobotCommandsService,
    MessageService,
    WaypointCommandsService,
    UpgradeCommandsService,
    AvailableOptionsService,
    PlayerColorService,
    WaypointService,
    GameStateResetService,
    ResearchCommandsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
