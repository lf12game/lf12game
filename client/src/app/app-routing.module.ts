import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {JoinGameComponent} from "./components/join-game/join-game.component";
import {CreateGameComponent} from "./components/create-game/create-game.component";
import {GameMenuComponent} from "./components/game-menu/game-menu.component";
import {GameLobbyComponent} from "./components/game-lobby/game-lobby.component";
import {GameVisualizationComponent} from "./components/game-visualization/game-visualization.component";
import {GameScreenComponent} from "./components/game-screen/game-screen.component";

const routes: Routes = [
  {path: '', component: GameMenuComponent},
  {path: 'create', component: CreateGameComponent},
  {path: 'join', component: JoinGameComponent},
  {path: 'lobby', component: GameLobbyComponent},
  {path: 'gameView', component: GameScreenComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule {
}
